<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>::SISTEMA DE GESTION ADMINISTRATIVO ESTRANET WISP::Modulo de Pagos</title>
<meta name="viewport" content="width=100%,initial-scale=1,user-scalable=no">
<style type="text/css">
form{
font:Tahoma;
font-size:12px;
color:gray;
width:100%;
height:20%;
}
table
{
width:95%;
max-height:1%;
border:0px;
background-color:none;
position:absolute;
top:30%;
left:3%;
right:97%;
bottom:70%;
}
tr
{
background-color:none;
color:black;
}
.total
{
color:gray;
background-color:yellow;
}
.informe
{
color:gray;
text-decoration:none;
background-color:transparent;
width:20%;
height:10%;
position:absolute;
top:1%;
left:80%;
right:20%;
bottom:99%;
max-width:20%;
max-height:10%;
min-width:20%;
min-height:10%;
}
.mensaje
{
font-family:tahoma;
font-size:16px;
color:gray;
background-color:transparent;
width:100%;
height:100%;
position:absolute;
top:20%;
left:20%;
right:80%;
bottom:80%;
}
.cambiar_pagina
{
border:0px;
color:gray;
background-color:transparent;
width:100%;
height:10%;
position:absolute;
top:17%;
left:71%;
right:29%;
bottom:83%;
}
.opcion1
{
position:absolute;
top:0%;
bottom:100%;
left:20%;
right:80%;
border:2.0px solid #5F86F9;
height:15%;
width:25%;
}
.opcion2
{
position:absolute;
top:0%;
bottom:100%;
left:50%;
right:50%;
border:2.0px solid #5F86F9;
height:15%;
width:25%;
}
#consultar
{
position:absolute;
left:30%;
right:70%;
color:white;
background-color:#5F86F9;
}
#ir
{
color:white;
background-color:#5F86F9;
}
#anio,#mes,#pagina
{
color:white;
background-color:silver;
}
#documento
{
background-color:#F4F4EE;
border:3px solid #5F86F9;
width:75%;
height:120%;
position:absolute;
top:5%;
left:10%;
right:90%;
bottom:95%;
padding-bottom:0px;
}
</style>

<script language="javascript">

//funciones

//Validar datos
function comprobar(f)
{
   var anio = f.anio.value;
   var mes = f.mes.value;
   var fecha_actual=new Date();
   var anio_actual=fecha_actual.getFullYear();	
   if (anio <1999 || anio>anio_actual || mes=="")
   {
      alert("Se ingreso de datos incorrectos.");
      return false;
   }
   
   return true;
}
function validar_pagina(f)
{
var pagina=f.pagina.value;
numero_pagina=parseInt(pagina);
f.pagina.value=numero_pagina;
if(isNaN(numero_pagina))
{
f.pagina.value="";
numero_pagina=parseInt(pagina);
}
if(numero_pagina<=0 || isNaN(numero_pagina)==true)return false;
return true;
}
</script>
</head>
<!--Los que no pagaron-->
<body>
<div id="documento">
<br><fieldset class="opcion1"><legend style="color:red">NO</legend>
<form action="pagos.php" method="post" name="formu" id="formu" onsubmit="return comprobar(this)">
   <label for="aniolb">Anio:</label> <input type="text" name="anio" id="anio" value=""><br>
   <label for="meslb">Mes:</label>   <select name="mes" id="mes">
           <option selected></option>
	    <option value="1">Enero</option>
            <option value="2">Febrero</option>
            <option value="3">Marzo</option>
	    <option value="4">Abril</option>	   
	    <option value="5">Mayo</option> 	
	    <option value="6">Junio</option>
	    <option value="7">Julio</option>
	    <option value="8">Agosto</option>
	    <option value="9">Septiembre</option>
	    <option value="10">Octubre</option>
	    <option value="11">Noviembre</option>
	    <option value="12">Diciembre</option>
	  </select><br><br>
	<input type="hidden" name="opcion" value="1">	
         <input type="submit" id="consultar" value="   Consultar   ">
</form></fieldset>
<!--Los que pagaron-->
<br><fieldset class="opcion2"><legend style="color:green">SI</legend>
<form action="pagos.php" method="post" name="formu" id="formu" onsubmit="return comprobar(this)">
   <label for="aniolb">Anio:</label> <input type="text" name="anio" id="anio" value=""><br>
   <label for="meslb">Mes:</label>   <select name="mes" id="mes">
            <option selected></option>
	    <option value="1">Enero</option>
            <option value="2">Febrero</option>
            <option value="3">Marzo</option>
	   <option value="4">Abril</option>	   
	   <option value="5">Mayo</option> 	
	   <option value="6">Junio</option>
	   <option value="7">Julio</option>
	   <option value="8">Agosto</option>
	   <option value="9">Septiembre</option>
	   <option value="10">Octubre</option>
	   <option value="11">Noviembre</option>
	   <option value="12">Diciembre</option>
	  </select><br><br>
       	 <input type="hidden" name="opcion" value="0">
         <input type="submit" id="consultar" value="   Consultar   ">
</form></fieldset>
<!--Seleccion de pagina a ver-->
<fieldset class="cambiar_pagina">
<form action="pagos.php" method="post" name="seleccion_pagina" id="seleccion_pagina" onsubmit="return validar_pagina(this)">
<?php
//Variables auxiliares
$inicio=1;
$limite=15;
if(isset($_POST['pagina']))
	{
//Para primera carga
$pagina_actual=$_POST['pagina'];
echo '<label for="ir" name="irlb" style="padding:5.5px;margin:3px;position:absolute;top:60%;left:1%;right:99%;width:100%"> Numero pagina actual: '.$pagina_actual.'</label><br>';
	}
else{
$pagina_actual=1;
echo '<label for="ir" name="irlb" style="padding:5.5px;margin:3px;position:absolute;top:60%;left:1%;right:99%;width:100%"> Numero pagina actual: '.$pagina_actual.'</label><br>';
    }
?>
<label for="pagina">Numero Paginas: <?php
//Ayuda form pagina
if (isset($_POST['anio']) && isset($_POST['mes'])) 		{
							//lectura form consulta
   							$anio=$_POST['anio'];
							$mes=$_POST['mes'];
							$opcion=$_POST['opcion'];
//Uso fichero auxiliar
manegar_fichero_aux($opcion,$mes,$anio,$inicio,$limite,0);
//Fin fichero auxiliar
							       }//Fin form consulta
								else{
//Conexion auxiliar  
manegar_fichero_aux($opcion,$mes,$anio,$inicio,$limite,1); 
//Fin conexion auxiliar
									}//Inactividad form consulta				       
cantidad_paginas($opcion,$mes,$anio,$inicio,$limite); //Cantidad paginas
//Fin ayuda form pagina
?></label>
<input type="text" name="pagina" id="pagina" value="" size="9">
<input type="submit" id="ir" value="Ir">
</form>
</fieldset>
<!--Fin seleccion pagina-->
<div id="consulta">
<table>
<tr style="color:white;background-color:#5F86F9;"><td>ID</td><td>Nombre</td><td>Apellido</td><td>Razon Social</td><td>Monto</td></tr>
<?php
//validacion datos
if(isset($_POST['opcion']))
{
if($_POST['opcion']==1)echo '<p class="mensaje" id="mensaje">Los que No pagaron en el mes : '.$_POST['mes']." anio : ".$_POST['anio'].'</p>';
else if($_POST['opcion']==0)echo '<p class="mensaje" id="mensaje">Los que pagaron en el mes : '.$_POST['mes']." anio : ".$_POST['anio'].'</p>';
}
else
{
//Conexion auxiliar  
manegar_fichero_aux($opcion,$mes,$anio,$inicio,$limite,1); 
//Fin conexion auxiliar
if($opcion==1)echo '<p class="mensaje" id="mensaje">Los que No pagaron en el mes : '.$mes." anio : ".$anio.'</p>';
else if($opcion==0)echo '<p class="mensaje" id="mensaje">Los que pagaron en el mes : '.$mes." anio : ".$anio.'</p>';
}
?>
<?php
if (isset($_POST['anio']) && isset($_POST['mes'])) 		{
							//lectura form consulta
   							$anio=$_POST['anio'];
							$mes=$_POST['mes'];
							$opcion=$_POST['opcion'];
//Uso fichero auxiliar
manegar_fichero_aux($opcion,$mes,$anio,$inicio,$limite,0);
//Fin fichero auxiliar
//Consulta desde form princ
conectar($opcion,$mes,$anio,$inicio,$limite);
//Fin consulta form princ

							       }//Fin form consulta
								else{
//Conexion auxiliar  
manegar_fichero_aux($opcion,$mes,$anio,$inicio,$limite,1); 
conectar($opcion,$mes,$anio,$inicio,$limite);
//Fin conexion auxiliar
									}//Inactividad form consulta				       
?>
</table>
</div>
<a class="informe" href="informe_pagos.php"><img style="position:absolute;top:50%;bottom:50%;left:40%;right:60%;" src="../imagenes/pagos.png">Informe Completo</a>
<a style="text-decoration:none;color:gray;position:absolute;top:1%;bottom:99%;left:1%;right:99%;" href="buscarcliente.php"><img src="../imagenes/salir.png"><br>Salir</a>
<?php
//Funciones
//Funcion conexion bd
function conectar($opcion,$mes,$anio,$inicio,$limite)
{
//Variables L
$total=0.0;
$cantidad=0;
//Conexion base de datos
$conexion_aux=mysql_connect("localhost","root","gmg365ca");//aux conexion bd
mysql_select_db("bdgestionadmin",$conexion_aux);//aux seleccion bd
$conexion=mysql_connect("localhost","root","gmg365ca") or
  die("Problemas en la conexion");
mysql_select_db("bdgestionadmin",$conexion) or
  die("Problemas con la base de datos");
switch($opcion){
case 1:
$registros=mysql_query("SELECT cliente.id_cliente,cliente.nombre,cliente.apellido,cliente.razonsocial,cliente.baja,pagoabono.periodo,pagoabono.pago FROM cliente,pagoabono WHERE NOT EXISTS(SELECT periodo,pago FROM pagoabono where month(periodo)=$mes and year(periodo)=$anio and cliente.id_cliente=pagoabono.id_cliente) HAVING (cliente.baja=0) LIMIT $inicio,$limite;", $conexion) or die("Problemas en la seleccion".mysql_error());
break;
case 0:
$registros=mysql_query("SELECT cliente.id_cliente,cliente.nombre,cliente.apellido,cliente.razonsocial,cliente.baja,pagoabono.periodo,pagoabono.pago FROM cliente,pagoabono where cliente.id_cliente=pagoabono.id_cliente HAVING (year(periodo)=$anio and month(periodo)=$mes) LIMIT $inicio,$limite;", $conexion) or die("Problemas en la seleccion".mysql_error());
break;
		}

//Inicio recorrido BD
while ($reg=mysql_fetch_array($registros))
{
//Para Pago
$id_cliente=$reg['id_cliente'];
$registro_aux=mysql_query("SELECT pagoabono.pago,pagoabono.periodo FROM pagoabono WHERE month(periodo)=$mes and year(periodo)=$anio and id_cliente=$id_cliente;",$conexion_aux);
$reg_aux=mysql_result($registro_aux,0);
$pago=$reg_aux;
//Fin PP
if($reg['baja']==0){
echo '<tr style="background-color:#F0EFD8;color:gray;"><td>'.$id_cliente.'</td><td>'.$reg['nombre'].'</td><td>'.$reg['apellido'].'</td><td>'.$reg['razonsocial'].'</td><td>'.$pago.'</td></tr>';
	           }
else
{
echo '<tr style="background-color:#D89E5C;color:gray;"><td>'.$reg['id_cliente'].'</td><td>'.$reg['nombre'].'</td><td>'.$reg['apellido'].'</td><td>'.$reg['razonsocial'].'</td><td>'.$pago.'</td></tr>';
}
$total=$total+$pago;
$cantidad=$cantidad+1;                   	
}
//Fin recorrido BD
//Para total
$total=number_format($total);
echo '<tr><td></td><td></td><td></td><td class="total">Cantidad Clientes: '.$cantidad.'</td><td class="total">Total: '.$total.'</td></tr>';
mysql_close($conexion_aux);
mysql_close($conexion);
//Cierre conexion base de datos
}
//Funcion cantidad paginas
function cantidad_paginas($opcion,$mes,$anio,$inicio,$limite)
{
//Conexion base de datos
$conexion=mysql_connect("localhost","root","gmg365ca") or
  die("Problemas en la conexion");
mysql_select_db("bdgestionadmin",$conexion) or
  die("Problemas con la base de datos");
switch($opcion){
case 1:
$registros=mysql_query("SELECT cliente.id_cliente,cliente.nombre,cliente.apellido,cliente.razonsocial,cliente.baja,pagoabono.periodo,pagoabono.pago FROM cliente,pagoabono WHERE NOT EXISTS(SELECT periodo,pago FROM pagoabono where month(periodo)=$mes and year(periodo)=$anio and cliente.id_cliente=pagoabono.id_cliente) HAVING (cliente.baja=0);", $conexion) or die("Problemas en la seleccion".mysql_error());
break;
case 0:
$registros=mysql_query("SELECT cliente.id_cliente,cliente.nombre,cliente.apellido,cliente.razonsocial,cliente.baja,pagoabono.periodo,pagoabono.pago FROM cliente,pagoabono where cliente.id_cliente=pagoabono.id_cliente HAVING (year(periodo)=$anio and month(periodo)=$mes);", $conexion) or die("Problemas en la seleccion".mysql_error());
break;
		}
//Inicio recorrido BD
$numero_registros=0;
while ($reg=mysql_fetch_array($registros))
{
$numero_registros=$numero_registros+1;
}
//Fin recorrido BD
$cantidad_paginas=ceil($numero_registros/$limite);
echo $cantidad_paginas."";
mysql_close($conexion);
//Cierre conexion base de datos
}
function manegar_fichero_aux(&$opcion,&$mes,&$anio,&$inicio,&$limite,$decision)
{
if($decision==1)
	{
    $consulta= simplexml_load_file("pagos_temp.xml"); //Carga fichero auxiliar
    $opcion=$consulta->opcion;
    $mes=$consulta->mes;
    $anio=$consulta->anio;	
if(isset($_POST['pagina']))$inicio=(($_POST['pagina']*$limite)-$limite);
else{$inicio=0;}
   $limite=15;
	}
else
	{
$fichero =fopen("pagos_temp.xml","w");
fputs($fichero,"<consulta>");
fputs($fichero,"<opcion>".$opcion."</opcion>");
fputs($fichero,"<mes>".$mes."</mes>");
fputs($fichero,"<anio>".$anio."</anio>");
fputs($fichero,"</consulta>");
fclose($fichero);
	}
}
?>
</div>
</body>
</html>
