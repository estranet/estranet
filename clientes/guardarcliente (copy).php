<?php require_once('../Connections/gestionAdmin.php'); ?>
<?php 
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

 $fecha=explode("/",$_POST['fechaAlta']);
 $fecha=$fecha[2]."-".$fecha[1]."-".$fecha[0];
 $insertSQL = sprintf("INSERT INTO cliente (nombre, apellido, razonsocial, DNI, cuit, domicilio,domicilio_latitud,domicilio_longitud, barrio, ciudad, provincia, fechaAlta, CP, telf, otros,fultimo) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s,%s)",
                       GetSQLValueString($_POST['nombre'], "text"),
                       GetSQLValueString($_POST['Apellido'], "text"),
                       GetSQLValueString($_POST['Razonsocial'], "text"),
                       GetSQLValueString($_POST['dni'], "text"),
                       GetSQLValueString($_POST['cuit'], "text"),
                       GetSQLValueString($_POST['Domicilio'], "text"),
                       GetSQLValueString($_POST['latitud'], "text"),
		       GetSQLValueString($_POST['longitud'], "text"),
		       GetSQLValueString($_POST['barrio'], "text"),
                       GetSQLValueString($_POST['Ciudad'], "text"),
                       GetSQLValueString($_POST['id_provincia'], "int"),
                       GetSQLValueString($fecha, "date"),
                       GetSQLValueString($_POST['co'], "text"),
					   GetSQLValueString($_POST['telf'], "text"),
					   GetSQLValueString($_POST['otros'], "text"),
					   GetSQLValueString(date("Y-m-d H:i:s"), "date"));

  mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
  $id_cliente=mysql_insert_id($gestionAdmin);
  $insertSQL = sprintf("INSERT INTO cuenta (id_cliente, finaciacion, abonomensual,id_servicio, costoInstlacion, pagoc) VALUES (%s, %s, %s , %s, %s, %s)",
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString($_POST['cuotas'], "text"),
                       GetSQLValueString($_POST['abono'], "double"),
					   GetSQLValueString($_POST['id_servicio'], "int"),
					   GetSQLValueString($_POST['costo'],"float"),
   					   GetSQLValueString("1","float"));

  mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
  $insertSQL = sprintf("INSERT INTO fichatecnica (id_cliente, ipcliente, nodo, ipantena, macap, esid, mactarjetared) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString($_POST['ipcliente'], "text"),
                       GetSQLValueString($_POST['nodo'], "text"),
                       GetSQLValueString($_POST['ipantena'], "text"),
                       GetSQLValueString($_POST['macap'], "text"),
                       GetSQLValueString($_POST['esid'], "text"),
                       GetSQLValueString($_POST['makred'], "text"));
  mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
//codigo by andres,insert log:
 
$updateSQL=sprintf("INSERT INTO log(id_usuario,descripcion) VALUES(%s,%s)", GetSQLValueString($_POST['id_usuario'], "int"), GetSQLValueString('inserto nuevo cliente: '.$_POST['Apellido'].' '.$_POST['nombre'], "text"));
 mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($updateSQL, $gestionAdmin) or die(mysql_error());
//fin codigo andres


  if($_POST['oi']==0){
     //Genero Orden de Instalacion
	   $insertSQL = sprintf("INSERT INTO ordeninstalacion (id_cliente, fecha) VALUES (%s, %s)",
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString($fecha, "date"));
	   mysql_select_db($database_gestionAdmin, $gestionAdmin);
       $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());			
	   $id_orden=mysql_insert_id($gestionAdmin);	   
	 header(sprintf("location: imprimiroi.php?id_cliente=%s&id_orden=%s",$id_cliente,$id_orden));			   
  }else{
      header(sprintf("location: editarcliente.php?id_cliente=%s",$id_cliente));
  }
?>
