<?php
/**
 * Codigo para borrar y recuperar xml - por andres tejerina
 */
if(isset($_GET["reinicio"]) and $_GET["reinicio"] == 1){
	   $fichero = fopen('pagos_temp.xml', 'w+');
	    fputs($fichero,"<consulta>");
		fputs($fichero,"<opcion>".'0'."</opcion>");
		fputs($fichero,"<mes>1</mes>");
		fputs($fichero,"<anio>2022</anio>");
		fputs($fichero,"<criterio>dia</criterio>");
		fputs($fichero,"<tipo>ASC</tipo>");
		fputs($fichero,"</consulta>");
		fclose($fichero);
		header("Location: ./informe_pagos.php");
   }
	/**
	 * Fin codigo Andrés
	 */
?>


<html lang="es" xml:lang="es" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Informe Pagos Estranet WISP</title>
<meta name="viewport" content="width=100%,initial-scale=0.5,user-scalable=yes">
<style type="text/css">
#documento
{
border:3px;
padding:3px;
width:900px;
height:400px;
position:absolute;
top:1%;
left:20%;
right:80%;
bottom:98%;
}
table
{
width:90%;
height:1%;
max-width:100%;
max-height:1%;
min-width:1%;
min-height:1%;
border:3px solid #5F86F9;
position:absolute;
left:1%;
right:99%;
top:230px;
bottom:70%;
}
tr
{
width:100%;
height:1%;
background-color:#E0D6D6;
color:black;
}
td
{
width:1%;
height:1%;
}
.cambiar_pagina
{
list-style-type:none;
color:white;
width:100%;
position:absolute;
top:110px;
left:630px;
}
#encabezado_resultado
{
width:818px;
height:42px;
background-color:#5F86F9;
position:absolute;
left:8px;
top:185px;
}
.mensaje
{
font-family:tahoma;
font-size:16px;
color:black;
background-color:transparent;
position:absolute;
top:0px;
left:300px;
}
#barra
{
color:white;
background-color:#F1F0F0;
border:3px solid #5F86F9;
margin-left:0px;
margin-right:0px;
margin-top:3px;
margin-bottom:3px;
width:811px;
height:180px;
position:relative;
top:10px;
left:5px;
}
#barra_acciones
{
list-style-type:none;
background-color:transparent;
display:block;
margin-top:3px;
margin-bottom:3px;
width:300px;
position:absolute;
top:20px;
left:570px;
}
li
{
margin-left:10px;
margin-right:3px;
padding-left:3px;
padding-right:3px;
float:left;
}
a:link,a:active,a:hover,a:visited
{
text-decoration:none;
color:black;
font-family:tahoma;
font-size:12px;
}
#consultar
{
color:gray;
background-color:transparent;
position:absolute;
left:60px;
}
#mes,#anio
{
color:black;
background-color:white;
}
#aniolb,#meslb
{
color:gray;
}
#consulta_p
{
width:200px;
height:30px;
position:absolute;
left:400px;
top:10px;
}
body
{
width:700px;
height:480px;
}
</style>
<script language="javascript">
//imprimir informe
function imprSelec(consulta)
{
var ficha=document.getElementById("consulta");
var ventimp=window.open(' ','popimpr');
ventimp.document.write(ficha.innerHTML);
ventimp.document.close();
ventimp.print();
ventimp.close();
}
//Validar datos
function comprobar(f)
{
   var anio = f.anio.value;
   var mes = f.mes.value;
   var fecha_actual=new Date();
   var anio_actual=fecha_actual.getFullYear();	
   if (anio <1999 || anio>anio_actual || mes=="")
   {
      alert("Se ingreso de datos incorrectos.");
      return false;
   }
   
   return true;
}
function informar_servicios()
{
window.open('informe_servicios.php','Informacion Servicios','width=300, height=400');
}
function imprGraf1()
{
window.open('informe_grafico1.php','informe_grafico1','width=1000, height=740');
}
function imprGraf2()
{
window.open('informe_grafic2.php','informe_grafico2','width=1000, height=740');
}
</script>
</head>
<body>
<div id="documento">
<div id="barra">
<!--Consulta personalizada-->
<div id="consulta_p">
<fieldset style="background-color:transparent;position:relative;top:1%">
<form action="informe_pagos.php" method="post" name="formu" id="formu" onsubmit="return comprobar(this)">
   <label for="anio" id="aniolb">Año:</label> 
<select name="anio" id="anio">
<!--Generar años-->
<?php obtener_anios();

?>
</select><br>
   <br><label for="mes" id="meslb">Mes:</label>   <select name="mes" id="mes">
            <option selected></option>
	    <option value="1">Enero</option>
            <option value="2">Febrero</option>
            <option value="3">Marzo</option>
	   <option value="4">Abril</option>	   
	   <option value="5">Mayo</option> 	
	   <option value="6">Junio</option>
	   <option value="7">Julio</option>
	   <option value="8">Agosto</option>
	   <option value="9">Septiembre</option>
	   <option value="10">Octubre</option>
	   <option value="11">Noviembre</option>
	   <option value="12">Diciembre</option>
	  </select>
       	 <br><br><input type="submit" id="consultar" value="   Consultar   ">
    	<button onclick='location.href="./informe_pagos.php?reinicio=1"' style="z-index:1000;width:50%;margin:1px;padding:1px;position:absolute;top:60px;left:130%;background-color:#C0C15D;color:white">Restablecer</button>

</form>
</fieldset>
</div>
<ul id="barra_acciones">
<li><a href="informe_grafico1.php" onclick="javascript:imprGraf1()" target="informe_grafico1"><img src="../imagenes/grafico2.png" alt="Grafico1"></a></li>
<li><a href="informe_grafico2.php" onclick="javascript:imprGraf2()" target="informe_grafico2"><img src="../imagenes/grafico2.png" alt="Grafico2"></a></li>
<li style="margin-left:6px"><a href="buscarcliente.php"><img src="../imagenes/volver.png" alt="Volver">
</a></li>
</ul>
</div>
<div id="consulta">
<?php 
//Variables
$o=0;
$m=0;
$a=0;
$ini=0;
$lim=30;
$criterio='dia';
$tipo='ASC';
//Generacion Informe ?>
<table style="background-color:#F1F0F0;border:5px solid #5F86F9">
<tr style="background-color:silver;border:5px solid gray">
<td><a href="informe_pagos.php?opcion=id_cliente&tipo=ASC">Nro</a></td>
<td><a href="informe_pagos.php?opcion=apellido&tipo=ASC">Apellido</a></td>
<td><a href="informe_pagos.php?opcion=nombre&tipo=ASC">Nombre</a></td>
<td><a href="informe_pagos.php?opcion=razonsocial&tipo=ASC">Razon Social</a></td>
<td><a href="informe_pagos.php?opcion=dia&tipo=ASC">Dia</a></td>
<td><a href="informe_pagos.php?opcion=hora&tipo=ASC">Hora</a></td>
<td><a href="#">Monto</a></td></tr>
<?php
//Tipo Ordenamiento Datos
if(isset($_GET['opcion']) || isset($_GET['tipo']))
{
$criterio=$_GET['opcion'];
$tipo=$_GET['tipo'];
}
else
{
actualizar_criterio_tipo($criterio,$tipo);
}
if(isset($_POST['anio']) && isset($_POST['mes']))//Carga P
{
cargar_fecha(1,$criterio,$tipo);
}
else{
		if(isset($_GET['opcion']) && isset($_GET['tipo']) || isset($_GET['accion']) && isset($_GET['posi']) || isset($_GET['dia']))cargar_fecha(2,$criterio,$tipo);
		else{cargar_fecha(0,$criterio,$tipo);}
   }//Primera carga
cargar_datos($o,$m,$a,$criterio,$tipo);
//Mensaje Descripcion Fecha
?><div id="encabezado_resultado">
<?php
generar_mensaje($m,$a);
?>
</div>
<?php
//Pagina Siguiente o Anterior
if (isset($_GET['accion'])){	
				
//Valida AV
if((($_GET['posi']+$lim)/$lim)>=cantidad_paginas($o,$m,$a,$lim) && $_GET['accion']==1)
{
$ini=0;
generar_informe($o,$m,$a,$ini,$lim,$criterio,$tipo);
}
else{
				$accion=$_GET['accion'];
				switch($accion){

				case 1:
				$ini=$_GET['posi']+$lim;				
				generar_informe($o,$m,$a,$ini,$lim,$criterio,$tipo);
				break;				
				case 0:
				$ini=$_GET['posi'];
				if($ini>=$lim)$ini=$ini-$lim;				
				generar_informe($o,$m,$a,$ini,$lim,$criterio,$tipo);
				break;
				case 2:
				$ini=0;
				generar_informe($o,$m,$a,$ini,$lim,$criterio,$tipo);				
				break;
				case 3:
				$ini=(cantidad_paginas($o,$m,$a,$lim)*$lim-$lim);
				generar_informe($o,$m,$a,$ini,$lim,$criterio,$tipo);
				break;
				default:
				break;
			                        }  
	}
			    }
else if(!isset($_GET['accion']))
    {
generar_informe($o,$m,$a,$ini,$lim,$criterio,$tipo);
    }//Caso Comun 
//Para Nav
$nav=(($ini+$lim)/$lim);
echo '<p style="color:gray;position:absolute;top:135px;left:75%">'.$nav.'/ '.cantidad_paginas($o,$m,$a,$lim).' Paginas</p>'; //Cantidad paginas
//Fin Generacion Informe
//Funciones
function cargar_datos(&$o,&$m,&$a,&$criterio,&$tipo)
{
$consulta= simplexml_load_file("pagos_temp.xml"); //Carga fichero auxiliar
    $o=$consulta->opcion;
    $m=$consulta->mes;
    $a=$consulta->anio;
    $criterio=$consulta->criterio;
    $tipo=$consulta->tipo;		
}
function generar_informe($o,$m,$a,$ini,$lim,$criterio,$tipo)
{
//variables locales
$total=0.0;
$cantidad=0;
$num_cliente=0;
//Conexion base de datos
$conexion_aux=mysql_connect("localhost","root","Exl786to.") or die ("Problema conexion aux");
mysql_select_db("estranet",$conexion_aux) or die ("Problema con bd");
$conexion=mysql_connect("localhost","root","Exl786to.") or
  die("Problemas en la conexion");
mysql_select_db("estranet",$conexion) or
  die("Problemas con la base de datos");

$registros=mysql_query("SELECT cliente.id_cliente, cliente.nombre, cliente.apellido, cliente.razonsocial, cliente.baja,recibo.fecha,day(fecha)as dia,time(fecha)as hora,recibo.id_recibo
FROM cliente, recibo
WHERE cliente.id_cliente = recibo.id_cliente
HAVING (
year( fecha ) =$a
AND month( fecha ) =$m
)
ORDER BY $criterio,hora $tipo LIMIT $ini,$lim;", $conexion) or die("Problemas en la seleccion".mysql_error());
//Inicio recorrido BD
while ($reg=mysql_fetch_array($registros))
{$pago=0;
if($reg['baja']==0){
$id_cliente=$reg['id_cliente'];
$id_recibo=$reg['id_recibo'];
$registros_aux=mysql_query("SELECT recibo_detalle.preciopag FROM recibo_detalle WHERE recibo_detalle.id_cliente=$id_cliente and recibo_detalle.id_recibo=$id_recibo;",$conexion_aux) or die("Problema en la Selec".mysql_error());
		while ($reg_aux=mysql_fetch_array($registros_aux))
		{
		 $pago=$pago+$reg_aux['preciopag'];				
		}mysql_free_result($conexion_aux);//PRU
//Para adicionales
if($pago==0)
{
$registros_aux=mysql_query("SELECT recibodr.preciopag FROM recibodr WHERE recibodr.id_cliente=$id_cliente and recibodr.id_recibo=$id_recibo;",$conexion_aux) or die("Problema en la Selec".mysql_error());

	while ($reg_aux=mysql_fetch_array($registros_aux))
		{
		 $pago=$pago+$reg_aux['preciopag'];				
		}
}
//Fin adicionales
$dia=$reg['dia'];
$hora=$reg['hora'];
$num_cliente=$num_cliente+1;//Numeracion cliente
echo '<tr><td><a href="informe_servicios.php?cliente='.$id_cliente.'" target="Informacion Servicios" onclick="javascript:informar_servicios()">'.$num_cliente.'</a></td><td style="width:30%;height:1%">'.$reg['apellido'].'</td><td style="width:30%;height:1%">'.$reg['nombre'].'</td><td style="width:30%;height:1%">'.$reg['razonsocial'].'</td><td><a href="informe_pagos.php?dia='.$dia.'">'.$dia.'</a></td><td>'.$hora.'</td><td>'.$pago.'</td></tr>';
$total=$total+$pago;
$cantidad=$cantidad+1;           
         	   }	
}
//Fin recorrido BD
//Para total
$total=number_format($total);
echo '<tr><td></td><td></td><td></td><td></td><td></td><td style="background-color:yellow">Cantidad Clientes: '.$cantidad.'</td><td style="background-color:yellow">Total: '.$total.'</td></tr>';
mysql_close($conexion);
mysql_close($conexion_aux);
//Cierre conexion base de datos
}
//Funcion cantidad paginas
function cantidad_paginas($o,$m,$a,$lim)
{
//Conexion base de datos
$conexion=mysql_connect("localhost","root","Exl786to.") or
  die("Problemas en la conexion");
mysql_select_db("estranet",$conexion) or
  die("Problemas con la base de datos");

$registros=mysql_query("SELECT cliente.id_cliente, cliente.nombre, cliente.apellido, cliente.razonsocial, cliente.baja,recibo.fecha,day(fecha)as dia,time(fecha)as hora,recibo.id_recibo
FROM cliente, recibo
WHERE cliente.id_cliente = recibo.id_cliente
HAVING (
year( fecha ) =$a
AND month( fecha ) =$m
)
;", $conexion) or die("Problemas en la seleccion".mysql_error());

//Inicio recorrido BD
$numero_registros=0;
while ($reg=mysql_fetch_array($registros))
{
$numero_registros=$numero_registros+1;
}
//Fin recorrido BD
$cantidad_paginas=ceil($numero_registros/$lim);
//echo $cantidad_paginas;
mysql_close($conexion);
//Cierre conexion base de datos
return $cantidad_paginas;
}
function informar($o,$m,$a)
{
//Conexion base de datos
$conexion_aux=mysql_connect("localhost","root","Exl786to.");//Conexion bd aux
mysql_select_db("estranet",$conexion_aux);//Seleccion bd aux
$conexion=mysql_connect("localhost","root","Exl786to.") or
  die("Problemas en la conexion");
mysql_select_db("estranet",$conexion) or
  die("Problemas con la base de datos");
$registros=mysql_query("SELECT cliente.id_cliente, cliente.nombre, cliente.apellido, cliente.razonsocial, cliente.baja,recibo.fecha,day(fecha)as dia,time(fecha)as hora,recibo.id_recibo
FROM cliente, recibo
WHERE cliente.id_cliente = recibo.id_cliente
HAVING (
year( fecha ) =$a
AND month( fecha ) =$m
)
;", $conexion) or die("Problemas en la seleccion".mysql_error());
//Inicio recorrido BD
$numero_registros=0;
$total=0.0;
while ($reg=mysql_fetch_array($registros))
{$pago=0;
$id_cliente=$reg['id_cliente'];
$id_recibo=$reg['id_recibo'];
$registros_aux=mysql_query("SELECT recibo_detalle.preciopag FROM recibo_detalle WHERE recibo_detalle.id_cliente=$id_cliente and recibo_detalle.id_recibo=$id_recibo;",$conexion_aux) or die("Problema en la Selec".mysql_error());
		while ($reg_aux=mysql_fetch_array($registros_aux))
		{
		 $pago=$pago+$reg_aux['preciopag'];				
		}mysql_free_result($conexion_aux);
//Para adicionales
if($pago==0)
{
$registros_aux=mysql_query("SELECT recibodr.preciopag FROM recibodr WHERE recibodr.id_cliente=$id_cliente and recibodr.id_recibo=$id_recibo;",$conexion_aux) or die("Problema en la Selec".mysql_error());

	while ($reg_aux=mysql_fetch_array($registros_aux))
		{
		 $pago=$pago+$reg_aux['preciopag'];				
		}
}
//Fin adicionales
$total=$total+$pago;
}
//Para total
$total=number_format($total);
//Para total Por Dia
$total_dia=0.0;
//Recepcion dia
if(isset($_GET['dia']))
{
$d=$_GET['dia'];
}
else
{
$fecha_actual=getdate();
$d=$fecha_actual["mday"];
}
$registros=mysql_query("SELECT cliente.id_cliente, cliente.nombre, cliente.apellido, cliente.razonsocial, cliente.baja,recibo.fecha,recibo.id_recibo,recibo.fpago
FROM cliente, recibo
WHERE cliente.id_cliente = recibo.id_cliente
HAVING (
year( fecha ) =$a
AND month( fecha ) =$m AND day( fecha ) =$d);",$conexion) or die("Problemas en la seleccion".mysql_error());
//Inicio recorrido BD
while ($reg=mysql_fetch_array($registros))
{$pago=0;
$pago_cta_hoy=0;
$id_cliente=$reg['id_cliente'];
$id_recibo=$reg['id_recibo'];
$fpago=$reg['fpago'];
$registros_aux=mysql_query("SELECT recibo_detalle.preciopag FROM recibo_detalle WHERE recibo_detalle.id_cliente=$id_cliente and recibo_detalle.id_recibo=$id_recibo;",$conexion_aux) or die("Problema en la Selec".mysql_error());
		while ($reg_aux=mysql_fetch_array($registros_aux))
		{
					 if($fpago>0){
				$pago_cta_hoy=$pago_cta_hoy+$reg_aux['preciopag'];
					}
		 $pago=$pago+$reg_aux['preciopag'];				
		}mysql_free_result($conexion_aux);
//Para adicionales
if($pago==0)
{
$registros_aux=mysql_query("SELECT recibodr.preciopag FROM recibodr WHERE recibodr.id_cliente=$id_cliente and recibodr.id_recibo=$id_recibo;",$conexion_aux) or die("Problema en la Selec".mysql_error());

	while ($reg_aux=mysql_fetch_array($registros_aux))
		{
					 if($fpago>0){
				$pago_cta_hoy=$pago_cta_hoy+$reg_aux['preciopag'];
					}
		 $pago=$pago+$reg_aux['preciopag'];				
		}
}
//Fin adicionales
$total_dia=$total_dia+$pago;
$total_dia_cta=$total_dia_cta+$pago_cta_hoy;
}
//Fn
//codigo andres
if($total_dia_cta==0){
	
$total_dia_contado=number_format($total_dia);
	
	}else{
	
$total_dia_contado=number_format($total_dia-$total_dia_cta);
	}

$total_dia_cta=number_format($total_dia_cta);


//fin codigo andres

$total_dia=number_format($total_dia);
//Fin PPD
//Para Cantidad
$registros=mysql_query("SELECT DISTINCT cliente.id_cliente FROM cliente,recibo WHERE cliente.id_cliente = recibo.id_cliente 
and
year( fecha ) =$a
and month( fecha ) =$m
;", $conexion) or die("Problemas en la seleccion".mysql_error());

//Inicio recorrido BD
$numero_registros=0;
while ($reg=mysql_fetch_array($registros))
{
$numero_registros=$numero_registros+1;
}
$numero_registros=number_format($numero_registros);
//Fin Cant
//Para Instalaciones
$registros=mysql_query("SELECT ordeninstalacion.id_cliente FROM ordeninstalacion
 WHERE year( fecha ) =$a
 and month( fecha ) =$m
;", $conexion) or die("Problemas en la seleccion".mysql_error());

//Inicio recorrido BD
$cantidad_instalaciones=0;
while ($reg=mysql_fetch_array($registros))
{
$cantidad_instalaciones=$cantidad_instalaciones+1;
}
$cantidad_instalaciones=number_format($cantidad_instalaciones);
//Cant Instalaciones
//Para las Bajas
$registros=mysql_query("SELECT cliente.id_cliente FROM cliente
 WHERE cliente.baja=1 and year( fechaBaja ) =$a
 and month( fechaBaja ) =$m
;", $conexion) or die("Problemas en la seleccion".mysql_error());
//Inicio recorrido BD
$cantidad_bajas=0;
while ($reg=mysql_fetch_array($registros))
{
$cantidad_bajas=$cantidad_bajas+1;
}
$cantidad_bajas=number_format($cantidad_bajas);
//Cant Bajas
//ImprR
echo'<p style="width:15%;margin:1px;padding:1px;position:absolute;top:20px;left:5%;background-color:#C0C15D;color:white">Cantidad: '.$numero_registros.'</p>';
echo'<p style="width:15%;margin:1px;padding:1px;position:absolute;top:20px;left:20%;background-color:#C0C15D;color:white">$: '.$total.'</p>';
echo'<p style="width:30%;margin:1px;padding:1px;position:absolute;top:50px;left:5%;background-color:#897595;color:white">Recaudacion '.$d.'/'.$m.' <br>Efectivo: $'.$total_dia_contado.'&nbsp&nbsp Cuenta:$'.$total_dia_cta.'</p>';
echo'<p style="width:10%;margin:1px;padding:1px;position:absolute;top:110px;left:5%;background-color:#7C9575;color:white">'.'altas '.$cantidad_instalaciones.'</p>';
echo'<p style="width:10%;margin:1px;padding:1px;position:absolute;top:150px;left:5%;background-color:#927E4F;color:white">'.'bajas '.$cantidad_bajas.'</p>';

?>
<?php
mysql_close($conexion);
mysql_close($conexion_aux);
}
function cargar_fecha($opcion,$criterio,$tipo)
{
if($opcion==0){ 
  $fecha=getdate();
  $anio_actual=$fecha["year"];
  $mes_actual=$fecha["mon"];
		}
else if($opcion==1){
$anio_actual=$_POST['anio'];
$mes_actual=$_POST['mes'];
                   }
else if($opcion==2)
{

$consulta=simplexml_load_file("pagos_temp.xml"); //Carga fichero auxiliar
$anio_actual=$consulta->anio;
$mes_actual=$consulta->mes;
}
$fichero =fopen("pagos_temp.xml","w");
fputs($fichero,"<consulta>");
fputs($fichero,"<opcion>".'0'."</opcion>");
fputs($fichero,"<mes>".$mes_actual."</mes>");
fputs($fichero,"<anio>".$anio_actual."</anio>");
fputs($fichero,"<criterio>".$criterio."</criterio>");
fputs($fichero,"<tipo>".$tipo."</tipo>");
fputs($fichero,"</consulta>");
fclose($fichero);
}
function obtener_anios()
{
$fecha=getdate();
$anio_actual=$fecha["year"];
$anio_inicio=2000;
echo '<option selected></option>';
for($i=$anio_inicio;$i<=$anio_actual;$i++)
	{
	echo '<option value="'.$i.'">'.$i.'</option>';

	}
}
function actualizar_criterio_tipo(&$criterio,&$tipo)
{
	$consulta=simplexml_load_file("pagos_temp.xml"); //Carga fichero auxiliar
    $criterio=$consulta->criterio;
    $tipo=$consulta->tipo;	
	
}
function generar_mensaje($m,$a)
{
switch($m){
case 1:
echo '<p class="mensaje" id="mensaje"><strong style="color:white"> Enero del '.$a.'</strong></p>';
break;
case 2:
echo '<p class="mensaje" id="mensaje"><strong style="color:white"> Febrero del '.$a.'</strong></p>';
break;
case 3:
echo '<p class="mensaje" id="mensaje"><strong style="color:white"> Marzo del '.$a.'</strong></p>';
break;
case 4:
echo '<p class="mensaje" id="mensaje"><strong style="color:white"> Abril del '.$a.'</strong></p>';
break;
case 5:
echo '<p class="mensaje" id="mensaje"><strong style="color:white"> Mayo del '.$a.'</strong></p>';
break;
case 6:
echo '<p class="mensaje" id="mensaje"><strong style="color:white"> Junio del '.$a.'</strong></p>';
break;
case 7:
echo '<p class="mensaje" id="mensaje"><strong style="color:white"> Julio del '.$a.'</strong></p>';
break;
case 8:
echo '<p class="mensaje" id="mensaje"><strong style="color:white"> Agosto del '.$a.'</strong></p>';
break;
case 9:
echo '<p class="mensaje" id="mensaje"><strong style="color:white"> Septiembre del '.$a.'</strong></p>';
break;
case 10:
echo '<p class="mensaje" id="mensaje"><strong style="color:white"> Octubre del '.$a.'</strong></p>';
break;
case 11:
echo '<p class="mensaje" id="mensaje"><strong style="color:white"> Noviembre del '.$a.'</strong></p>';
break;
case 12:
echo '<p class="mensaje" id="mensaje"><strong style="color:white"> Diciembre del '.$a.'</strong></p>';
break;
default:
echo 'Error en fecha ingresada';
break;
	}
}
?>
</table>
</div>
<ul class="cambiar_pagina">
<?php
echo '<li><a style="background-color:#5F86F9;color:white" href="informe_pagos.php?accion=2&posi='.$ini.'"><<</a></li>';
echo '<li><a style="background-color:#5F86F9;color:white" href="informe_pagos.php?accion=0&posi='.$ini.'"><</a></li>';
echo '<li><a style="background-color:#5F86F9;color:white" href="informe_pagos.php?accion=1&posi='.$ini.'">></a></li>';
echo '<li><a style="background-color:#5F86F9;color:white" href="informe_pagos.php?accion=3&posi='.$ini.'">>></a></li>';
?>
</ul>
<?php
informar($o,$m,$a);
?>
</div>
</body>
</html>
