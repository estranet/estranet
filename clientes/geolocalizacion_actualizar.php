<?php 
//Conexion base de datos
$id_cliente=$_GET['id_cliente'];
$conexion = new PDO('mysql:host=localhost;dbname=estranet;charset=utf8', 'root', 'Exl786to.');
$consulta= $conexion->query('SELECT * FROM cliente WHERE id_cliente='.$id_cliente);
$result = $consulta->fetch(PDO::FETCH_ASSOC);
$nombre=$result['nombre'];
$apellido=$result['apellido'];
$direccion=$result['direccion'];
//Para Guardar cambios
if(isset($_POST['guardar']))
{
$lat=$_POST['latitud'];
$long=$_POST['longitud'];
$latitud=$_POST['latitud'];
$longitud=$_POST['longitud'];
$id_cli=$_GET['id_cliente'];
if (guardar($lat,$long,$id_cli)>0)echo "Guardado con exito";
}
else
{
if($result['domicilio_latitud']!=0||$result['domicilio_longitud']!=0)
	{
$latitud=$result['domicilio_latitud'];
$longitud=$result['domicilio_longitud'];
	}
	else
	{
	$latitud= "-29.4133725";
	$longitud="-66.8523035";
	}
}
// INICIALIZO LAS VARIABLES 
$zoom= "17";
$tipo_mapa = "ROADMAP";

if (isset($_GET["direccion"])) $direccion=  urldecode ($_GET["direccion"]);

// LONGITUD Y LATITUD SI ESTAN COMO PARAMETROS SE TOMAN
if (isset($_GET["lon"])) $longitud= $_GET["lon"];
if (isset($_GET["lat"])) $latitud= $_GET["lat"];

// ZOOM ENTRE 0 y 19
if (isset($_GET["zoom"])) $zoom= $_GET["zoom"];
if (($zoom<=0) || ($zoom>=20)){ $zoom= "17";}
// GUARDO EN LA BASE DE DATOS
function guardar($latitud,$longitud,$id_cliente)
			{
$conexion = new PDO('mysql:host=localhost;dbname=estranet;charset=utf8', 'root', 'Exl786to.');
$consulta = $conexion->prepare("UPDATE cliente SET domicilio_latitud=?,domicilio_longitud=? WHERE id_cliente=?");
$filas_afectadas=$consulta;
$consulta->execute(array($latitud,$longitud,$id_cliente));
if ($filas_afectadas>=1)return 0;
else {return -1;}
return -2;
			}
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAsxuNAyF6y_bIELKuW846H9-BVd60h_E8'></script>
<script type="text/javascript">

// VARIABLES GLOBALES JAVASCRIPT
var geocoder;
var marker;
var latLng;
var latLng2;
var map;

// INICiALIZACION DE MAPA
function initialize() {
  geocoder = new google.maps.Geocoder();
  let latAux='<?php echo $latitud;?>';
  let LongAux='<?php echo $longitud;?>';
  latLng = new google.maps.LatLng(latAux ,LongAux);
  map = new google.maps.Map(document.getElementById('mapCanvas'), {
    zoom:<?php echo $zoom;?>,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.<?php echo $tipo_mapa;?>
  });


// CREACION DEL MARCADOR  
    marker = new google.maps.Marker({
    position: latLng,
    title: 'Arrastra el marcador si quieres moverlo',
    map: map,
    draggable: true
  });
 
 

 
// Escucho el CLICK sobre el mama y si se produce actualizo la posicion del marcador 
     google.maps.event.addListener(map, 'click', function(event) {
     updateMarker(event.latLng);
   });
  
  // Inicializo los datos del marcador
  //    updateMarkerPosition(latLng);
     
      geocodePosition(latLng);
 
  // Permito los eventos drag/drop sobre el marcador
  google.maps.event.addListener(marker, 'dragstart', function() {
    updateMarkerAddress('Arrastrando...');
  });
 
  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerStatus('Arrastrando...');
    updateMarkerPosition(marker.getPosition());
  });
 
  google.maps.event.addListener(marker, 'dragend', function() {
    updateMarkerStatus('Arrastre finalizado');
    geocodePosition(marker.getPosition());
  });
  

 
}


// Permito la gesti¢n de los eventos DOM
google.maps.event.addDomListener(window, 'load', initialize);

// ESTA FUNCION OBTIENE LA DIRECCION A PARTIR DE LAS COORDENADAS POS
function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('No puedo encontrar esta direccion.');
    }
  });
}

// OBTIENE LA DIRECCION A PARTIR DEL LAT y LON DEL FORMULARIO
function codeLatLon() { 
      str= document.form_mapa.longitud.value+" , "+document.form_mapa.latitud.value;
      latLng2 = new google.maps.LatLng(document.form_mapa.latitud.value ,document.form_mapa.longitud.value);
      marker.setPosition(latLng2);
      map.setCenter(latLng2);
      geocodePosition (latLng2);
      // document.form_mapa.direccion.value = str+" OK";
}

// OBTIENE LAS COORDENADAS DESDE lA DIRECCION EN LA CAJA DEL FORMULARIO
function codeAddress() {
        var address = document.form_mapa.direccion.value;
          geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
             updateMarkerPosition(results[0].geometry.location);
             marker.setPosition(results[0].geometry.location);
             map.setCenter(results[0].geometry.location);
           } else {
            alert('ERROR : ' + status);
          }
        });
      }

// OBTIENE LAS COORDENADAS DESDE lA DIRECCION EN LA CAJA DEL FORMULARIO
function codeAddress2 (address) {
          
          geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
             updateMarkerPosition(results[0].geometry.location);
             marker.setPosition(results[0].geometry.location);
             map.setCenter(results[0].geometry.location);
             document.form_mapa.direccion.value = address;
           } else {
            alert('ERROR : ' + status);
          }
        });
      }

function updateMarkerStatus(str) {
  document.form_mapa.direccion.value = str;
}

// RECUPERO LOS DATOS LON LAT Y DIRECCION Y LOS PONGO EN EL FORMULARIO
function updateMarkerPosition (latLng) {
  document.form_mapa.longitud.value =latLng.lng();
  document.form_mapa.latitud.value = latLng.lat();
}

function updateMarkerAddress(str) {
  document.form_mapa.direccion.value = str;
}

// ACTUALIZO LA POSICION DEL MARCADOR
function updateMarker(location) {
        marker.setPosition(location);
        updateMarkerPosition(location);
        geocodePosition(location);
      }
function goToDirection() {
  alert(document.form_mapa.longitud.value)
}
</script>

</script>
</head>
<body  <?php  if ($direccion != "") { ?> onload=" codeAddress2('<?php  echo $direccion; ?>')" <?php  } ?> >
 
 

<style type="text/css">
  html { height: 100% }
  body { height: 80%; margin: 10px; padding: 0px }
  #mapCanvas { height: 100% }
</style> 


<div id="formulario">
  <center>
  <p style="border: 1px solid #CCC;background-color: #EEE;color: #999; width:450px; font-family: verdana; font-size:12px"><font color="#006600">
  Geolocalizacion Estranetsrl<br><?php echo $nombre?><br><?php echo $apellido?>
</font></p>
<button onclick="goToDirection()"  style="width: 100px;font-size: 10px;font-family: verdana;font-weight: bold;"  type="button">Ir a dirección</button>

  <form name="form_mapa" method="POST" enctype="multipart/form-data">
       <table>
        <tr>
        <td><p style="font-size: 10px;font-family: verdana;font-weight: bold;">
  	   Direccion: &nbsp;<input type="text" name="direccion" id="direccion" value="<?php echo $direccion;?>" style="width: 250px;font-size: 10px;font-family: verdana;font-weight: bold;" />
  	   &nbsp;&nbsp;<input type="button" value="Ubicar" onclick="codeAddress()">
  	</p>
  	</td>
	<td><p style="font-size: 10px;font-family: verdana;font-weight: bold;">
	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="latitud" value="<?php echo $latitud;?>" style="width: 100px;font-size: 10px;font-family: verdana;font-weight: bold;" />	    
	</p>
	</td>
	<td> <p style="font-size: 10px;font-family: verdana;font-weight: bold;">
	   &nbsp; <input type="text" name="longitud" value="<?php echo $longitud;?>" style="width: 100px;font-size: 10px;font-family: verdana;font-weight: bold;" />	
	   &nbsp;&nbsp;<input type="submit" name="guardar" value="Guardar">
<!--<input type="button" value="coordenadas" onclick="codeLatLon()">-->
	</p>
	</td>	
	</tr>
     </table> 
     </form>
   </center>       
</div> 
 
<div id="mapCanvas"></div>

</body>
</html>
