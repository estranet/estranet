<?php require_once('../../Connections/gestionAdmin.php'); ?>
<?php
$d_reg_cliente = "0";
if (isset($_GET['id_cliente'])) {
  $d_reg_cliente = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_cliente = sprintf("SELECT * FROM cliente WHERE cliente.id_cliente=%s", $d_reg_cliente);
$reg_cliente = mysql_query($query_reg_cliente, $gestionAdmin) or die(mysql_error());
$row_reg_cliente = mysql_fetch_assoc($reg_cliente);
$totalRows_reg_cliente = mysql_num_rows($reg_cliente);
?>
<?php
require('../../pdf/fpdf.php');
class PDF extends FPDF
{
  function Footer()
  {
    $this->Image('logo.jpg',2,1);
	  
  }
  function datos($row_reg_cliente)
  {
      
  //1	$this->MultiCell(20,1,"Prueba");
	    $this->SetFillColor(0,0,0);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","U",12);
		$this->Cell(0,0.5,"CONTRATO DE PRESTACION DE SERVICIO DE CONECTIVIDAD ",0,1,"C",0);
		$this->Ln();
		$this->SetFont("Arial","",7);
		$this->Cell(0.689,0.5,"Entre ","0","L",0);
		$this->SetFont("Arial","B",7);
		$this->Cell(2.2,0.5,"ESTRANET S.R.L. ","0","J",0);
		$this->SetFont("Arial","",7);
		$this->Cell(4.127,0.5,"representado en este acto por el Sr.");
		$this->SetFont("Arial","B",7);
		$this->Cell(4.456,0.5,"Carlos Froil�n Maza DNI N� 10.781.411,");
		$this->SetFont("Arial","",7);
		$this->Cell(8.23,0.5,"con domicilio en Calle Benjam�n de la Vega 33, de la Ciudad de La Rioja,");
		$this->Ln();
		$this->Cell(10.8,0.5,"en car�cter de Socio Gerente, por una parte en adelante �EMPRESA o LA EMPRESA�  y la Sr/a.   ","0","L",0); 
		$this->SetFont("Arial","B",7);
		$dato=$row_reg_cliente['apellido']." ".$row_reg_cliente['nombre']." ".$row_reg_cliente['razonsocial']." DNI N� ".$row_reg_cliente['DNI'];
		$c=count($datos);
		$this->Line(11.437,7.45,20.3,7.45);
		$this->Cell(8.863,0.5,$dato);
		$this->Ln(0.5);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"con domicilio en ".strtoupper($row_reg_cliente['domicilio']." - Barrio ".$row_reg_cliente['barrio'])." de LA CIUDAD DE LA RIOJA  �,  por la otra en adelante CLIENTE o EL CLIENTE, han acordado el siguiente Contrato,con especial atenci�n al ART. 1198 del C�digo Civil y sujeto a las siguientes clausulas:");
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"PRIMERO: DEFINICIONES:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"�Servicio de Transmisi�n de Se�ales Digitales� o �Servicios�: Son las prestaciones que permiten el transporte de una se�al digital entre dos puntos de la Rep�blica Argentina definidos en el anexo I, que es parte integrante de este contrato, constituido por el �Equipamiento�: Incluye todos los equipos o materiales instalados en el predio del CLIENTE o de terceras partes, necesario para proveer  los Servicios de Internet Inal�mbrico y adicionales a contratar.");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"SEGUNDO: OBJETO DEL CONTRATO:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"El CLIENTE requiere a LA EMPRESA y esta acepta la prestaci�n de Servicios cuyo alcance, caracter�sticas y duraci�n se detallan a continuaci�n  conforme los t�rminos y condiciones del presente. La EMPRESA prestar� los Servicios con su propio Equipamiento y personal. Por lo tanto a�n cuando la EMPRESA pueda instalar el Equipamiento en dependencias del CLIENTE, la EMPRESA retiene el dominio y la titularidad sobre el Equipamiento de su propiedad.");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"TERCERO: DOCUMENTACION CONTRACTUAL:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"El CLIENTE requiere a LA EMPRESA y esta acepta la prestaci�n de Servicios cuyo alcance, caracter�sticas y duraci�n se detallan a continuaci�n  conforme los t�rminos y condiciones del presente. La EMPRESA prestar� los Servicios con su propio Equipamiento y personal. Por lo tanto a�n cuando la EMPRESA pueda instalar el Equipamiento en dependencias del CLIENTE, la EMPRESA retiene el dominio y la titularidad sobre el Equipamiento de su propiedad.");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"CUARTO: TARIFAS:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"4.1 Las tarifas mensuales por Servicios y el Cargo Inicial por la Instalaci�n del Equipamiento, est�n establecidas en el ANEXO I.
4.2 Todos los pagos emergentes del presente, deber�n ser efectuados en el domicilio de pago que se fija m�s abajo o por cualquier otro m�todo que la EMPRESA comunique oportunamente como disponible para sus clientes  en pesos argentinos o d�lares estadounidenses.
4.3 Las tarifas son netas para la EMPRESA excluyendo el impuesto a los Ingresos Brutos, siendo responsabilidad de la EMPRESA el pago de dicho tributo. Consecuentemente, el Impuesto al Valor Agregado (IVA) y todo otro cualquiera que lo sustituya en el futuro ser� a cargo del CLIENTE y ser� adicionado a la factura correspondiente. Igual tratamiento merecer� cualquier impuesto, tasa, carga o contribuci�n espec�ficos que se pudieren imponer en el futuro a la EMPRESA por la prestaci�n de servicios.
4.4 El CLIENTE pagar� a la EMPRESA del 1 al 10 de cada mes CON LA MODALIDAD DE PAGO ANTICIPADO,  la factura del mes en curso. Todas las facturas deber�n reunir los requisitos formales que establezcan las normas vigentes sobre el particular.
4.5 La falta de pago total dentro de la fecha acordada, producir� la mora del CLIENTE de pleno derecho, sin necesidad de interpelaci�n judicial o extrajudicial alguna y autorizar� a la EMPRESA  a devengar a partir del d�a siguiente de la fecha de vencimiento de la factura hasta la fecha de efectivo pago intereses moratorios, punitorios  y compensatorios con la tasa m�xima permitida por la ley.
4.6 La falta de pago total de lo adeudado en concepto de capital e intereses punitorios aqu� estipulados dentro de los treinta (30) d�as corridos desde la fecha de vencimiento original de la factura respectiva, autorizar� a la EMPRESA a suspender la prestaci�n de los Servicios. A este efecto, la EMPRESA dar� un preaviso de diez (10) d�as por medio fehaciente al CLIENTE. El Servicio no se restablecer� hasta tanto no sean efectivizados los montos impagos mas los intereses y todo costo operativo en que pudiera incurrir la EMPRESA para restablecer el Servicio.
4.7 En el caso de que el CLIENTE deje de abonar en t�rmino dos (2) cuotas consecutivas, la  EMPRESA podr� resolver inmediatamente este Contrato, desconectar los Servicios y recuperar el Equipamiento provisto de acuerdo a este Contrato. En caso que la EMPRESA  decida hacer uso de esta �ltima opci�n, dar� aviso por escrito al CLIENTE sin perjuicio del derecho de la EMPRESA de reclamar la cl�usula penal estipulada en el Anexo de Servicio.
4.8 En caso que la EMPRESA considere necesario modificar el importe de las tarifas estipuladas, deber� notificar al CLIENTE las tarifas aplicables al nuevo per�odo con una anticipaci�n no menor a quince (15) d�as h�biles de la fecha de vencimiento del nuevo per�odo.
");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"QUINTO: OBLIGACIONES DEL CLIENTE:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"Sin perjuicio de las provisiones contenidas en otros Art�culos, el CLIENTE tendr� las siguientes obligaciones:
5.1 Utilizar los Servicios aqu� contratados exclusivamente para su propia actividad. Estando expresamente prohibido para el CLIENTE vender y/o subcontratar los Servicios, materiales y equipamientos objeto del presente, con o sin fines de lucro, otorgando en su caso derecho a la EMPRESA de resolver el Contrato, debiendo el CLIENTE � cl�usula penal Anexo I, Art�culo 3.3 y Art�culo CUARTO del presente, pagar todas las facturas presentadas de acuerdo a los t�rminos establecidos en el Art�culo CUARTO precedente.
5.2 De acuerdo a normas legales queda absolutamente prohibido efectuar SPAM (env�o de correo masivo indiscriminado) de correo electr�nico.
5.3 Asimismo, en caso de que el CLIENTE utilice los Servicios materia del presente, para dar/utilizar Servicios de voz sobre IP, Web hosting, FTP Server, VPN, Juegos en tiempo Real ( Shooter, RPG, etc), Browsers AOL 5.0/6 o Prodigy 5.0, Subida frecuente de archivos o E-Mails de gran tama�o, aplicaciones de intercambio de Archivos (Morpheus, Kazaa, AudioGalaxy, etc.), acceso Remoto u otros servicios diferentes a los aqu� solicitados, la EMPRESA no asume ninguna responsabilidad por la prestaci�n o utilizaci�n de dicho(s) servicio(s), puesto que su utilizaci�n recae en equipamiento de terceros y fuera de la red de la EMPRESA.
5.4 En caso de transgredir la cl�usula 5.1 y/o 5.2 y/o 5.3 quedar� autom�ticamente anulado el presente Contrato y la EMPRESA se reserva todos los derechos para accionar seg�n su criterio.
5.5 En caso de trasgresi�n de las reglamentaciones vigentes que pudieran causar penalizaciones a la EMPRESA por parte de terceros, al CLIENTE se le aplicar� una multa de tres mil (3.000) U$S, quedar� autom�ticamente anulado el Contrato y la EMPRESA se reservar� todos los derechos para accionar seg�n su criterio.
");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"SEXTO: RESPONSABILIDAD DEL CLIENTE:",0,1,"B",0);	
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"6.1 El CLIENTE cumplir� con todas las leyes, decretos y regulaciones del gobierno que le sean aplicables como usuario de los Servicios.
6.2 Los Servicios objeto del presente no est�n previstos en su utilizaci�n para salvar y/o proteger vidas y la EMPRESA no ser� responsable ni ante el CLIENTE ni antes terceras partes, por ning�n da�o, reclamo o p�rdida que se produzca por el uso de los mismos en tal sentido.
6.3 La mera presentaci�n en concurso de acreedores o declaraciones de quiebra del CLIENTE, otorgar� derecho a la EMPRESA a resolver el Contrato sin m�s, y el CLIENTE deber� pagar la cl�usula penal especificada en el Anexo I, Art�culo 3.3, sin perjuicio de los derechos acordados a la EMPRESA en el Art�culo CUARTO precedente.
6.4 El suministro de energ�a el�ctrica para el Equipamiento ser� de cargo exclusivo del CLIENTE.
");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"S�PTIMO: EQUIPAMIENTO:",0,1,"B",0);	
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"7.1 La EMPRESA podr� introducir en el Equipamiento los cambios que considere necesarios, de los cuales dar� aviso al CLIENTE con al menos un (1) d�a de anticipaci�n.
7.2 El Equipamiento no podr� ser reubicado o instalado en lugares distintos a los indicados e instalados, sin el previo consentimiento de la EMPRESA.
7.3 La EMPRESA no est� obligada a prestar el Servicio materia del presente Contrato desde el momento en que el Equipamiento sufra cualquier desperfecto ocasionado por negligencia, uso indebido, sustracci�n, accidente o cualquier otra acci�n no atribuible a la EMPRESA.
7.4 Una vez instalado el Equipamiento, el personal de la EMPRESA responsable de dicha instalaci�n realizar�, en presencia del CLIENTE, las pruebas t�cnicas que certifiquen la correcta operaci�n, las caracter�sticas y la calidad del Servicio.
7.5 Las pruebas t�cnicas concluyen con la entrega al CLIENTE de una constancia escrita denominada ORDEN DE INSTALACION en la que deber� constar la identificaci�n del personal de la EMPRESA responsable de la instalaci�n y de las pruebas, sus firmas, la direcci�n de la instalaci�n, nombre, documento de identidad del CLIENTE, las pruebas t�cnicas realizadas y sus resultados.
");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"OCTAVA: RESPONSABILIDAD DE LA EMPRESA:",0,1,"B",0);	
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"8.1 La EMPRESA se compromete a poner en funcionamiento el Servicio dentro de los treinta (30) d�as corridos desde la fecha de celebraci�n del presente.
8.2 La EMPRESA prestar� el Servicio durante las veinticuatro (24) horas de los trescientos sesenta y cinco (365) d�as del a�o.
8.3 Si por causas atribuibles a la EMPRESA y, salvo caso fortuito o fuerza mayor previstos en el Art�culo 10 inciso 5) existiera una p�rdida de disponibilidad, interrupciones, suspensiones de los Servicios u otros defectos o fallas en la prestaci�n de los Servicios contratados por un per�odo igual o mayor a cuatro (4) horas por mes (99,50% de disponibilidad), el CLIENTE tendr� derecho a un cr�dito en el precio mensual que ser� proporcional a la duraci�n total en exceso a tales cuatro (4) horas de dichos defectos. El cr�dito por interrupci�n ser� deducible del abono mensual del mes subsiguiente conforme lo pactado en el anexo de Servicio.
8.4 Consecuentemente, la responsabilidad de la EMPRESA por cualquier da�o directo o indirecto que el CLIENTE o terceras partes puedan sufrir eventualmente por la p�rdida de disponibilidad, defectos, interrupciones, suspensiones u otras faltas que afecten los Servicios contemplados en este Contrato, estar� limitada, para todos los prop�sitos legales, a otorgar los cr�ditos por el tiempo de interrupci�n mencionados anteriormente.
8.5 La EMPRESA no ser� pasible de sanci�n alguna por ninguna interrupci�n de los Servicios o cualquier otra falla en �l, debida a causas fuera de su control comercial razonable, incluyendo entre otras, interferencias electromagn�ticas, obstrucciones para los servicios digitales, terrestres y/o falta de energ�a el�ctrica, fallas irreparables del sat�lite, la indisponibilidad tempor�nea o permanente para la EMPRESA del transportado utilizado para proveer los Servicios digitales satelitales o fibra �ptica todos ellos sin perjuicio de la facultad rescisoria rec�proca obrante en el Art�culo 10 inciso 5).
8.6 La EMPRESA no ser� pasible de sanci�n alguna ante la producci�n de cualquier da�o directo, indirecto, incidental, consecuente, especial o cualquier otro da�o que se le pueda causar a el CLIENTE., por motivos directos o indirectos ocasionados por la Empresa,-
 8.7 En caso que la EMPRESA requiera realizar trabajos de mantenimiento o mejoras tecnol�gicas en su infraestructura que pudieran afectar los Servicios que brinda al CLIENTE, se lo comunicar� por escrito a �ste �ltimo con 5 d�as calendario de anticipaci�n, debiendo adoptar las medidas necesarias para asegurar la continuidad del Servicio.
8.8 la EMPRESA no podr� suspender los servicios sin previo aviso al CLIENTE, con la misma anticipaci�n se�alada en el �tem anterior, salvo caso fortuito o de fuerza mayor. La responsabilidad de la EMPRESA por el servicio de mantenimiento se extinguir� inmediatamente si se comprobase la intervenci�n en el Equipamiento por parte de personas no autorizadas expresamente por la EMPRESA y/o se detecten anomal�as en las condiciones de instalaci�n esenciales no imputables a la EMPRESA.
8.9 La EMPRESA asume el compromiso de cumplir con todos los recaudos legales y legislaci�n aplicable para brindar el Servicio tomando a su cargo todas las autorizaciones ante las autoridades del �rea
");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"NOVENO: VIGENCIA DEL CONTRATO:",0,1,"B",0);	
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"Este Contrato tendr�  vigencia a partir de la fecha de su celebraci�n y tendr� una duraci�n de doce (12) meses a partir de la puesta en funcionamiento de los Servicios. El mismo ser� renovable por per�odos iguales de conformidad entre partes, y se deber� suscribir un nuevo contrato entre las partes,  si no existiera ese nuevo contrato se tendr� por valido y  t�citamente reconocido los t�rminos del presente entre las partes, hasta que exista notificaci�n fehaciente (carta documento, etc.) de resolver.- No podra dar de baja el contrato mientras existiera deuda con la Empresa.-");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"D�CIMO: TERMINACI�N ANTICIPADA.:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"10.1 Rescisi�n Voluntaria del Cliente: Sin perjuicio de lo ya se�alado, en la eventualidad de que el CLIENTE ponga t�rmino anticipado al presente Contrato  con anterioridad a su vencimiento mediante aviso escrito fehaciente con treinta (30) d�as de anticipaci�n, deber� abonar a la EMPRESA una indemnizaci�n equivalente a tres (3) meses del abono mensual. En el caso de que el CLIENTE tenga una antig�edad mayor a ciento ochenta (180) d�as la indemnizaci�n ser� solamente del mes de abono en curso. No proceder� la Resoluci�n si el cliente hubiese obrado con culpa o estuviese en mora �Art. 1198 del C�digo Civil Argentino
10.2 Rescisi�n Voluntaria de la Empresa: En la eventualidad de que la EMPRESA ponga t�rmino anticipado a la presente solicitud  con anterioridad a su vencimiento lo har�  mediante aviso escrito fehaciente con treinta (15) d�as de anticipaci�n.
10.3 Incumplimiento de la EMPRESA:  La falta de la EMPRESA en el cumplimiento de cualquiera de las obligaciones emergentes del presente Contrato, por un per�odo mayor a los veinte (20) d�as, permitir� a el Cliente resolver este Contrato sin mas .
10.4 Incumplimiento del CLIENTE:  La falta del CLIENTE en el cumplimiento de cualquiera de las obligaciones emergentes del presente Contrato, por un per�odo mayor a los sesenta (60) d�as, permitir� a la EMPRESA tener derecho de resolver este Contrato sin m�s, debiendo pagar el CLIENTE la cl�usula penal especificada en el Anexo I, Art�culo 3.3, sin perjuicio de los derechos acordados a la EMPRESA en el Art�culo Cuarto precedente.
10.5 Caso Fortuito o Fuerza Mayor: Si cualquiera de las partes se encontrase impedida de prestar o recibir los Servicios, debido a decisi�n del Gobierno o por causas de fuerza mayor, el Contrato podr� ser terminada inmediatamente, mediante notificaci�n escrita previa con por lo menos diez (10) d�as de anticipaci�n a la fecha de terminaci�n aplicable, sin incurrir en ninguna responsabilidad legal.
");
		$this->Ln(0.5);	
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"D�CIMO PRIMERO: RESOLUCI�N DEL CONTRATO:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"11.1 La EMPRESA podr� resolver de pleno derecho el presente Contrato en los siguientes supuestos:
a. Si el CLIENTE o personal a su cargo intervienen directamente o permiten que terceras personas intervengan o manipulen de cualquier forma el Equipamiento, o los mismos sufren alg�n desperfecto o da�o por causas imputables al CLIENTE o su personal.
b. Si el CLIENTE incumple cualquier obligaci�n establecida en el presente Contrato.
11.2 La resoluci�n del Contrato operar� a partir del d�a h�bil siguiente de recibida por el CLIENTE la carta de la EMPRESA comunicando su intenci�n de poner t�rmino a la misma, sin perjuicio del derecho de la EMPRESA para cobrar la deuda que pudiese estar pendiente de pago por los Servicios prestados, de exigir la restituci�n del Equipamiento y de ejercer las dem�s acciones a que se encuentre facultada. Ser�n de exclusiva cuenta del CLIENTE todos los gastos que se generen con respecto de la resoluci�n del presente Contrato por causa imputable a aquel, incluy�ndose pero no limit�ndose a gastos administrativos, honorarios de abogados y los que pudieran originarse con motivo de actuaciones judiciales o extrajudiciales.
");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"D�CIMO SEGUNDO: INFORMACION RECIBIDA Y/O TRANSMITIDA:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"12.1 En cumplimiento del derecho del CLIENTE al secreto de las telecomunicaciones, la EMPRESA se obliga a no tener acceso a la informaci�n que el CLIENTE transmita o reciba a trav�s de los Servicios materia del presente Contrato. Sin perjuicio de ello, el CLIENTE declara tener pleno conocimiento de:
a. La EMPRESA no ejerce control de ninguna clase respecto del contenido de la informaci�n que el CLIENTE transmita o reciba a trav�s del enlace provisto.
b. El CLIENTE es el �nico responsable por el contenido de dicha informaci�n.
c. En caso de que el CLIENTE haga mal uso del Servicio materia del presente Contrato o transmita informaci�n prohibida por ley, contraria al orden p�blico o las buenas costumbres, o que atente contra derechos de terceros, el CLIENTE asumir� toda responsabilidad civil, penal, administrativa o de cualquier naturaleza a que pudiere haber lugar, quedando la EMPRESA exenta de cualquier responsabilidad.
d. En caso que se le impute a la EMPRESA alg�n tipo de responsabilidad por el uso del enlace materia del presente Contrato o por la naturaleza o contenido de la informaci�n transmitida o recibida por el CLIENTE a trav�s del mismo, el CLIENTE se obliga a indemnizar a la EMPRESA por cualquier da�o o responsabilidad que le sea imputada directamente o a su personal.
e. La EMPRESA se encuentra exenta de responsabilidad por da�os directos o indirectos que pudieran sufrir el CLIENTE y/o terceros, incluyendo lucro cesante, p�rdida de fondos y/o valores almacenados, p�rdida de informaci�n o posibles da�os a los sistemas del CLIENTE, cualquiera sea la causa, incluidos virus, troyanos, y/o cualquier tipo de archivos peligrosos y/o ataques contra los sistemas del CLIENTE provenientes de Internet o de terceros. Asimismo, la EMPRESA se encuentra exenta de responsabilidad por actos de terceros hackers que pudieran introducirse en los sistemas del CLIENTE.");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"D�CIMO TERCERO: ATENCION DE RECLAMOS:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"La EMPRESA mantendr� servicios de informaci�n y asistencia T�cnica para la soluci�n de reclamos relativos al servicio, facturaci�n, instalaci�n y operaci�n de los Servicios adicionales solicitados, tramitados �stos a trav�s del  tel�fono 03822 420082, �nicamente, donde se asentara el reclama y conforma la orden de soporte t�cnico, cuando este fuera por defectos o fallas ocasionadas por los equipos provistos por la EMPRESA, ser� sin cargo para el CLIENTE, cuando la atenci�n de las fallas en el servicio sea por motivos ajenos a la EMPRESA, como fallas de la computadora, desconfiguraci�n de la misma, perdida de la IP por formateo o intervenci�n del cliente o personal ajeno a la Empresa, cambios de ubicaci�n, agregado de nuevos equipamiento, etc .este ser� por cargo exclusivamente del CLIENTE.-");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"D�CIMO CUARTO: CESION DEL CONTRATO:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5," El CLIENTE no podr� comercializar o subarrendar los Servicios materia del presente Contrato, ni ceder su posici�n en el presente, total ni parcialmente, ni ceder derechos u obligaciones sin el previo consentimiento de la EMPRESA");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"D�CIMO QUINTO: MODIFICACIONES DEL CONTRATO:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"Las modificaciones al Contrato, ser�n acordadas por ambas partes con la firma de las mismas. Ante la Falta de pago de los abonos convenidos, la Empresa podr� suspender el servicio de Internet  no implicando ello que no sea exigible el pago de los d�as y/o meses que estuviera cortado el servicio, porque esto tiene un especial Car�cter de  �Pacto comisorio expreso� con efecto de suspensi�n punitoria.- por cuanto deber� abonar la totalidad del importe, aun cuando estuviere suspendi� por la falta de pago, la persistencia en el domicilio del cliente de todos los equipos facilitados en comodato como antena, caja estanca, Access point, poe transformadores, conexiones, etc. demuestra la indudable vinculaci�n contractual con la �EMPRESA�, y obliga al cliente a mantener el pago del abono mensual e intereses si correspondiere.-");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"D�CIMO SEXTO: LEGISLACI�N Y JURISDICCI�N:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"Toda cuesti�n o divergencia, reclamaci�n o duda que surja entre las partes, referida a la interpretaci�n, ejecuci�n o resoluci�n de este Contrato, o que en cualquier forma se relacione con �l, directa o indirectamente, se establece para tal fin la jurisdicci�n de los Tribunales Ordinarios de la Ciudad de La Rioja, renunciando a cualquier otro fuero o jurisdicci�n que pudiera corresponder.-");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"D�CIMO S�PTIMO: NOTIFICADORES Y DOMICILIOS:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"Todas las notificaciones a ser intercambiadas la relaci�n con este Contrato ser�n cursadas por medio fehaciente y se reputar�n efectuadas ante la recepci�n por el destinatario en los siguientes domicilios: La Empresa: BENJAMIN DE LA VEGA N� 33 CIUDAD LA RIOJA
El Cliente:  en CALLE CERRO OTTO N� 74 - B� CIRCULO POLICIAL   de esta ciudad de La Rioja. Cualquiera de las partes podr� fijar nuevo domicilio o sector de su empresa involucrado, comunic�ndoselo por escrito fehacientemente a la otra, con treinta (30) d�as de anticipaci�n
.-");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"D�CIMO OCTAVO: SELLADOS:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"Los costos correspondientes al sellado del presente Contrato ser�n abonados por partes iguales entre el CLIENTE y la EMPRESA, si as� correspondiere.
En prueba de conformidad se firman dos (2) ejemplares de un mismo texto y tenor.
.-");
		$this->Ln(0.5);

				
  }
}	
$pdf=new PDF('P','cm','A4');
$title="REMITO ELECTRONICO";
//$pdf->remito();
$pdf->SetMargins(0.7,5.5,0.7);
$pdf->AddPage('P');
//Cargo Datos de Recibo
$pdf->datos($row_reg_cliente);
$pdf->SetAuthor('Lic. Hernan Nicolas Davila');
//F para inscrutar
ob_end_clean();
$pdf->Output('../documentos/orden.pdf','F');
?>
<?php
mysql_free_result($reg_cliente);
?>
