<?php require_once('../../Connections/gestionAdmin.php'); ?>
<?php
$d_reg_cliente = "0";
if (isset($_GET['id_cliente'])) {
  $d_reg_cliente = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_cliente = sprintf("SELECT * FROM cliente WHERE cliente.id_cliente=%s", $d_reg_cliente);
$reg_cliente = mysql_query($query_reg_cliente, $gestionAdmin) or die(mysql_error());
$row_reg_cliente = mysql_fetch_assoc($reg_cliente);
$totalRows_reg_cliente = mysql_num_rows($reg_cliente);
?>
<?php
require('../../pdf/fpdf.php');
class PDF extends FPDF
{
  function Footer()
  {
    $this->Image('logo.jpg',2,1);
	  
  }
  function datos($row_reg_cliente)
  {
      
  //1	$this->MultiCell(20,1,"Prueba");
	    $this->SetFillColor(0,0,0);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","U",12);
		$this->Cell(0,0.5,"ANEXO 1:  ANEXO DE SERVICIO ",0,1,"C",0);
		$this->Ln();
		$this->Cell(0,0.5,"1 TIPO DE SERVICIO",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"1.1. Un (1) enlace de datos de 128. Kbps . Dedicados para comunicar los siguientes puntos:");
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"Local de EL CLIENTE situado en:".strtoupper($row_reg_cliente['domicilio']." - Barrio ".$row_reg_cliente['barrio'])." de LA CIUDAD DE LA RIOJA  �, Y LA EMPRESA en Benjam�n de la Vega 33 de B� Centro de esta ciudad.-");
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"1.2. La conectividad a la Red. Internet al backbone nacional e internacional.");
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"2. Cargos:",0,1,"B",0);
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"2.1. Cargos Iniciales de Instalaci�n. El equipamiento a instalar incluye 10 metros de cable UTP, conectores, antenas y otros elementos para su normal funcionamiento. En caso de requerirse otros elementos, por necesidades de EL CLIENTE (Cable, Placa de Red, etc.), ser�n facturados independientemente. IMPORTE DE LA INSTALACION: $..240. + IVA.");
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"2.2. Cargo Mensual.");
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"El cargo mensual m�s los impuestos que correspondan, pagaderos del 01 al 10 e cada mes en el domicilio de LA EMPRESA, en efectivo o por cualquier otro m�todo que LA EMPRESA comunique oportunamente como disponible para sus clientes, en pesos IMPORTE DEL ABONO:$..116  + IVA.- La Empresa no posee cobrador, por lo que el pago deber� efectuarse en Benjamin de la Vega 33, La Rioja");
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"3. Pagos por Rescisi�n � Resoluci�n:",0,1,"B",0);	
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"3.1. Rescisi�n Voluntaria por EL CLIENTE:");
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"EL CLIENTE podr� rescindir voluntariamente este Contrato antes de su finalizaci�n luego de los seis meses de firmado, debiendo preavisar fehacientemente y por escrito con treinta (30) d�as de anticipaci�n su decisi�n y deber� abonar la totalidad de la factura del mes en curso.");
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"En el caso que LA EMPRESA rescinda voluntariamente este Contrato antes de su expiraci�n, dentro de los primeros seis meses, deber� preavisar al cliente por medio fehaciente con treinta (30) d�as de anticipaci�n.");
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"3.3. Incumplimiento de EL CLIENTE al Contrato de Servicio:");
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"En caso que EL CLIENTE incumpla sustancialmente el Contrato de Servicio de manera que habilite a LA EMPRESA a resolverlo de acuerdo a lo estipulado en el �ltimo p�rrafo del Art. Cuarto Inc.7), pagar� la siguiente cl�usula penal a LA EMPRESA, sin perjuicio de la obligaci�n de pago de los intereses punitorios previstos en el Art. CUARTO  del Contrato de Servicio, devengados entre la fecha de mora y fecha de efectivo pago: un monto equivalente al 50% (cincuenta por ciento) de los cargos mensuales correspondientes a los meses que restan para completar la duraci�n del contrato.");
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"3.4. Incumplimiento de LA EMPRESA al Contrato de Servicio:");
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"Para el supuesto que EL CLIENTE decida hacer uso de la facultad resolutoria, deber� cursar intimaci�n fehaciente a LA EMPRESA para que subsane el incumplimiento total / parcial incurrido dentro del plazo perentorio de veinte (20) d�as corridos, bajo apercibimiento de considerar resuelto sin m�s el presente contrato, en forma parcial � total seg�n sea el caso. Recibida tal intimaci�n, LA EMPRESA podr� -a su opci�n- remitir nota escrita a EL CLIENTE, manifestando su decisi�n de reemplazar por un tiempo determinado la topolog�a � el equipamiento o a�n de prestar servicios por intermedio de terceros prestadores, en condiciones que cumplan con las especificaciones pactadas, tomando a su cargo los mayores costos del mismo. El tiempo en que hubiera suplantado efectivamente de la misma forma antes indicada, no ser� computado a los efectos de determinar el incumplimiento de LA EMPRESA conforme a lo previsto en esta cl�usula.");
		$this->Ln(0.5);
		$this->SetFont("Times","B",9);
		$this->Cell(0,0.5,"4. Cargo por demora en la devoluci�n del Equipamiento:",0,1,"B",0);	
		$this->SetFont("Arial","",6);
		$this->MultiCell(0,0.5,"Ante la resoluci�n del Contrato y pasado los dos (2) d�as de demora para la entrega de Equipamiento instalado en dependencias del Cliente, el Cliente deber� abonar a La Empresa por cada un (1) d�a de demora el valor del abono vigente dividido treinta (30) m�s los intereses moratorios y compensatorios con la tasa m�ximos permitidos por la ley desde la fecha de resoluci�n del Contrato hasta la fecha de efectiva la entrega de los mismos. En la Ciudad de La Rioja a los");
		$this->Ln(0.5);

				//falta la fecha
  }
}	
$pdf=new PDF('P','cm','A4');
$title="REMITO ELECTRONICO";
//$pdf->remito();
$pdf->SetMargins(0.7,5.5,0.7);
$pdf->AddPage('P');
//Cargo Datos de Recibo
$pdf->datos($row_reg_cliente);
$pdf->SetAuthor('Lic. Hernan Nicolas Davila');
//F para inscrutar
ob_end_clean();
$pdf->Output('../documentos/orden.pdf','F');
?>
<?php
mysql_free_result($reg_cliente);
?>
