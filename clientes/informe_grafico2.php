<!doctype html>
<html>
<head>
<title>Estranetsrl Pagos</title>
<meta name = "viewport" content = "initial-scale = 0.5, user-scalable = yes">
<style>
#areaA
{
border:5px solid black;
width:1000px;
height:600px;
position:absolute;
top:10%;
left:10%;
}
body
{
width:900px;
height:600px;
}
#cambiar_obj
{
list-style-type:none;
}
</style>
<script language="JavaScript">

function ocultar(id){

var cambiar=document.getElementById(id); 
    cambiar.style.display='none';     
   			
			}
function mostrar(id){

var cambiar= document.getElementById(id);
   cambiar.style.display='inline';	
    }
window.onload=function () 
{
ocultar("cambiar_obj");
}
</script>
</head>
<body>
<div id="areaA">
<?php 
if(isset($_POST['dat']))$opcion=$_POST['dat'];//Para opcion global
//En caso necesario se cambia obj
cambiar_objetivo();
?>
<form style="position:absolute;top:10px;left:700px" action="informe_grafico2.php" method="post" name="objetivo" id="objetivo">
<?
mostrar_objetivo($opcion);
?>
<ul id="cambiar_obj">
<li><input type="radio" name="opc" id="opc" value="1" checked> Recaudacion</li>
<li><input type="radio" name="opc" id="opc" value="2"> Cantidad pagaron</li>
<li><input type="radio" name="opc" id="opc" value="3"> Altas</li>
<li><input type="radio" name="opc" id="opc" value="4"> Bajas</li>
<li><input type="text" name="obj" id="obj" size="12">
<input style="color:white;background-color:gray" type="submit" name="cambiar" id="cambiar" value="Cambiar"></li>
</ul>
</form>	
<form action="informe_grafico2.php" method="post" name="dato" id="dato">
<input type="radio" name="dat" id="dat" value="1" checked> Recaudacion
<input type="radio" name="dat" id="dat" value="2"> Cantidad pagaron
<input type="radio" name="dat" id="dat" value="3"> Altas
<input type="radio" name="dat" id="dat" value="4"> Bajas
<input style="position:absolute;top:1px;left:470px;background-color:gray;color:white" type="submit" name="actualizar" id="actualizar" value="<< Actualizar Grafico>>">
</form>
<div style="width:100%; margin: 0px auto; font-family:sans-serif;">
<?php
/** Include class */
include( 'GoogChart.class.php' );
$anio=2014;cargar_anio($anio);
function generar_grafico($opcion,$anio){
switch($opcion){
case 1:
$monto[0]=(promedio_anual_R($anio)*100)/retornar_objetivo($opcion);
$monto[1]=(promedio_anual_R($anio-1)*100)/retornar_objetivo($opcion);
$monto[2]=(promedio_anual_R($anio-2)*100)/retornar_objetivo($opcion);
$monto[3]=(promedio_anual_R($anio-3)*100)/retornar_objetivo($opcion);
break;
case 2:
$monto[0]=(promedio_anual_C($anio)*100)/retornar_objetivo($opcion);
$monto[1]=(promedio_anual_C($anio-1)*100)/retornar_objetivo($opcion);
$monto[2]=(promedio_anual_C($anio-2)*100)/retornar_objetivo($opcion);
$monto[3]=(promedio_anual_C($anio-3)*100)/retornar_objetivo($opcion);
break;
case 3:
$monto[0]=(promedio_anual_A($anio)*100)/retornar_objetivo($opcion);
$monto[1]=(promedio_anual_A($anio-1)*100)/retornar_objetivo($opcion);
$monto[2]=(promedio_anual_A($anio-2)*100)/retornar_objetivo($opcion);
$monto[3]=(promedio_anual_A($anio-3)*100)/retornar_objetivo($opcion);
break;
case 4:
$monto[0]=(promedio_anual_B($anio)*100)/retornar_objetivo($opcion);
$monto[1]=(promedio_anual_B($anio-1)*100)/retornar_objetivo($opcion);
$monto[2]=(promedio_anual_B($anio-2)*100)/retornar_objetivo($opcion);
$monto[3]=(promedio_anual_B($anio-3)*100)/retornar_objetivo($opcion);
break; 
         	}
$chart = new GoogChart();

// Set graph data
$data = array(
			''.$anio.'' =>ceil($monto[0]),
			''.($anio-1).'' =>ceil($monto[1]),
			''.($anio-2).'' => ceil($monto[2]),
			''.($anio-3).'' => ceil($monto[3])			

	);
// Set graph colors
$color = array(
		'#99C754'
        	);
switch($opcion){

case 1:
echo '<h2>Promedio de las recaudaciones por anio</h2>';
$chart->setChartAttrs( array(
	'type' => 'line',
	'title' => 'Recaudaciones periodo '.$anio.'-'.($anio-11),
	'data' => $data,
	'size' => array( 850, 300 ),
	'color' => $color,
	'labelsXY' => true,
	));
break;
case 2:
echo '<h2>Promedio cantidad clientes pagaron por anio</h2>';
$chart->setChartAttrs( array(
	'type' => 'line',
	'title' => 'Cantidad  periodo '.$anio.'-'.($anio-11),
	'data' => $data,
	'size' => array( 850, 300 ),
	'color' => $color,
	'labelsXY' => true,
	));
break;
case 3:
echo '<h2>Promedio de las altas por anio</h2>';
$chart->setChartAttrs( array(
	'type' => 'line',
	'title' => 'Altas periodo '.$anio.'-'.($anio-11),
	'data' => $data,
	'size' => array( 850, 300 ),
	'color' => $color,
	'labelsXY' => true,
	));
break;
case 4:
echo '<h2>Promedio de las bajas por anio</h2>';
$chart->setChartAttrs( array(
	'type' => 'line',
	'title' => 'Bajas periodo '.$anio.'-'.($anio-11),
	'data' => $data,
	'size' => array( 850, 300 ),
	'color' => $color,
	'labelsXY' => true,
	));
break;

                 }

echo $chart;
						}
/* Opciones
-1 Recaudaciones
-2 Cantidad de clientes que pagaron
-3 Altas
-4 Bajas
*/
generar_grafico($opcion,$anio);

?>
</div>
<table style="width:900px;height:2%;position:absolute;top:480px;left:10px;margin-left:1px;margin-right:2px;background-color:silver">
<tr style="color:white;background-color:black">
<td>Dato</td>
<?php
echo '<td>'.$anio.'</td>';
echo '<td>'.($anio-1).'</td>';
echo '<td>'.($anio-2).'</td>';
echo '<td>'.($anio-3).'</td>';
?>
</tr>
<!--Montos-->
<tr style="background-color:gray;color:white">
<td>Recaudaciones</td>
<td><?php echo promedio_anual_R($anio);?></td>
<td><?php echo promedio_anual_R($anio-1);?></td>
<td><?php echo promedio_anual_R($anio-2);?></td>
<td><?php echo promedio_anual_R($anio-3);?></td>
</tr>
<!--Cantidades-->
<tr style="background-color:gray;color:white">
<td>Cantidades</td>
<td><?php echo promedio_anual_C($anio);?></td>
<td><?php echo promedio_anual_C($anio-1);?></td>
<td><?php echo promedio_anual_C($anio-2);?></td>
<td><?php echo promedio_anual_C($anio-3);?></td>
</tr>
<!--Altas-->
<tr style="background-color:gray;color:white">
<td>Altas</td>
<td><?php echo promedio_anual_A($anio);?></td>
<td><?php echo promedio_anual_A($anio-1);?></td>
<td><?php echo promedio_anual_A($anio-2);?></td>
<td><?php echo promedio_anual_A($anio-3);?></td>
</tr>
<!--Bajas-->
<tr style="background-color:gray;color:white">
<td>Bajas</td>
<td><?php echo promedio_anual_B($anio);?></td>
<td><?php echo promedio_anual_B($anio-1);?></td>
<td><?php echo promedio_anual_B($anio-2);?></td>
<td><?php echo promedio_anual_B($anio-3);?></td>
</tr>
<?php
function retornar_objetivo($opcion)
{
$consulta= simplexml_load_file("objetivos_temp.xml"); //Carga fichero auxiliar
switch($opcion){
case 1:
$objetivo=$consulta->objetivoR;
break;
case 2:
$objetivo=$consulta->objetivoC;
break;
case 3:
$objetivo=$consulta->objetivoA;
break;
case 4:
$objetivo=$consulta->objetivoB;
break;
		 }
return number_format($objetivo+0.1);
}
function mostrar_objetivo($opcion){
$consulta= simplexml_load_file("objetivos_temp.xml"); //Carga fichero auxiliar
switch($opcion){
case 1:
$objetivo=$consulta->objetivoR;
break;
case 2:
$objetivo=$consulta->objetivoC;
break;
case 3:
$objetivo=$consulta->objetivoA;
break;
case 4:
$objetivo=$consulta->objetivoB;
break;
		 }
echo '<strong> Objetivo: '.number_format($objetivo+0.1).'</strong><p onClick=ocultar("cambiar_obj") onDblClick=mostrar("cambiar_obj")>[+]</p>';
	                          }
function cargar_anio(&$a)
{
$consulta= simplexml_load_file("pagos_temp.xml"); //Carga fichero auxiliar
    $a=$consulta->anio;
}
function informar_pagos_mensuales($m,$a)
{
//Conexion base de datos
$conexion_aux=mysql_connect("localhost","root","Exl786to.");//Conexion bd aux
mysql_select_db("estranet",$conexion_aux);//Seleccion bd aux
$conexion=mysql_connect("localhost","root","Exl786to.") or
  die("Problemas en la conexion");
mysql_select_db("estranet",$conexion) or
  die("Problemas con la base de datos");
$registros=mysql_query("SELECT cliente.id_cliente, cliente.nombre, cliente.apellido, cliente.razonsocial, cliente.baja,recibo.fecha,day(fecha)as dia,time(fecha)as hora,recibo.id_recibo
FROM cliente, recibo
WHERE cliente.id_cliente = recibo.id_cliente
HAVING (
year( fecha ) =$a
AND month( fecha ) =$m
)
;", $conexion) or die("Problemas en la seleccion".mysql_error());
//Inicio recorrido BD
$numero_registros=0;
$total=0.0;
while ($reg=mysql_fetch_array($registros))
{$pago=0;
$id_cliente=$reg['id_cliente'];
$id_recibo=$reg['id_recibo'];
$registros_aux=mysql_query("SELECT recibo_detalle.preciopag FROM recibo_detalle WHERE recibo_detalle.id_cliente=$id_cliente and recibo_detalle.id_recibo=$id_recibo;",$conexion_aux) or die("Problema en la Selec".mysql_error());
		while ($reg_aux=mysql_fetch_array($registros_aux))
		{
		 $pago=$pago+$reg_aux['preciopag'];				
		}mysql_free_result($conexion_aux);
//Para adicionales
if($pago==0)
{
$registros_aux=mysql_query("SELECT recibodr.preciopag FROM recibodr WHERE recibodr.id_cliente=$id_cliente and recibodr.id_recibo=$id_recibo;",$conexion_aux) or die("Problema en la Selec".mysql_error());

	while ($reg_aux=mysql_fetch_array($registros_aux))
		{
		 $pago=$pago+$reg_aux['preciopag'];				
		}
}
//Fin adicionales
$numero_registros=$numero_registros+1;
$total=$total+$pago;
}
//Para total
$numero_registros=number_format($numero_registros);
$total=number_format($total);
mysql_close($conexion);
mysql_close($conexion_aux);
return $total;
}
//Cantidad clientes que pagaron
function informar_cantidad_pagaron_mensuales($m,$a)
{
//Conexion base de datos
$conexion=mysql_connect("localhost","root","Exl786to.") or
  die("Problemas en la conexion");
mysql_select_db("estranet",$conexion) or
  die("Problemas con la base de datos");
$registros=mysql_query("SELECT DISTINCT cliente.id_cliente FROM cliente,recibo WHERE cliente.id_cliente = recibo.id_cliente 
and
year( fecha ) =$a
and month( fecha ) =$m
;", $conexion) or die("Problemas en la seleccion".mysql_error());
//Inicio recorrido BD
$cantidad_clientes=0;
while ($reg=mysql_fetch_array($registros))
{
$cantidad_clientes=$cantidad_clientes+1;
}
$cantidad_clientes=number_format($cantidad_clientes);
mysql_close($conexion);
return $cantidad_clientes;
}
//Cantidad de altas que hubieron
function informar_cantidad_altas_mensuales($m,$a)
{
//Conexion base de datos
$conexion=mysql_connect("localhost","root","Exl786to.") or
  die("Problemas en la conexion");
mysql_select_db("estranet",$conexion) or
  die("Problemas con la base de datos");
$registros=mysql_query("SELECT ordeninstalacion.id_cliente FROM ordeninstalacion
 WHERE year( fecha ) =$a
 and month( fecha ) =$m;", $conexion) or die("Problemas en la seleccion".mysql_error());
//Inicio recorrido BD
$cantidad_instalaciones=0;
while ($reg=mysql_fetch_array($registros))
{
$cantidad_instalaciones=$cantidad_instalaciones+1;
}
$cantidad_instalaciones=number_format($cantidad_instalaciones);
mysql_close($conexion);
return $cantidad_instalaciones;
}
//Cantidad de bajas que hubieron
function informar_cantidad_bajas_mensuales($m,$a)
{
//Conexion base de datos
$conexion=mysql_connect("localhost","root","Exl786to.") or
  die("Problemas en la conexion");
mysql_select_db("estranet",$conexion) or
  die("Problemas con la base de datos");
$registros=mysql_query("SELECT cliente.id_cliente FROM cliente
 WHERE cliente.baja=1 and year( fechaBaja ) =$a
 and month( fechaBaja ) =$m;", $conexion) or die("Problemas en la seleccion".mysql_error());
//Inicio recorrido BD
$cantidad_bajas=0;
while ($reg=mysql_fetch_array($registros))
{
$cantidad_bajas=$cantidad_bajas+1;
}
$cantidad_bajas=number_format($cantidad_bajas);
mysql_close($conexion);
return $cantidad_bajas;
}
function cambiar_objetivo()
{
if(isset($_POST['obj']))    				{
$tObj=$_POST['opc'];
$objetivo=$_POST['obj'];
$consulta= simplexml_load_file("objetivos_temp.xml"); //Carga fichero auxiliar
$objR=$consulta->objetivoR;
$objC=$consulta->objetivoC;
$objA=$consulta->objetivoA;
$objB=$consulta->objetivoB;

switch($tObj)
{
case 1:
$objR=$objetivo;
break;
case 2:
$objC=$objetivo;
break;
case 3:
$objA=$objetivo;
break;
case 4:
$objB=$objetivo;
break;
}
$fichero1 =fopen("objetivos_temp.xml","w");
fputs($fichero1,"<consulta>");
fputs($fichero1,"<objetivoR>".$objR."</objetivoR>");
fputs($fichero1,"<objetivoC>".$objC."</objetivoC>");
fputs($fichero1,"<objetivoA>".$objA."</objetivoA>");
fputs($fichero1,"<objetivoB>".$objB."</objetivoB>");
fputs($fichero1,"</consulta>");
fclose($fichero1);

						}
}
function promedio_anual_R($anio)
{
$acu=0;
$cont=0;
for($i=1;$i<=12;$i++){
			if (informar_pagos_mensuales($i,$anio)!=0)
								{
						$acu=$acu+informar_pagos_mensuales($i,$anio);
						$cont=$cont+1;	
								}
     		     }
$pro=($acu/$cont);
return ceil($pro);
}
function promedio_anual_C($anio)
{
$acu=0;
$cont=0;
for($i=1;$i<=12;$i++){
				if(informar_cantidad_pagaron_mensuales($i,$anio)!=0) {			
									$acu=$acu+informar_cantidad_pagaron_mensuales($i,$anio);
									$cont=$cont+1;											
										}
     		     }
$pro=($acu/$cont);
return ceil($pro);
}
function promedio_anual_A($anio)
{
$acu=0;
$cont=0;
for($i=1;$i<=12;$i++){
				if(informar_cantidad_altas_mensuales($i,$anio)!=0) {			
									$acu=$acu+informar_cantidad_altas_mensuales($i,$anio);
									$cont=$cont+1;											
										}
     		     }
$pro=($acu/$cont);
return ceil($pro);
}
function promedio_anual_B($anio)
{
$acu=0;
$cont=0;
for($i=1;$i<=12;$i++){
				if(informar_cantidad_bajas_mensuales($i,$anio)!=0) {			
									$acu=$acu+informar_cantidad_bajas_mensuales($i,$anio);
									$cont=$cont+1;											
										}
     		     }
$pro=($acu/$cont);
return ceil($pro);
}
?>
</table>
</div>
</body>
</html>
