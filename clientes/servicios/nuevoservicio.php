<?php require_once('../../Connections/gestionAdmin.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $fecha_primer_p=trim($_POST['fecha']);
  $fecha=date("Y-m-d");
  $saldoParcial=doubleval($_POST['costo']);

  $saldoInsert= $saldoParcial> 0?$saldoParcial:($saldoParcial * -1);

  $insertSQL = sprintf("INSERT INTO servicio (id_cliente, servicio, financiacion, costo, fecha,fecha_primer_pago, saldo) VALUES (%s, %s, %s, %s, %s,%s, %s)",
                       GetSQLValueString($_POST['id_cliente'], "int"),
                       GetSQLValueString($_POST['servicio'], "text"),
                       GetSQLValueString($_POST['cuotas'], "int"),
                       GetSQLValueString($_POST['costo'], "double"),
                       GetSQLValueString($fecha, "date"),
                       GetSQLValueString($fecha_primer_p, "date"),
					   GetSQLValueString($saldoInsert, "double"));
  mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
  header("location:index.php?id_cliente=".$_POST['id_cliente']);
}

$id_reg_cliente = "0";
if (isset($_GET['id_cliente'])) {
  $id_reg_cliente = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_cliente = sprintf("SELECT cliente.nombre, cliente.apellido, cliente.razonsocial FROM cliente WHERE cliente.id_cliente=%s", $id_reg_cliente);
$reg_cliente = mysql_query($query_reg_cliente, $gestionAdmin) or die(mysql_error());
$row_reg_cliente = mysql_fetch_assoc($reg_cliente);
$totalRows_reg_cliente = mysql_num_rows($reg_cliente);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.Estilo5 {font-size: 10px}
.Estilo6 {color: #828B93; font: Tahoma;}
-->
</style>
<link href="../../css/recibo.css" rel="stylesheet" type="text/css">
<link href="../../style.css" rel="stylesheet" type="text/css">
</head>

<body>
<br>
<form name="form1" method="POST" action="<?php echo $editFormAction; ?>">
  <table align="center" class="borde" >
    <!--DWLayoutTable-->
    <tr align="center">
      <td height="24" colspan="2" class="titulo">Alta de servicio para el cliente: <?php echo $row_reg_cliente['nombre']; ?>&nbsp;&nbsp; <?php echo $row_reg_cliente['apellido']; ?>&nbsp; <?php echo $row_reg_cliente['razonsocial']; ?></td>
    </tr>
    <tr>
      <td width="173" class="top11">Servicio</td>
      <td width="678" class="top11"><input name="servicio" type="text" id="servicio" size="75"> <input name="id_cliente" type="hidden" id="id_cliente" value="<?php echo $_GET['id_cliente']; ?>"></td>
    </tr>
    <tr>
      <td class="top11">Costo</td>
      <td class="top11"><input name="costo" type="text" id="costo"></td>
    </tr>
    <tr>
      <td class="top11">Financiacion</td>
      <td class="top11"><p>
  <input name="cuotas" type="text" id="cuotas" value="1" size="10">
&nbsp; Cuotas</p>        </td>
    </tr>
    <tr>
      <td class="top11">Fecha de primer Pago (dd/mm/aaaa) </td>
      <td class="top11"><input name="fecha" type="date" id="fecha" value="<?php echo date("Y-m-d") ?>"></td>
    </tr>
    <tr align="center" valign="middle">
      <td height="42" colspan="2"><input type="submit" name="Submit" value="Grabar">
        &nbsp;&nbsp;&nbsp; <input name="Submit2" type="button" onClick="location.replace('index.php?id_cliente=<?php echo $_GET['id_cliente']; ?>')" value="Cancelar"></td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1">
</form>
</body>
</html>
<?php
mysql_free_result($reg_cliente);
?>
