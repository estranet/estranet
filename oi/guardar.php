<?php require_once('../Connections/gestionAdmin.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

  $updateSQL = sprintf("UPDATE ordeninstalacion SET observacion=%s WHERE id_orden=%s AND id_cliente=%s",
                       GetSQLValueString($_POST['obs'], "text"),
                       GetSQLValueString($_POST['id_orden'], "int"),
                       GetSQLValueString($_POST['id_cliente'], "int"));

  mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($updateSQL, $gestionAdmin) or die(mysql_error());
  $fecha=explode("/",$_POST['fechainst']);
  $fecha=$fecha[2]."-".$fecha[1]."-".$fecha[0];
  $fecha=$fecha." ".$_POST['hora'];
  //echo $fecha;
  $updateSQL = sprintf("UPDATE fichatecnica SET ipcliente=%s, nodo=%s, ipantena=%s, macap=%s, esid=%s, mactarjetared=%s, mastil=%s, cable=%s, senial=%s, tipoantena=%s, fhinstalacion=%s WHERE id_cliente=%s",
                       GetSQLValueString($_POST['ipcliente'], "text"),
                       GetSQLValueString($_POST['nodo'], "text"),
                       GetSQLValueString($_POST['ipantena'], "text"),
                       GetSQLValueString($_POST['macap'], "text"),
                       GetSQLValueString($_POST['esid'], "text"),
                       GetSQLValueString($_POST['macTarj'], "text"),
                       GetSQLValueString($_POST['mastil'], "int"),
                       GetSQLValueString($_POST['cable'], "int"),
                       GetSQLValueString($_POST['senal'], "int"),
                       GetSQLValueString($_POST['tipoantena'], "text"),
                       GetSQLValueString($fecha, "date"),
                       GetSQLValueString($_POST['id_cliente'], "int"));

  mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($updateSQL, $gestionAdmin) or die(mysql_error());
//  $ultimo=date("Y-m-d H:i:s");
  $updateSQL = sprintf("UPDATE cliente SET fultimo=%s WHERE id_cliente=%s",
                       GetSQLValueString(date("Y-m-d H:i:s"), "date"),
					   $_POST['id_cliente']);
  mysql_select_db($database_gestionAdmin, $gestionAdmin);
  //echo   $updateSQL;
 $Result1 = mysql_query($updateSQL, $gestionAdmin) or die(mysql_error());

  header("location: ../clientes/index.php");
?>
