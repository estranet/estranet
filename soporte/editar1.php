<?php require_once('../Connections/gestionAdmin.php'); ?>
<?php
$d_reg_bcliente = "0";
if (isset($_GET['id_soporte'])) {
  $d_reg_bcliente = (get_magic_quotes_gpc()) ? $_GET['id_soporte'] : addslashes($_GET['id_soporte']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_bcliente = sprintf("SELECT soporte.id_cliente, soporte.id_soporte, soporte.cargo, soporte.fecha AS fecha, soporte.observaciones FROM soporte WHERE soporte.id_soporte=%s", $d_reg_bcliente,$d_reg_bcliente);
$reg_bcliente = mysql_query($query_reg_bcliente, $gestionAdmin) or die(mysql_error());
$row_reg_bcliente = mysql_fetch_assoc($reg_bcliente);
$totalRows_reg_bcliente = mysql_num_rows($reg_bcliente);
if($totalRows_reg_bcliente>0)
   $_GET['id_cliente']=$row_reg_bcliente['id_cliente'];
$d_reg_cliente = "0";
if (isset($_GET['id_cliente'])) {
  $d_reg_cliente = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_cliente = sprintf("SELECT cliente.id_cliente, cliente.nombre, cliente.apellido, cliente.razonsocial, cliente.domicilio, cliente.barrio, cliente.fechaAlta AS fecha, cliente.cuit, cliente.telf, cuenta.abonomensual, articulos.articulo FROM cliente, cuenta, articulos WHERE cuenta.id_cliente=cliente.id_cliente AND articulos.id_articulo=cuenta.id_servicio AND cliente.id_cliente=%s", $d_reg_cliente,$d_reg_cliente);
$reg_cliente = mysql_query($query_reg_cliente, $gestionAdmin) or die(mysql_error());
$row_reg_cliente = mysql_fetch_assoc($reg_cliente);
$totalRows_reg_cliente = mysql_num_rows($reg_cliente);

$d_regFichaTecnica = "0";
if (isset($_GET['id_cliente'])) {
  $d_regFichaTecnica = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_regFichaTecnica = sprintf("SELECT * FROM fichatecnica WHERE fichatecnica.id_cliente=%s", $d_regFichaTecnica);
$regFichaTecnica = mysql_query($query_regFichaTecnica, $gestionAdmin) or die(mysql_error());
$row_regFichaTecnica = mysql_fetch_assoc($regFichaTecnica);
$totalRows_regFichaTecnica = mysql_num_rows($regFichaTecnica);


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>::SISTEMA DE GESTION ADMINISTRATIVO ESTRANET WISP::Soporte T&eacute;cnico</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../style.css" rel="stylesheet" type="text/css">
<link href="../css/inphecthyuz.css" rel="stylesheet" type="text/css">
<link href="../css/recibo.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo6 {color: #828B93; font: Tahoma;}
-->
</style>
</head>

<body>
<table width="1000" border="0" align="center" class="borde">
  <tr>
    <td colspan="2" align="center" valign="middle" class="titulo"><p>Soporte Tecnico </p>    </td>
  </tr>
  <tr align="left">
    <td colspan="2" valign="middle">Fecha de Soporte:&nbsp; <?php echo $row_reg_bcliente['fecha']; ?></td>
  </tr>
  <tr>
    <td width="474" align="center" valign="middle"><table width="90%"  border="0">
      <tr>
        <td align="center" class="fondo">Datos del Cliente</td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Cliente: <?php echo $row_reg_cliente['apellido']; ?> <?php echo $row_reg_cliente['nombre']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Razon Social: <?php echo $row_reg_cliente['razonsocial']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Domicilio: <?php echo $row_reg_cliente['domicilio']; ?>&nbsp; <?php echo $row_reg_cliente['barrio']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Fecha de Alta:&nbsp; <?php echo $row_reg_cliente['fecha']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">C.U.I.T.: <?php echo $row_reg_cliente['cuit']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Tel.: <?php echo $row_reg_cliente['telf']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Velociodad: &nbsp;<?php echo $row_reg_cliente['articulo']; ?> - <?php echo $row_reg_cliente['abonomensual']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">E-mail:</td>
      </tr>
    </table></td>
    <td width="512" align="center" valign="top"><table width="90%"  border="0">
      <tr>
        <td align="center" class="fondo">Datos del Cliente</td>
      </tr>
      <tr>
        <td align="left"><table width="100%" class="borde" >
            <tr class="cliente">
              <td width="39%" align="left">IP CLIENTE </td>
              <td width="61%" class="top11"><?php echo $row_regFichaTecnica['ipcliente']; ?></td>
            </tr>
            <tr class="cliente">
              <td align="left">NODO</td>
              <td class="top11"><?php echo $row_regFichaTecnica['nodo']; ?></td>
            </tr>
            <tr class="cliente">
              <td align="left">IP ANTENA </td>
              <td class="top11"><?php echo $row_regFichaTecnica['ipantena']; ?></td>
            </tr>
            <tr class="cliente">
              <td align="left">ESID</td>
              <td class="top11"><?php echo $row_regFichaTecnica['esid']; ?></td>
            </tr>
            <tr class="cliente">
              <td height="14" align="left">
                <label class="Estilo6"></label>
                <label>Mac AP:</label>
              </td>
              <td class="top11"><?php echo $row_regFichaTecnica['macap']; ?></td>
            </tr>
            <tr class="cliente">
              <td height="14" align="left">MAC TARJETA RED </td>
              <td class="top11"><?php echo $row_regFichaTecnica['mactarjetared']; ?></td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr align="center" valign="top">
    <td height="21" colspan="2"><form action="guardarsoporte1.php" method="post" enctype="multipart/form-data" name="form1">
      <table width="100%"  border="0">
        <tr>
          <td align="left" class="fondo">Observaciones </td>
        </tr>
        <tr>
          <td align="left"><textarea name="textarea" cols="60" rows="6"><?php echo $row_reg_bcliente['observaciones']; ?></textarea></td>
        </tr>
        <tr>
          <td align="left" class="fondo">Soporte a Cargo de: </td>
        </tr>
        <tr>
          <td align="left"><input <?php if (!(strcmp($row_reg_bcliente['cargo'],"0"))) {echo "CHECKED";} ?> name="cargo" type="radio" class="nada" value="0" checked>
             La Empresa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input <?php if (!(strcmp($row_reg_bcliente['cargo'],"1"))) {echo "CHECKED";} ?> name="cargo" type="radio" class="nada" value="1">
            &nbsp; Del Cliente 
            <input name="id_cliente" type="hidden" id="id_cliente" value="<?php echo $_GET['id_cliente']; ?>">
            <input name="id_soporte" type="hidden" id="id_soporte" value="<?php echo $row_reg_bcliente['id_soporte']; ?>">
</td>
        </tr>
        <tr>
          <td height="25" align="center" valign="middle">&nbsp;</td>
        </tr>
      </table>
        <input type="submit" name="Submit" value="Grabar">
   &nbsp;&nbsp;&nbsp; 
   <input name="Submit2" type="button" onClick="location.replace('../clientes/index.php')" value="Cancelar">
    </form></td>
  </tr>
</table>
</body>
</html>
<?php
mysql_free_result($reg_cliente);

mysql_free_result($regFichaTecnica);

mysql_free_result($reg_bcliente);
?>
