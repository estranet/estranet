<?php require_once('../Connections/gestionAdmin.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$updateSQL = sprintf("UPDATE soporte SET cargo=%s, observaciones=%s WHERE id_soporte=%s",
				   GetSQLValueString($_POST['cargo'], "int"),
				   GetSQLValueString($_POST['textarea'], "text"),
				   GetSQLValueString($_POST['id_soporte'], "int"));
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$Result1 = mysql_query($updateSQL, $gestionAdmin) or die(mysql_error());

header("location: ../clientes/index.php");

?>
