<?php
require_once('../Connections/gestionAdmin.php');
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

  $insertSQL = sprintf("INSERT INTO soporte (id_cliente, cargo, fecha, observaciones) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_POST['id_cliente'], "int"),
                       GetSQLValueString($_POST['cargo'], "int"),
                       GetSQLValueString($_POST['fecha'], "date"),
                       GetSQLValueString($_POST['textarea'], "text"));
  mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
  $id_soporte=mysql_insert_id($gestionAdmin);
  $url="imprimirrecibo.php?id_cliente=".$_POST['id_cliente']."&id_soporte=".$id_soporte;
  header("location: ".$url);

?>
