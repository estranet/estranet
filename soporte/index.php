<?php require_once('../Connections/gestionAdmin.php'); ?>
<?php
$d_reg_cliente = "0";
if (isset($_GET['id_cliente'])) {
  $d_reg_cliente = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_cliente = sprintf("SELECT cliente.id_cliente, cliente.nombre, cliente.apellido, cliente.razonsocial, cliente.domicilio, cliente.barrio, cliente.fechaAlta AS fecha, cliente.cuit, cliente.telf,cliente.otros, cuenta.abonomensual, articulos.articulo FROM cliente, cuenta, articulos WHERE cuenta.id_cliente=cliente.id_cliente AND articulos.id_articulo=cuenta.id_servicio AND cliente.id_cliente=%s", $d_reg_cliente,$d_reg_cliente);
$reg_cliente = mysql_query($query_reg_cliente, $gestionAdmin) or die(mysql_error());
$row_reg_cliente = mysql_fetch_assoc($reg_cliente);
$totalRows_reg_cliente = mysql_num_rows($reg_cliente);

$d_regFichaTecnica = "0";
if (isset($_GET['id_cliente'])) {
  $d_regFichaTecnica = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_regFichaTecnica = sprintf("SELECT * FROM fichatecnica WHERE fichatecnica.id_cliente=%s", $d_regFichaTecnica);
$regFichaTecnica = mysql_query($query_regFichaTecnica, $gestionAdmin) or die(mysql_error());
$row_regFichaTecnica = mysql_fetch_assoc($regFichaTecnica);
$totalRows_regFichaTecnica = mysql_num_rows($regFichaTecnica);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>::SISTEMA DE GESTION ADMINISTRATIVO ESTRANET WISP::Soporte T&eacute;cnico</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../style.css" rel="stylesheet" type="text/css">
<link href="../css/inphecthyuz.css" rel="stylesheet" type="text/css">
<link href="../css/recibo.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo6 {color: #828B93; font: Tahoma;}
-->
</style>
</head>

<body>
<table width="1000" border="0" align="center" class="borde">
  <tr>
    <td colspan="2" align="center" valign="middle" class="titulo"><p>Soporte Tecnico </p>    </td>
  </tr>
  <tr align="left">
    <td colspan="2" valign="middle">Fecha de Soporte:&nbsp; <?php echo date("d/m/Y"); ?></td>
  </tr>
  <tr>
    <td width="474" align="center" valign="middle"><table width="90%"  border="0">
      <tr>
        <td align="center" class="fondo">Datos del Cliente</td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Cliente: <?php echo $row_reg_cliente['apellido']; ?> <?php echo $row_reg_cliente['nombre']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Razon Social: <?php echo $row_reg_cliente['razonsocial']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Domicilio: <?php echo $row_reg_cliente['domicilio']; ?>&nbsp; <?php echo $row_reg_cliente['barrio']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Fecha de Alta:&nbsp; <?php echo $row_reg_cliente['fecha']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">C.U.I.T.: <?php echo $row_reg_cliente['cuit']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Tel.: <?php echo $row_reg_cliente['telf']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Velociodad: &nbsp;<?php echo $row_reg_cliente['articulo']; ?> - <?php echo $row_reg_cliente['abonomensual']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Otros: <?php echo $row_reg_cliente['otros']; ?></td>
      </tr>
    </table></td>
    <td width="512" align="center" valign="top"><table width="90%"  border="0">
      <tr>
        <td align="center" class="fondo">Datos del Cliente</td>
      </tr>
      <tr>
        <td align="left"><table width="100%" class="borde" >
            <tr class="cliente">
              <td width="39%" align="left">IP CLIENTE </td>
              <td width="61%" class="top11"><?php echo $row_regFichaTecnica['ipcliente']; ?></td>
            </tr>
            <tr class="cliente">
              <td align="left">NODO</td>
              <td class="top11"><?php echo $row_regFichaTecnica['nodo']; ?></td>
            </tr>
            <tr class="cliente">
              <td align="left">IP ANTENA </td>
              <td class="top11"><?php echo $row_regFichaTecnica['ipantena']; ?></td>
            </tr>
            <tr class="cliente">
              <td align="left">ESID</td>
              <td class="top11"><?php echo $row_regFichaTecnica['esid']; ?></td>
            </tr>
            <tr class="cliente">
              <td height="14" align="left">
                <label class="Estilo6"></label>
                <label>Mac AP:</label>
              </td>
              <td class="top11"><?php echo $row_regFichaTecnica['macap']; ?></td>
            </tr>
            <tr class="cliente">
              <td height="14" align="left">MAC TARJETA RED </td>
              <td class="top11"><?php echo $row_regFichaTecnica['mactarjetared']; ?></td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr align="center" valign="top">
    <td height="21" colspan="2"><form action="guardarsoporte.php" method="post" enctype="multipart/form-data" name="form1">
      <table width="100%"  border="0">
        <tr>
          <td align="left" class="fondo">Observaciones </td>
        </tr>
        <tr>
          <td align="left"><textarea name="textarea" cols="60" rows="6"></textarea></td>
        </tr>
        <tr>
          <td align="left" class="fondo">Soporte a Cargo de: </td>
        </tr>
        <tr>
          <td align="left"><input name="cargo" type="radio" class="nada" value="0" checked>
             La Empresa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input name="cargo" type="radio" class="nada" value="1">
            &nbsp; Del Cliente 
            <input name="id_cliente" type="hidden" id="id_cliente" value="<?php echo $_GET['id_cliente']; ?>">
            <input name="fecha" type="hidden" id="fecha" value="<?php echo date("Y-m-d");?>"></td>
        </tr>
        <tr>
          <td height="25" align="center" valign="middle">&nbsp;</td>
        </tr>
      </table>
        <input type="submit" name="Submit" value="Generar  e Imprimir">
   &nbsp;&nbsp;&nbsp; 
   <input name="Submit2" type="button" onClick="history.back()" value="Cancelar">
    </form></td>
  </tr>
</table>
</body>
</html>
<?php
mysql_free_result($reg_cliente);

mysql_free_result($regFichaTecnica);
?>
