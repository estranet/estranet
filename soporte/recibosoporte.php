<?php require_once('../Connections/gestionAdmin.php'); ?>
<?php
$d_reg_datos = "0";
if (isset($_GET['id_soporte'])) {
  $d_reg_datos = (get_magic_quotes_gpc()) ? $_GET['id_soporte'] : addslashes($_GET['id_soporte']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_datos = sprintf("SELECT soporte.id_soporte, soporte.id_cliente, DATE_FORMAT(soporte.fecha,'%%d/%%m/%%Y') AS fecha FROM soporte WHERE soporte.id_soporte=%s", $d_reg_datos,$d_reg_datos);
$reg_datos = mysql_query($query_reg_datos, $gestionAdmin) or die(mysql_error());
$row_reg_datos = mysql_fetch_assoc($reg_datos);
$totalRows_reg_datos = mysql_num_rows($reg_datos);

$d_reg_cliente = "0";
if (isset($_GET['id_cliente'])) {
  $d_reg_cliente = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_cliente = sprintf("SELECT * FROM cliente WHERE cliente.id_cliente=%s", $d_reg_cliente);
$reg_cliente = mysql_query($query_reg_cliente, $gestionAdmin) or die(mysql_error());
$row_reg_cliente = mysql_fetch_assoc($reg_cliente);
$totalRows_reg_cliente = mysql_num_rows($reg_cliente);

$d_reg_datosTecnicos = "0";
if (isset($_GET['id_cliente'])) {
  $d_reg_datosTecnicos = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_datosTecnicos = sprintf("SELECT * FROM fichatecnica WHERE fichatecnica.id_cliente=%s", $d_reg_datosTecnicos);
$reg_datosTecnicos = mysql_query($query_reg_datosTecnicos, $gestionAdmin) or die(mysql_error());
$row_reg_datosTecnicos = mysql_fetch_assoc($reg_datosTecnicos);
$totalRows_reg_datosTecnicos = mysql_num_rows($reg_datosTecnicos);

$d_reg_servicio = "0";
if (isset($_GET['id_cliente'])) {
  $d_reg_servicio = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_servicio = sprintf("SELECT cuenta.abonomensual, articulos.articulo FROM cuenta, articulos WHERE articulos.id_articulo=cuenta.id_servicio AND cuenta.id_cliente=%s", $d_reg_servicio);
$reg_servicio = mysql_query($query_reg_servicio, $gestionAdmin) or die(mysql_error());
$row_reg_servicio = mysql_fetch_assoc($reg_servicio);
$totalRows_reg_servicio = mysql_num_rows($reg_servicio);

$d_reg_datosSoporte = "0";
if (isset($_GET['id_soporte'])) {
  $d_reg_datosSoporte = (get_magic_quotes_gpc()) ? $_GET['id_soporte'] : addslashes($_GET['id_soporte']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_datosSoporte = sprintf("SELECT soporte.id_soporte, soporte.id_cliente, soporte.cargo, soporte.observaciones FROM soporte WHERE soporte.id_soporte=%s", $d_reg_datosSoporte);
$reg_datosSoporte = mysql_query($query_reg_datosSoporte, $gestionAdmin) or die(mysql_error());
$row_reg_datosSoporte = mysql_fetch_assoc($reg_datosSoporte);
$totalRows_reg_datosSoporte = mysql_num_rows($reg_datosSoporte);
//by joni
//para generar imagen mapa
$contents= file_get_contents('http://maps.googleapis.com/maps/api/staticmap?center='.$row_reg_cliente["domicilio_latitud"].','.$row_reg_cliente["domicilio_longitud"].'&zoom=16&size=300x300&maptype=hybrid&sensor=true_or_false\&markers=color:blue|label:S|'.$row_reg_cliente["domicilio_latitud"].','.$row_reg_cliente["domicilio_longitud"].'');
imagejpeg(imagecreatefromstring($contents), "mapa_geolocalizacion.jpeg"); 
echo $row_reg_cliente['domicilio_latitud'];
//end by joni
?>
<?php
require('../pdf/fpdf.php');
class PDF extends FPDF
{
	function Bordes()
	{
		 

			//Arial bold 15
			$this->SetLineWidth(0.03);
			$this->SetDrawColor(0,64,0);
			$this->Rect(1,1,13,19);
			$this->Rect(15.715,1,13,19);
			$this->SetFontSize(11);
			$this->Image("../imagenes/logorecibo.jpg",1.3,1.3,7.2,1.623);
			$this->Image("../imagenes/logorecibo.jpg",16,1.3,7.2,1.623);
			$this->Text(9,2.923,"Soporte Nro: ");
			$this->Text(24,2.923,"Soporte Nro: ");
			$this->SetFont("Times","B",18);
			$this->SetTextColor(0,64,0);
			$this->SetFont("Times","U",18);
			$this->Text(4.459,3.757,"SOPORTE TECNICO");
			$this->Text(19.158,3.757,"SOPORTE TECNICO");
//code by joni
$this->SetDrawColor(64,0,0);
$this->Rect(1.3,11.79,12.49,6);
$this->Image('mapa_geolocalizacion.jpeg' ,3,11.99,9.2,5.623);	
//end code by joni
								
	}
	function Cliente($row_reg_cliente,$row_reg_datosTecnicos,$row_reg_servicio)
	{
		
        //Para la Empresa
		$this->SetLeftMargin(1.3);
		$this->SetY(5.3);
		$this->SetFillColor(225,255,225);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","U",10.602);
		$this->Cell(12.4,0.464,"Cliente",1,1,"L",1);		
		$this->SetY(5.839);
		$this->SetFillColor(255,255,255);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","",10);
		$this->Cell(4.635,0.464,"Nombre y Apellido:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['nombre']." ".$row_reg_cliente['apellido']." ".$row_reg_cliente['razonsocial'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Domicilio:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['domicilio'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Barrio:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['barrio'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Ciudad:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['ciudad'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Telefono:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['telf'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Otros:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['otros'],1,1,"L",1);
		//Para el Cliente
		$this->SetLeftMargin(16);
		$this->SetY(5.3);
		$this->SetFillColor(225,255,225);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","U",10.602);
		$this->Cell(12.4,0.464,"Cliente",1,1,"L",1);		
		$this->SetY(5.839);
		$this->SetFillColor(255,255,255);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","",10);
		$this->Cell(4.635,0.464,"Nombre y Apellido",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['nombre']." ".$row_reg_cliente['apellido']." ".$row_reg_cliente['razonsocial'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Domicilio:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['domicilio'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Barrio:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['barrio'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Ciudad:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['ciudad'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Telefono:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['telf'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Otros:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['otros'],1,1,"L",1);
		$this->SetLeftMargin(1.3);
		//Datos tecnicos para el Cliente
		$this->SetLeftMargin(1.3);
		$this->SetY(8.8);
		$this->SetFillColor(225,255,225);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","U",10.602);
		$this->Cell(12.4,0.464,"Datos T�cnicos",1,1,"L",1);		
		$this->SetY(9.3);
		$this->SetFillColor(255,255,255);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","",11);
		$this->Cell(4.635,0.464,"IP CLIENTE",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_datosTecnicos['ipcliente'],1,1,"L",1);
		$this->Cell(4.635,0.464,"NODO",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_datosTecnicos['nodo'],1,1,"L",1);
		$this->Cell(4.635,0.464,"IP ANTENA",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_datosTecnicos['ipantena'],1,1,"L",1);
		$this->Cell(4.635,0.464,"ESID",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_datosTecnicos['esid'],1,1,"L",1);
		//$this->Cell(4.635,0.464,"Mac AP",1,0,"L",1);
		//$this->Cell(7.765,0.464,$row_reg_datosTecnicos['macap'],1,1,"L",1);
		//$this->Cell(4.635,0.464,"MAC TARJETA DE RED",1,0,"L",1);
		//$this->Cell(7.765,0.464,$row_reg_datosTecnicos['mactarjetared'],1,1,"L",1);
		$this->Cell(4.635,0.464,"SERVICIO",1,0,"L",1);
		$this->Cell(7.765,0.464,"Abono de: $ ".number_format($row_reg_servicio['abonomensual'],2,',','.'),1,1,"L",1);
		//Para la Empresa
		$this->SetLeftMargin(16);
		$this->SetY(8.8);
		$this->SetFillColor(225,255,225);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","U",10.602);
		$this->Cell(12.4,0.464,"Datos T�cnicos",1,1,"L",1);		
		$this->SetY(9.3);
		$this->SetFillColor(255,255,255);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","",11);
		$this->Cell(4.635,0.464,"IP CLIENTE",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_datosTecnicos['ipcliente'],1,1,"L",1);
		$this->Cell(4.635,0.464,"NODO",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_datosTecnicos['nodo'],1,1,"L",1);
		$this->Cell(4.635,0.464,"IP ANTENA",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_datosTecnicos['ipantena'],1,1,"L",1);
		$this->Cell(4.635,0.464,"ESID",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_datosTecnicos['esid'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Mac AP",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_datosTecnicos['macap'],1,1,"L",1);
		$this->Cell(4.635,0.464,"MAC TARJETA DE RED",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_datosTecnicos['mactarjetared'],1,1,"L",1);
		$this->Cell(4.635,0.464,"SERVICIO",1,0,"L",1);
		$this->Cell(7.765,0.464,"Abono de: $ ".number_format($row_reg_servicio['abonomensual'],2,',','.'),1,1,"L",1);
	}
	function Detalle12($observacion,$cargo)
	{
	  	//Observaciones
		$this->SetY(12.3);
		$this->SetLeftMargin(1.3);
		$this->Ln(0.6);
		$this->SetFillColor(225,255,225);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","U",10.602);
		//$this->Rect(1.3,14.581,12.4,2.301,"F");
		//$this->MultiCell(12.4,0.464,"Observaciones:","1","J",1);
		//$this->SetFont("Times","",10);
  	    //$this->SetTextColor(0,0,0);
		//$this->Ln(0.1);
		//$this->MultiCell(12.4,0.464,$observacion,"0","J",0);
		//$this->Rect(1.3,12.9,12.4,2.301,"");
		$this->SetFillColor(225,255,225);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","U",10.602);
		$this->SetY(14.9);
		//$this->Cell(12.4,0.464,"Soporte a Cargo de:",1,1,"L",1);
		//$this->Rect(1.5,15.5,0.5,0.5,"");
/* 		if($cargo==0){
			$this->Line(1.5,15.5,2,16);
			$this->Line(2,15.5,1.5,16);
			$this->Line(16.2,15.5,16.7,16);
			$this->Line(16.7,15.5,16.2,16);
		}else{	
			$this->Line(5,15.5,5.5,16);
			$this->Line(5.5,15.5,5,16);
			$this->Line(19.7,15.5,20.2,16);
			$this->Line(20.2,15.5,19.7,16);
		} */
		//$this->SetFont("Times","",10.602);	
		//$this->Rect(5,15.5,0.5,0.5,"");
		//$this->SetFont("Times","",10.602);	
		//$this->Text(2.2,16, "La Empresa");
		//$this->Text(5.7,16, "Cliente");
		//$this->SetFont("Times","I",10);
		$this->Text(2.3,19.1, "Firma del Cliente ");
		$this->Line(2,18.74,6,18.74);
		$this->Line(8,18.74,11.5,18.74);
		$this->Text(8.7,19.1, "Firma del T�cnico ");
		$this->SetFont("Times","I",10);
		$this->Text(9.671,19.802, "Constacia para la Empresa");
	    //Detalle de Para el Cliente
		$this->SetY(12.3);
		$this->SetLeftMargin(16);
		$this->Ln(0.6);
		$this->SetFillColor(225,255,225);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","U",10.602);
		//$this->Rect(1.3,14.581,12.4,2.301,"F");
		$this->MultiCell(12.4,0.464,"Observaciones:","1","J",1);
		$this->Ln(0.1);
		$this->SetFont("Times","",10);
 	    $this->SetTextColor(0,0,0);
		$this->MultiCell(12.4,0.464,$observacion,"0","J",0);
		$this->Rect(16,12.9,12.4,2.301,"");
		$this->SetFillColor(225,255,225);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","U",10.602);
		$this->SetY(14.9);
		$this->Cell(12.4,0.464,"Soporte a Cargo de:",1,1,"L",1);
		$this->Rect(16.2,15.5,0.5,0.5,"");
		$this->SetFont("Times","",10.602);	
		$this->Rect(19.7,15.5,0.5,0.5,"");
		$this->SetFont("Times","",10.602);	
		$this->Text(16.8,16, "La Empresa");
		$this->Text(20.5,16, "Cliente");
		$this->SetFont("Times","I",10);
		$this->Text(17.3,18.5, "Firma del Cliente ");
		$this->Line(17,18.1,20.6,18.1);
		$this->Line(22.6,18.1,26,18.1);
		$this->Text(23,18.5, "Firma del T�cnico ");
		$this->SetFont("Times","I",10);
		$this->Text(24.7,19.802, "Constacia para del Cliente");
		
	}
	function Datos($row_reg_datos)
	{
	   //Datos del Recibo
       		$this->SetTextColor(0,0,0);
			$this->SetFont("Times","B",16);
			$numeracion=$this->number_pad($row_reg_datos['id_soporte'],5,"0");
  	        $this->Text(11.5,2.923, $numeracion);
			$this->Text(26.5,2.923, $numeracion);
			$this->SetY(4.429);
			$this->SetFillColor(225,255,225);
			$this->SetTextColor(0,0,0);
			$this->SetFont("Times","B",10.602);
			$this->Cell(12.4,0.464,"Fecha de Soporte: ".$row_reg_datos['fecha'],1,1,"L",1);
			$this->SetLeftMargin(16);
			$this->SetY(4.429);
			$this->SetFillColor(225,255,225);
			$this->SetTextColor(0,0,0);
			$this->SetFont("Times","B",10.602);
			$this->Cell(12.4,0.464,"Fecha de Soporte: ".$row_reg_datos['fecha'],1,1,"L",1);	
			
	}
	function number_pad($number,$n,$caracter) {
         return str_pad((int) $number,$n,$caracter,STR_PAD_LEFT);
    }
	

}
//Void
$formaPago=array("Efectivo","Tarjeta de Credito","Cheque");
$pdf=new PDF();
$title="REMITO ELECTRONICO";
//$pdf->remito();
$pdf->SetLeftMargin(1.3);  
$pdf->AddPage('L');
$pdf->Bordes();
//Cargo Datos de Recibo
$pdf->Datos($row_reg_datos);
$pdf->Cliente($row_reg_cliente,$row_reg_datosTecnicos,$row_reg_servicio);
$pdf->Detalle12($row_reg_datosSoporte['observaciones'],$row_reg_datosSoporte['cargo']);
$pdf->SetAuthor('Lic. Hernan Nicolas Davila');
//F para inscrutar
ob_end_clean();
$pdf->Output('recibo.pdf',"F");
?>
<?php
mysql_free_result($reg_datos);

mysql_free_result($reg_cliente);

mysql_free_result($reg_datosTecnicos);

mysql_free_result($reg_servicio);

mysql_free_result($reg_datosSoporte);
?>
