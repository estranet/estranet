<?php
require('fpdf.php');

class PDF extends FPDF
{
	function Bordes()
	{
		 

			//Arial bold 15
			$this->SetLineWidth(0.03);
			$this->SetDrawColor(51,102,255);
			$this->Rect(1,1,13,19);
			$this->Rect(15,1,13,19);
			$this->Rect(6.984,1.3,1,1);
			$this->Rect(20.986,1.3,1,1);
			$this->SetFontSize(24);
			$this->Text(7.215,2.095, "X");
			$this->Text(21.201,2.095, "X");
			$this->Image("../imagenes/logorecibo.jpg",1.3,1.3);
			$this->Image("../imagenes/logorecibo.jpg",15.3,1.3);
			$this->SetFont("Times","BU",12);
			$this->SetTextColor(51,102,255);
			$this->Text(9.427,1.543, "Recibo de Pago");
			$this->Text(23.385,1.543, "Recibo de Pago");
			$this->SetFont("Times","",10.602);
			$this->SetTextColor(0,0,0);
			$this->Text(8.382,2.5, "Recibo Nro: ");
			$this->Text(22.391,2.5, "Recibo Nro: ");
			$this->Text(8.382,2.932, "Fecha: ");
			$this->Text(22.391,2.932, "Fecha: ");
			
	}
	function Cliente()
	{
		
        $this->SetY(3.239);
		$this->SetFillColor(204,236,253);
		$this->SetTextColor(21,102,255);
		$this->SetFont("Times","U",10.602);
		$this->Cell(12.4,0.464,"Cliente",1,1,"L",1);		
	}
}


$pdf=new PDF();
$title="HOLA";
//$pdf->remito();
$pdf->SetLeftMargin(1.3);  
$pdf->AddPage('L');
$pdf->Bordes();
$pdf->Cliente();
$pdf->SetAuthor('Lic. Hernan Nicolas Davila');

$pdf->Output();
?>

