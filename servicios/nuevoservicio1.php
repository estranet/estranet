
<?php require_once('../Connections/gestionAdmin.php'); ?>

<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
  $insertSQL = sprintf("INSERT INTO articulos (codigoArticulo, articulo, tipoarticulo, rubro, precioV) VALUES (%s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['codigoArticulo'], "text"),
                       GetSQLValueString($_POST['articulo'], "text"),
                       GetSQLValueString($_POST['tipoarticulo'], "int"),
                       GetSQLValueString($_POST['rubro'], "text"),
                       GetSQLValueString($_POST['precioV'], "double"));
  mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
  $id_servicio=mysql_insert_id($gestionAdmin);
  $url=sprintf("editarservicio.php?id_servicio=%s",$id_servicio);
  header(sprintf("location:%s",$url));
?>