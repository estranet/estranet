<?php require_once('../Connections/gestionAdmin.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}


  $updateSQL = sprintf("UPDATE articulos SET articulo=%s, tipoarticulo=%s, rubro=%s, precioV=%s, codigoArticulo=%s WHERE id_articulo=%s",
                       GetSQLValueString($_POST['articulo'], "text"),
                       GetSQLValueString($_POST['tipoarticulo'], "int"),
                       GetSQLValueString($_POST['rubro'], "text"),
                       GetSQLValueString($_POST['precioV'], "double"),
                       GetSQLValueString($_POST['codigoArticulo'], "text"),
					    GetSQLValueString($_POST['id_articulo'], "int"));

  mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($updateSQL, $gestionAdmin) or die(mysql_error());
  $url=sprintf("editarservicio.php?id_servicio=%s",$_POST['id_articulo']);
  header(sprintf("location:%s",$url));

?>
