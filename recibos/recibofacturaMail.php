<?php require_once('../Connections/gestionAdmin.php'); ?>
<?php
$id_reg_recibo = "1";
if (isset($_GET['id_recibo'])) {
  $id_reg_recibo = (get_magic_quotes_gpc()) ? $_GET['id_recibo'] : addslashes($_GET['id_recibo']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_recibo = sprintf("SELECT recibo.id_recibo, recibo.nrorecibo, DATE_FORMAT(recibo.fecha,'%%d/%%m/%%Y %%H:%%i:%%s') AS fecha, recibo.observacion, recibo.fpago, recibo.q FROM recibo WHERE recibo.id_recibo=%s", $id_reg_recibo);
$reg_recibo = mysql_query($query_reg_recibo, $gestionAdmin) or die(mysql_error());
$row_reg_recibo = mysql_fetch_assoc($reg_recibo);
$totalRows_reg_recibo = mysql_num_rows($reg_recibo);

$id_reg_cliente = "1";
if (isset($_GET['id_cliente'])) {
  $id_reg_cliente = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_cliente = sprintf("SELECT cliente.nombre, cliente.apellido, cliente.razonsocial, cliente.domicilio, cliente.ciudad, cliente.telf, cuenta.abonomensual FROM cliente, cuenta WHERE cliente.id_cliente=%s AND cuenta.id_cliente=cliente.id_cliente", $id_reg_cliente);
$reg_cliente = mysql_query($query_reg_cliente, $gestionAdmin) or die(mysql_error());
$row_reg_cliente = mysql_fetch_assoc($reg_cliente);
$totalRows_reg_cliente = mysql_num_rows($reg_cliente);

?>
<?php
require('../pdf/fpdf.php');
class PDF extends FPDF
{
	function Bordes()
	{
		 

			//Arial bold 15
			$this->SetLineWidth(0.03);
			$this->SetDrawColor(51,102,255);
			$this->Rect(4,1,13,19.5);
			//$this->Rect(15.715,1,13,19);
			$this->Rect(9.984,1.3,1,1);
			//$this->Rect(21.701,1.3,1,1);
			$this->SetFontSize(24);
			$this->Text(10.215,2.095, "X");
			//$this->Text(24.922,2.095, "X");
			$this->Image("../imagenes/logorecibo.jpg",4.3,1.3);
			//$this->Image("../imagenes/logorecibo.jpg",16,1.3);
			$this->SetFont("Times","BU",18);
			$this->SetTextColor(51,102,255);
			$this->Text(12,1.543, "Recibo de Pago");
			//$this->Text(23.69,1.543, "Recibo de Pago");
			$this->SetFont("Times","",10.602);
			$this->SetTextColor(0,0,0);
			$this->Text(11.382,2.5, "Recibo Nro: ");
			//$this->Text(23.066,2.5, "Recibo Nro: ");
			$this->Text(11.382,2.932, "Fecha: ");
			//$this->Text(23.066,2.932, "Fecha: ");
			
	}
	function Cliente($row_reg_cliente)
	{
		
        //Para la Empresa
		$this->SetLeftMargin(4.3);

		$this->SetY(3.239);
		$this->SetFillColor(204,236,253);
		$this->SetTextColor(21,102,255);
		$this->SetFont("Times","U",10.602);
		$this->Cell(12.4,0.464,"Cliente",1,1,"L",1);		
		$this->SetY(3.949);
		$this->SetFillColor(255,255,255);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","B",10);
		$this->Cell(4.635,0.464,"Nombre/Apellido",1,0,"L",1);
		$this->MultiCell(7.765,0.464,$row_reg_cliente['nombre']." ".$row_reg_cliente['apellido'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Razon Social.",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['razonsocial'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Domicilio:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['domicilio'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Ciudad:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['ciudad'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Telefono:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['telf'],1,1,"L",1);
		//Para el Cliente
		/*$this->SetLeftMargin(16);
		$this->SetY(3.239);
		$this->SetFillColor(204,236,253);
		$this->SetTextColor(21,102,255);
		$this->SetFont("Times","U",10.602);
		$this->Cell(12.4,0.464,"Cliente",1,1,"L",1);		
		$this->SetY(3.949);
		$this->SetFillColor(255,255,255);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","B",10);
		$this->Cell(4.635,0.464,"Nombre/Apellido",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['nombre']." ".$row_reg_cliente['apellido'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Razon Social.",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['razonsocial'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Domicilio:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['domicilio'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Ciudad:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['ciudad'],1,1,"L",1);
		$this->Cell(4.635,0.464,"Telefono:",1,0,"L",1);
		$this->Cell(7.765,0.464,$row_reg_cliente['telf'],1,1,"L",1);
		$this->SetLeftMargin(1.3);*/
	}
	function Detalle12($row_reg_detalle,$total,$pagado,$observacion,$fpago)
	{
	  //Detallde de Pago para la Empresa
$this->SetLeftMargin(4.3);	
	  $this->SetY(6.257);
		$this->SetFillColor(204,236,253);
		$this->SetTextColor(21,102,255);
		$this->SetFont("Times","U",8);
		$this->Cell(12.4,0.464,"Descripcion de Pago",1,1,"L",1);		
		$this->SetY(7.131);
		$this->SetFillColor(255,255,255);
		$this->SetTextColor(0,0,0);
		$this->SetFont("Times","",10);	
		$this->Cell(1.118,0.726,"Cant",1,0,"L",1);
		$this->Cell(8.725,0.726,"Descripcion",1,0,"L",1);
		$this->Cell(1.279,0.726,"Importe",1,0,"L",1);
		$this->Cell(1.279,0.726,"Total",1,1,"L",1);
		$this->SetFont("Times","B",10);
		$cant=count($row_reg_detalle);
		for($i=0;$i<$cant;$i++)
		{
		    $this->Cell(1.118,0.38,$row_reg_detalle[$i]['cantidad'],1,0,"C",1);
			$this->Cell(8.725,0.38,$row_reg_detalle[$i]['articulo'],1,0,"L",1);
			$this->Cell(1.279,0.38,number_format($row_reg_detalle[$i]['precio'],0,',','.'),1,0,"R",1);
			$this->Cell(1.279,0.38,number_format($row_reg_detalle[$i]['preciopag'],0,',','.'),1,1,"R",1);
		}
		for($i=0;$i<(14-$cant);$i++)
		{
		    $this->Cell(1.118,0.38,"",1,0,"L",1);
			$this->Cell(8.725,0.38,"",1,0,"L",1);
			$this->Cell(1.279,0.38,"",1,0,"L",1);
			$this->Cell(1.279,0.38,"",1,1,"L",1);
		}
		$this->SetY(13.3);
		$this->SetFont("Times","B",8);
		$this->Text(12.124,13.65, "Total a Pagar ");
		$this->SetLeftMargin(14.144);
		$this->SetFillColor(204,236,253);
		$this->SetTextColor(0,0,0); 
		$this->SetFont("Times","B",10);
		//$this->Cell(1.279,0.38,number_format($total,2,',','.'),1,0,"R",1);
		$this->Cell(2.558,0.38,number_format($pagado,0,',','.'),1,1,"R",1);
		//Observaciones
		$this->SetTextColor(21,102,255); 
		$this->SetY(13.582);
		$this->SetLeftMargin(4.3);
		$this->Ln(0.5);
		$this->Rect(4.3,14.581,12.4,2.301,"F");
		$this->MultiCell(11.2,0.28,"Observaciones:","0","J",0);
		$this->Ln();
		$this->MultiCell(11.2,0.68,$observacion,"0","J",0);
		$this->Rect(4.3,14.581,12.4,2.301,"");
		$this->Text(5.9,20.1, "Firma del Cobrador ");
		$this->Line(5.4,19.7,9.2,19.7);
		$this->SetFont("Times","I",10);
	}
	function Datos($recibo,$img,$nombreCompleto)
	{
	   //Datos del Recibo
 	        $this->SetFont("Times","B",16);
			$numeracion="01-".$this->number_pad($recibo['nrorecibo'],5,"0");
  	        $this->Text(13.5,2.5, $numeracion);
			//$this->Text(25,2.5, $numeracion);
			$this->SetFont("Times","B",12);
			$this->Text(12.671,2.932, $recibo['fecha']);
			$this->SetTextColor(0,0,0); 
			$this->SetY(13.582);
			$this->SetLeftMargin(4.3);
			if(file_exists($img)){
				$this->Image($img,4.2,17.1,7,2);
				}
		$this->SetFont("Times","B",10);

			$this->Text(6.1,19.3,$nombreCompleto);
			
			$this->Text(5.9,19.6,"ESTRANET S.R.L");


	}
	function number_pad($number,$n,$caracter) {
         return str_pad((int) $number,$n,$caracter,STR_PAD_LEFT);
    }
	

}
//Void
$formaPago=array("Efectivo","Tarjeta de Credito","Cheque");
$pdf=new PDF();
$title="REMITO ELECTRONICO";
//$pdf->remito();
$pdf->SetLeftMargin(1.3);  

$pdf->AddPage('P',"mm",array(210,297));
$pdf->Bordes();
//Cargo Datos de Recibo
$pdf->Datos($row_reg_recibo,$img_path,$nombre_completo);
$pdf->Cliente($row_reg_cliente);

$detalle=array();
if($row_reg_recibo['q']==0){
	$idr_reg_detalle = "1";
	if (isset($_GET['id_recibo'])) {
	  $idr_reg_detalle = (get_magic_quotes_gpc()) ? $_GET['id_recibo'] : addslashes($_GET['id_recibo']);
	}
	$idc_reg_detalle = "1";
	if (isset($_GET['id_cliente'])) {
	  $idc_reg_detalle = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
	}
	mysql_select_db($database_gestionAdmin, $gestionAdmin);
	$query_reg_detalle = sprintf("SELECT recibo_detalle.precio, articulos.articulo, recibo_detalle.cantidad,  recibo_detalle.preciopag, recibo_detalle.id_detalle FROM recibo_detalle, articulos WHERE articulos.id_articulo=recibo_detalle.id_articulo AND recibo_detalle.id_recibo=%s AND recibo_detalle.id_cliente=%s ORDER BY recibo_detalle.id_detalle", $idr_reg_detalle,$idc_reg_detalle);
	$reg_detalle = mysql_query($query_reg_detalle, $gestionAdmin) or die(mysql_error());
	$row_reg_detalle = mysql_fetch_assoc($reg_detalle);
	$totalRows_reg_detalle = mysql_num_rows($reg_detalle);
	
	$idR_reg_total = "1";
	if (isset($_GET['id_recibo'])) {
	  $idR_reg_total = (get_magic_quotes_gpc()) ? $_GET['id_recibo'] : addslashes($_GET['id_recibo']);
	}
	$idC_reg_total = "1";
	if (isset($_GET['id_cliente'])) {
	  $idC_reg_total = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
	}
	mysql_select_db($database_gestionAdmin, $gestionAdmin);
	$query_reg_total = sprintf("SELECT SUM(recibo_detalle.precio) AS total, SUM(recibo_detalle.preciopag) AS pagado FROM recibo_detalle WHERE recibo_detalle.id_recibo=%s AND recibo_detalle.id_cliente=%s", $idR_reg_total,$idC_reg_total);
	$reg_total = mysql_query($query_reg_total, $gestionAdmin) or die(mysql_error());
	$row_reg_total = mysql_fetch_assoc($reg_total);
	$totalRows_reg_total = mysql_num_rows($reg_total);
	$i=0;
	do
	 {
	   $detalle[$i]['precio']=$row_reg_detalle['precio'];
	   switch($row_reg_detalle['id_detalle'])
	   {
		  case 0:
				$id_reg_abono = "1";
				if (isset($_GET['id_cliente'])) {
				  $id_reg_abono = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
				}
				$id1_reg_abono = "1";
				if (isset($_GET['id_recibo'])) {
				  $id1_reg_abono = (get_magic_quotes_gpc()) ? $_GET['id_recibo'] : addslashes($_GET['id_recibo']);
				}
				mysql_select_db($database_gestionAdmin, $gestionAdmin);
				$query_reg_abono = sprintf("SELECT DATE_FORMAT(pagoabono.periodo,'%%c-%%y') AS  periodo FROM pagoabono WHERE pagoabono.id_cliente=%s AND pagoabono.id_recibo=%s", $id_reg_abono,$id1_reg_abono);
				$reg_abono = mysql_query($query_reg_abono, $gestionAdmin) or die(mysql_error());
				$row_reg_abono = mysql_fetch_assoc($reg_abono);
				$totalRows_reg_abono = mysql_num_rows($reg_abono);
				//Periodo
				$dfecha=explode("-",$row_reg_abono['periodo']);
				$meses=array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
				$periodo=$meses[$dfecha[0]-1]." - ".$dfecha[1];
				$Proporcional="";
				if($row_reg_detalle['precio']<$row_reg_cliente['abonomensual']){
				  $detalle[$i]['articulo']="Pago de Proporcional del Abono ".$periodo;
				}else
				  $detalle[$i]['articulo']=$row_reg_detalle['articulo']." ".$periodo.$Proporcional;
				break;
		  case 1:
				$detalle[$i]['articulo']="Descuento por abono en Termino 10 %";
				break;
		  case 2:
				$detalle[$i]['articulo']="Intereses Punitarios 10 %";				
				break;
		  case 3:
				$d_Recordset1 = "1";
				if (isset($_GET['id_cliente'])) {
				  $d_Recordset1 = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
				}
				mysql_select_db($database_gestionAdmin, $gestionAdmin);
				$query_Recordset1 = sprintf("SELECT cuenta.id_cliente, cuenta.finaciacion, cuenta.abonomensual, cuenta.id_servicio FROM cuenta WHERE cuenta.id_cliente=%s", $d_Recordset1);
				$Recordset1 = mysql_query($query_Recordset1, $gestionAdmin) or die(mysql_error());
				$row_Recordset1 = mysql_fetch_assoc($Recordset1);
				$totalRows_Recordset1 = mysql_num_rows($Recordset1);
				$id_reg_pago = "1";
				if (isset($_GET['id_cliente'])) {
				  $id_reg_pago = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
				}
				mysql_select_db($database_gestionAdmin, $gestionAdmin);
				$query_reg_pago = sprintf("SELECT COUNT(pagofinanciacion.cuota) AS cuota FROM pagofinanciacion WHERE pagofinanciacion.id_cliente=%s", $id_reg_pago);
				$reg_pago = mysql_query($query_reg_pago, $gestionAdmin) or die(mysql_error());
				$row_reg_pago = mysql_fetch_assoc($reg_pago);
				$totalRows_reg_pago = mysql_num_rows($reg_pago);
				$detalle[$i]['articulo']="Cuota de Instalacion ".$row_reg_pago['cuota'] . "/".$row_Recordset1['finaciacion'];				
				break;
		 case 4:        
				$dc_reg_saldo = "0";
				$id_cliente=$_GET['id_cliente'];
				if (isset($id_cliente)) {
				$dc_reg_saldo = (get_magic_quotes_gpc()) ? $id_cliente : addslashes($id_cliente);
				}
				mysql_select_db($database_gestionAdmin, $gestionAdmin);
				$query_reg_saldo = sprintf("SELECT saldo.id_cliente, saldo.que FROM saldo WHERE saldo.id_cliente=%s AND saldo.id_tipo=0", $dc_reg_saldo);
				$reg_saldo = mysql_query($query_reg_saldo, $gestionAdmin) or die(mysql_error());
				$row_reg_saldo = mysql_fetch_assoc($reg_saldo);
				$totalRows_reg_saldo = mysql_num_rows($reg_saldo);
				$detalle[$i]['articulo']="Saldo por pago Servicio Periodo:".$row_reg_saldo['que'];	
				mysql_free_result($reg_saldo);
				break;			
		 case 5:        
				$dc_reg_saldo = "0";
				$id_cliente=$_GET['id_cliente'];
				if (isset($id_cliente)) {
				$dc_reg_saldo = (get_magic_quotes_gpc()) ? $id_cliente : addslashes($id_cliente);
				}
				mysql_select_db($database_gestionAdmin, $gestionAdmin);
				$query_reg_saldo = sprintf("SELECT saldo.id_cliente, saldo.que FROM saldo WHERE saldo.id_cliente=%s AND saldo.id_tipo=1", $dc_reg_saldo);
				$reg_saldo = mysql_query($query_reg_saldo, $gestionAdmin) or die(mysql_error());
				$row_reg_saldo = mysql_fetch_assoc($reg_saldo);
				$totalRows_reg_saldo = mysql_num_rows($reg_saldo);
				$detalle[$i]['articulo']="Saldo de Costo de Instalacion  -  Cuota Nro. ".$row_reg_saldo['que'];	
				mysql_free_result($reg_saldo);
				break;		
		case 6:        
				$detalle[$i]['articulo']="Soporte T�cnico";	
				//Pago Soporte Tecnico
				 $updateSQL = sprintf("UPDATE soporte SET pago=%s WHERE id_cliente=%s",1,$_GET['id_cliente']);
				  mysql_select_db($database_gestionAdmin, $gestionAdmin);
				 $Result1 = mysql_query($updateSQL, $gestionAdmin) or die(mysql_error());
				break;
		case 7:
 	            mysql_select_db($database_gestionAdmin, $gestionAdmin);
				$query_reg_preciocable = "SELECT parametros.preciocable FROM parametros";
				$reg_preciocable = mysql_query($query_reg_preciocable, $gestionAdmin) or die(mysql_error());
				$row_reg_preciocable = mysql_fetch_assoc($reg_preciocable);
				$totalRows_reg_preciocable = mysql_num_rows($reg_preciocable);

				 $detalle[$i]['articulo']="Cable UTP x mts a $ ".number_format($row_reg_preciocable['preciocable'],2,'.',',') ;	
				 //Pago cable
				 $updateSQL = sprintf("UPDATE cuenta SET pagoc=%s WHERE id_cliente=%s",0,$_GET['id_cliente']);
				 mysql_select_db($database_gestionAdmin, $gestionAdmin);
				 $Result1 = mysql_query($updateSQL, $gestionAdmin) or die(mysql_error());
			    break;
		 default:
				$d_reg_servicio = "0";
				if (isset($row_reg_detalle['id_detalle'])) {
				  $d_reg_servicio = (get_magic_quotes_gpc()) ? $row_reg_detalle['id_detalle'] : addslashes($row_reg_detalle['id_detalle']);
				}
				mysql_select_db($database_gestionAdmin, $gestionAdmin);
				$query_reg_servicio = sprintf("SELECT servicio.servicio, servicio.financiacion FROM servicio WHERE servicio.id_servicio=%s", $d_reg_servicio);
				$reg_servicio = mysql_query($query_reg_servicio, $gestionAdmin) or die(mysql_error());
				$row_reg_servicio = mysql_fetch_assoc($reg_servicio);
				$totalRows_reg_servicio = mysql_num_rows($reg_servicio);
				//Cuotas
				$id_re_vcuotaCliente = "0";
				if (isset($row_reg_detalle['id_detalle'])) {
				  $id_re_vcuotaCliente = (get_magic_quotes_gpc()) ? $row_reg_detalle['id_detalle'] : addslashes($row_reg_detalle['id_detalle']);
				}
				mysql_select_db($database_gestionAdmin, $gestionAdmin);
				$query_re_vcuotaCliente = sprintf("SELECT COUNT(serviciospagos.id_servicio) AS cuota FROM serviciospagos WHERE serviciospagos.id_servicio=%s", $id_re_vcuotaCliente);
				$re_vcuotaCliente = mysql_query($query_re_vcuotaCliente, $gestionAdmin) or die(mysql_error());
				$row_re_vcuotaCliente = mysql_fetch_assoc($re_vcuotaCliente);
				$totalRows_re_vcuotaCliente = mysql_num_rows($re_vcuotaCliente);
				$cuotaSe=$row_re_vcuotaCliente['cuota'];
				$detalle[$i]['articulo']=$row_reg_servicio['servicio']." ".$cuotaSe."/".$row_reg_servicio['financiacion']." Cuotas";
		         																	
	   }
	   
	   $detalle[$i]['cantidad']=$row_reg_detalle['cantidad'];
	   $detalle[$i]['preciopag']=$row_reg_detalle['preciopag'];
	   $detalle[$i++]['id_detalle']=$row_reg_detalle['id_detalle'];
	   
	 }while($row_reg_detalle = mysql_fetch_assoc($reg_detalle));
		$pdf->Detalle12($detalle,$row_reg_total['total'],$row_reg_total['pagado'],$row_reg_recibo['observacion'],$row_reg_recibo['fpago']);
		mysql_free_result($reg_total);
		mysql_free_result($reg_abono);
}else{
  //Detaale de Recibo Comun?>
<?php
	$r_reg_detalleComun = "0";
	if (isset($_GET['id_recibo'])) {
	  $r_reg_detalleComun = (get_magic_quotes_gpc()) ? $_GET['id_recibo'] : addslashes($_GET['id_recibo']);
	}
	$c_reg_detalleComun = "0";
	if (isset($_GET['id_cliente'])) {
	  $c_reg_detalleComun = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
	}
	mysql_select_db($database_gestionAdmin, $gestionAdmin);
	$query_reg_detalleComun = sprintf("SELECT recibodr.id_recibo, recibodr.id_cliente, recibodr.id_detalle, recibodr.cantidad, recibodr.precio, recibodr.preciopag FROM recibodr WHERE recibodr.id_recibo=%s AND recibodr.id_cliente=%s", $r_reg_detalleComun,$c_reg_detalleComun);
	$reg_detalleComun = mysql_query($query_reg_detalleComun, $gestionAdmin) or die(mysql_error());
	$row_reg_detalleComun = mysql_fetch_assoc($reg_detalleComun);
	$totalRows_reg_detalleComun = mysql_num_rows($reg_detalleComun); 
?>	
  <?php  //recorro detalle de Recibos 
    $i=0;
	do { 
		$detalle[$i]['precio']=$row_reg_detalleComun['precio'];
		$detalle[$i]['articulo']=$row_reg_detalleComun['id_detalle'];
		$detalle[$i]['cantidad']=$row_reg_detalleComun['cantidad'];
		$detalle[$i]['precio']=$row_reg_detalleComun['precio']; 
		$detalle[$i++]['preciopag']=$row_reg_detalleComun['preciopag'];
	}while($row_reg_detalleComun = mysql_fetch_assoc($reg_detalleComun)); 
	$r_reg_total = "0";
	if (isset($_GET['id_recibo'])) {
	  $r_reg_total = (get_magic_quotes_gpc()) ? $_GET['id_recibo'] : addslashes($_GET['id_recibo']);
	}
	$c_reg_total = "0";
	if (isset($_GET['id_cliente'])) {
	  $c_reg_total = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
	}
	mysql_select_db($database_gestionAdmin, $gestionAdmin);
	$query_reg_total = sprintf("SELECT SUM(recibodr.preciopag) AS Total FROM recibodr WHERE recibodr.id_cliente=%s AND recibodr.id_recibo=%s", $c_reg_total,$r_reg_total);
	$reg_total = mysql_query($query_reg_total, $gestionAdmin) or die(mysql_error());
	$row_reg_total = mysql_fetch_assoc($reg_total);
	$totalRows_reg_total = mysql_num_rows($reg_total);
	$pdf->Detalle12($detalle,$row_reg_total['total'],$row_reg_total['Total'],$row_reg_recibo['observacion'],$row_reg_recibo['fpago']);
	mysql_free_result($reg_total);

}?>
<?php
//mysql_free_result($reg_detalleComun); 
?> 
<?php
$pdf->SetAuthor('Lic. Hernan Nicolas Davila'); 
//F para inscrutar
 ob_end_clean(); 
 $pdf->Output('reciboMail.pdf',"F"); ?>
<?php



mysql_free_result($reg_recibo);

mysql_free_result($reg_cliente);

//mysql_free_result($reg_detalle);

//mysql_free_result($Recordset1);

//mysql_free_result($reg_pago);


?>
