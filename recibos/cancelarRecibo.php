<?php  

require_once('../Connections/gestionAdmin.php');
session_name('valido');
session_start();

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
 ?>
<?php
$id_recibo = "1";
if (isset($_GET['id_recibo'])) {
  $id_recibo = (get_magic_quotes_gpc()) ? $_GET['id_recibo'] : addslashes($_GET['id_recibo']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_recibo = sprintf("SELECT recibo.id_recibo, recibo.nrorecibo, DATE_FORMAT(recibo.fecha,'%%d/%%m/%%Y %%H:%%i:%%s') AS fecha, recibo.observacion, recibo.fpago, recibo.q FROM recibo WHERE recibo.id_recibo=%s", $id_recibo);
$reg_recibo = mysql_query($query_reg_recibo, $gestionAdmin) or die(mysql_error());
$row_reg_recibo = mysql_fetch_assoc($reg_recibo);
$totalRows_reg_recibo = mysql_num_rows($reg_recibo);

$id_cliente = "1";
if (isset($_GET['id_cliente'])) {
  $id_cliente = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_cliente = sprintf("SELECT cliente.nombre, cliente.apellido, cliente.razonsocial, cliente.domicilio, cliente.ciudad, cliente.telf, cuenta.abonomensual FROM cliente, cuenta WHERE cliente.id_cliente=%s AND cuenta.id_cliente=cliente.id_cliente", $id_cliente);
$reg_cliente = mysql_query($query_reg_cliente, $gestionAdmin) or die(mysql_error());
$row_reg_cliente = mysql_fetch_assoc($reg_cliente);
$totalRows_reg_cliente = mysql_num_rows($reg_cliente);

$detalle=array();
if($row_reg_recibo['q']==0){


		 try {
			mysqli_begin_transaction($gestionAdmin, MYSQLI_TRANS_START_READ_ONLY);

			mysql_select_db($database_gestionAdmin, $gestionAdmin);
			$query_reg_detalle = sprintf("SELECT recibo_detalle.precio, articulos.articulo, recibo_detalle.cantidad,  recibo_detalle.preciopag, recibo_detalle.id_detalle,recibo_detalle.preciopag FROM recibo_detalle, articulos WHERE articulos.id_articulo=recibo_detalle.id_articulo AND recibo_detalle.id_recibo=%s AND recibo_detalle.id_cliente=%s ORDER BY recibo_detalle.id_detalle", $id_recibo,$id_cliente);
			$reg_detalle = mysql_query($query_reg_detalle, $gestionAdmin) or die(mysql_error());
			$row_reg_detalle = mysql_fetch_assoc($reg_detalle);
			$totalRows_reg_detalle = mysql_num_rows($reg_detalle);
		
			
			do
			 {
			   $detalle[$i]['precio']=$row_reg_detalle['precio'];
			   switch($row_reg_detalle['id_detalle'])
			   {
				  case 0:
						
						mysql_select_db($database_gestionAdmin, $gestionAdmin);
						$query_reg_abono = sprintf("DELETE FROM pagoabono WHERE pagoabono.id_cliente=%s AND pagoabono.id_recibo=%s", $id_cliente,$id_recibo);
						$reg_abono = mysql_query($query_reg_abono, $gestionAdmin) or die(mysql_error());
						break;
				  case 1:
						break;
				  case 2:
						break;
				  case 3:
						mysql_select_db($database_gestionAdmin, $gestionAdmin);
						$query_reg_pago = sprintf("DELETE FROM pagofinanciacion WHERE pagofinanciacion.id_cliente=%s AND pagofinanciacion.id_recibo=%s", $id_cliente,$id_recibo);
						$reg_pago = mysql_query($query_reg_pago, $gestionAdmin) or die(mysql_error());
						break;
				 case 4:        
						
						break;			
				 case 5:        
						
						break;		
				case 6:        
						
						break;
				case 7:
						 //Pago cable
						 $updateSQL = sprintf("UPDATE cuenta SET pagoc=%s WHERE id_cliente=%s",1,$id_cliente);
						 mysql_select_db($database_gestionAdmin, $gestionAdmin);
						 $Result1 = mysql_query($updateSQL, $gestionAdmin) or die(mysql_error());
						break;
				 default:
				 
				 mysql_select_db($database_gestionAdmin, $gestionAdmin);
				 $query_reg_servicio = sprintf("SELECT servicio.servicio, servicio.financiacion, servicio.costo, servicio.saldo, servicio.id_servicio as id_servicio,month(servicio.fecha_primer_pago) as mes,year(servicio.fecha_primer_pago) as anio FROM servicio  WHERE servicio.saldo<0 AND servicio.id_cliente=%s AND servicio.id_servicio=%s", $id_cliente,$row_reg_detalle['id_detalle']);
				 $reg_servicio = mysql_query($query_reg_servicio, $gestionAdmin) or die(mysql_error());
				 $row_reg_servicio = mysql_fetch_assoc($reg_servicio);
				 $totalRows_reg_servicio = mysql_num_rows($reg_servicio);  
			  
				 do{
					 $total=$row_reg_detalle['preciopag'];
					$saldoInsert=$total > 0?$total:($total* -1);
				 $updateSQL = sprintf("UPDATE servicio SET saldo=saldo+%s WHERE id_servicio=%s AND id_cliente=%s",
										GetSQLValueString($saldoInsert, "double"),
										GetSQLValueString($row_reg_detalle['id_detalle'], "int"),
										GetSQLValueString($id_cliente, "int"));
				   mysql_select_db($database_gestionAdmin, $gestionAdmin);
				   $Result1 = mysql_query($updateSQL, $gestionAdmin) or die(mysql_error());
		
		
			 
					  // }
				 }while($row_reg_servicio = mysql_fetch_assoc($reg_servicio));
				  
				 mysql_select_db($database_gestionAdmin, $gestionAdmin);
				 $query_re_vcuotaCliente = sprintf("DELETE FROM serviciospagos WHERE serviciospagos.id_cliente=%s AND serviciospagos.id_recibo=%s", $id_cliente,$id_recibo);
				 $re_vcuotaCliente = mysql_query($query_re_vcuotaCliente, $gestionAdmin) or die(mysql_error());
																							 
			   }
			   
			 }while($row_reg_detalle = mysql_fetch_assoc($reg_detalle));
			
			 mysql_select_db($database_gestionAdmin, $gestionAdmin);
			 $query_reg_detalle = sprintf("DELETE FROM recibo_detalle   WHERE recibo_detalle.id_recibo=%s AND recibo_detalle.id_cliente=%s ", $id_recibo,$id_cliente);
			 $reg_detalle = mysql_query($query_reg_detalle, $gestionAdmin) or die(mysql_error());
			 
			 mysql_select_db($database_gestionAdmin, $gestionAdmin);
				 $query_reg_detalle = sprintf("DELETE FROM recibo   WHERE recibo.id_recibo=%s AND recibo.id_cliente=%s ", $id_recibo,$id_cliente);
				 $reg_detalle = mysql_query($query_reg_detalle, $gestionAdmin) or die(mysql_error());
				 
				 mysqli_commit($gestionAdmin);
		} catch (\Throwable $e) {
		
			mysqli_rollback($gestionAdmin);
			throw $e; // but the error must be handled anyway
		}
	
}else{
  //Detaale de Recibo Comun?>
<?php
	$r_reg_detalleComun = "0";
	if (isset($_GET['id_recibo'])) {
	  $r_reg_detalleComun = (get_magic_quotes_gpc()) ? $_GET['id_recibo'] : addslashes($_GET['id_recibo']);
	}
	$c_reg_detalleComun = "0";
	if (isset($_GET['id_cliente'])) {
	  $c_reg_detalleComun = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
	}
	mysql_select_db($database_gestionAdmin, $gestionAdmin);
	$query_reg_detalleComun = sprintf("SELECT recibodr.id_recibo, recibodr.id_cliente, recibodr.id_detalle, recibodr.cantidad, recibodr.precio, recibodr.preciopag FROM recibodr WHERE recibodr.id_recibo=%s AND recibodr.id_cliente=%s", $r_reg_detalleComun,$c_reg_detalleComun);
	$reg_detalleComun = mysql_query($query_reg_detalleComun, $gestionAdmin) or die(mysql_error());
	$row_reg_detalleComun = mysql_fetch_assoc($reg_detalleComun);
	$totalRows_reg_detalleComun = mysql_num_rows($reg_detalleComun); 
?>	
  <?php  //recorro detalle de Recibos 
    $i=0;
	do { 
		$detalle[$i]['precio']=$row_reg_detalleComun['precio'];
		$detalle[$i]['articulo']=$row_reg_detalleComun['id_detalle'];
		$detalle[$i]['cantidad']=$row_reg_detalleComun['cantidad'];
		$detalle[$i]['precio']=$row_reg_detalleComun['precio']; 
		$detalle[$i++]['preciopag']=$row_reg_detalleComun['preciopag'];
	}while($row_reg_detalleComun = mysql_fetch_assoc($reg_detalleComun)); 
	$r_reg_total = "0";
	if (isset($_GET['id_recibo'])) {
	  $r_reg_total = (get_magic_quotes_gpc()) ? $_GET['id_recibo'] : addslashes($_GET['id_recibo']);
	}
	$c_reg_total = "0";
	if (isset($_GET['id_cliente'])) {
	  $c_reg_total = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
	}
	mysql_select_db($database_gestionAdmin, $gestionAdmin);
	$query_reg_total = sprintf("SELECT SUM(recibodr.preciopag) AS Total FROM recibodr WHERE recibodr.id_cliente=%s AND recibodr.id_recibo=%s", $c_reg_total,$r_reg_total);
	$reg_total = mysql_query($query_reg_total, $gestionAdmin) or die(mysql_error());
	$row_reg_total = mysql_fetch_assoc($reg_total);
	$totalRows_reg_total = mysql_num_rows($reg_total);
	$pdf->Detalle12($detalle,$row_reg_total['total'],$row_reg_total['Total'],$row_reg_recibo['observacion'],$row_reg_recibo['fpago']);
	mysql_free_result($reg_total);

}?>
<?php

?> 

<?php



mysql_free_result($reg_recibo);

mysql_free_result($reg_cliente);

//mysql_free_result($reg_detalle);

//mysql_free_result($Recordset1);

//mysql_free_result($reg_pago);
header(sprintf("location: ../historial/index.php?id_cliente=%s",$id_cliente));

?>
