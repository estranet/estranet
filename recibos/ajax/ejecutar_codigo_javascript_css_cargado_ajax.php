<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Documento sin t&iacute;tulo</title>
<script type="text/javascript">
function nuevoAjax()
{ 
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
	var xmlhttp=false; 
	try 
	{ 
		// Creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); 
	}
	catch(e)
	{ 
		try
		{ 
			// Creacion del objeto AJAX para IE 
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
		catch(E) { xmlhttp=false; }
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp=new XMLHttpRequest(); } 

	return xmlhttp; 
}

if(navigator.userAgent.indexOf("MSIE")>=0) navegador=0; // IE
else navegador=1; // Demas

function traeCodigo()
{
	ajax=nuevoAjax();
	ajax.open("POST", "ejecutar_codigo_javascript_css_cargado_ajax_proceso.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send(null);

	ajax.onreadystatechange=function() 
	{
		if(ajax.readyState==4)
		{
			// Obtengo el XML y separo sus nodos
			var resp=ajax.responseXML;
			var javascript=resp.getElementsByTagName("javascript")[0].childNodes[0].data;
			var css=resp.getElementsByTagName("css")[0].childNodes[0].data;
			
			// Creo el nuevo JS
			var etiquetaScript=document.createElement("script");
			document.getElementsByTagName('head')[0].appendChild(etiquetaScript);
			etiquetaScript.text=javascript;
			
			// Creo el nuevo CSS
			var etiquetaStyle=document.createElement("style");
			document.getElementsByTagName('head')[0].appendChild(etiquetaStyle);
			
			if(navegador==0)
			{
				var contenidoCSS=css.split("{");
				var ultimaEtiquetaStyle=document.styleSheets[document.styleSheets.length-1];
				ultimaEtiquetaStyle.addRule(contenidoCSS[0], "{"+contenidoCSS[1]);
			}
			else
			{
				var contenidoCSS=document.createTextNode(css);
				etiquetaStyle.appendChild(contenidoCSS);
			}
		} 	
	}
}
</script>
</head>

<body>
<a href="#" onclick="traeCodigo()">Trae c&oacute;digo JavaScript y CSS</a>
<br>
<a href="#" onclick="cambiaClase()">Ejecuta c&oacute;digo JavaScript</a>
<br><br>
Primero clickear en el link "Trae c&oacute;digo JavaScript y CSS" para cargar la funci&oacute;n JS y la clase CSS. Luego clickear 
"Ejecuta c&oacute;digo JavaScript" para ejecutar la funci&oacute;n que hemos cargado asincr&oacute;nicamente.
</body>
</html>