<?php require_once('../Connections/gestionAdmin.php'); ?>
<?php
//Guardo recibo
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
  /*
  FORMA DE PAGO
  0 -  Efectivo
  1 -  Tarjeta de Credito
  2 -  Cheque
  */
  $hora=explode(" ",$_POST['fecha']);
  $fecha=explode("/",$hora[0]);
  $fecha=$fecha[2]."-".$fecha[1]."-".$fecha[0]. " ".$hora[1];
/*code by joni*/
$hora=date("H:i:s");
$fecha=$fecha.' '.$hora; 
/*end code by joni*/
  $insertSQL = sprintf("INSERT INTO recibo (nrorecibo, fecha, id_cliente, observacion, fpago) VALUES (%s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['nrorecibo'], "int"),
                       GetSQLValueString($fecha, "date"),
                       GetSQLValueString($_POST['id_cliente'], "int"),
                       GetSQLValueString($_POST['observ'], "text"),
                       GetSQLValueString($_POST['fpago'], "int"));

  mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
 // echo $insertSQL;
  $id_recibo=mysql_insert_id($gestionAdmin);
  $id_cliente=$_POST['id_cliente'];
  //Finaciacion
  if($_POST['cuota']>-1){
	   $insertSQL = sprintf("INSERT INTO pagofinanciacion (id_cliente,id_recibo, cuota, pago) VALUES (%s, %s, %s, %s)",
						   GetSQLValueString($id_cliente, "int"),
						   GetSQLValueString($id_recibo, "int"),
						   GetSQLValueString($_POST['cuota'], "int"),
						   GetSQLValueString($_POST['pcuota'], "double"));
	  mysql_select_db($database_gestionAdmin, $gestionAdmin);
	  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
	 // echo $insertSQL;
	  $insertSQL = sprintf("INSERT INTO recibo_detalle (id_recibo, id_cliente, id_detalle, cantidad, precio, id_articulo, preciopag) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($id_recibo, "int"),
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString(3, "int"),
                       GetSQLValueString(1, "int"),
                       GetSQLValueString($_POST['pcuota1'], "double"),
                       0,
					   GetSQLValueString($_POST['pcuota'], "double"));
	  mysql_select_db($database_gestionAdmin, $gestionAdmin);
      $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
	  //Verifico Saldo para proximo mes
	  if($_POST['pcuota1']>$_POST['pcuota'])
	  {
	    $saldo=$_POST['pcuota1']-$_POST['pcuota'];
		if($_POST['vSaldoC']==-1){
	        $SQL = sprintf("INSERT INTO saldo (id_cliente, id_tipo, saldo, que) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString(1, "int"),
                       GetSQLValueString($saldo, "double"),
                       GetSQLValueString($_POST['cuota'], "text"));	
		}else{
		    $SQL = sprintf("UPDATE saldo SET saldo=(saldo+%s), que=%s WHERE id_cliente=%s AND id_tipo=%s",
                       GetSQLValueString($saldo, "double"),
                       GetSQLValueString($_POST['cuota'], "text"),
                       GetSQLValueString($id_cliente, "int"),
                       1);
		}			   
		 mysql_select_db($database_gestionAdmin, $gestionAdmin);
		 $Result1 = mysql_query($SQL, $gestionAdmin) or die(mysql_error());
	  
	  }		  
  }
  //Pago de Saldo de Cuota
  if($_POST['vSaldoC']>0){
       $saldo=$_POST['vSaldoC']-$_POST['pSaldoC']; 
       $SQL = sprintf("UPDATE saldo SET saldo=%s, que=%s WHERE id_cliente=%s AND id_tipo=%s",
                       GetSQLValueString($saldo, "double"),
                       GetSQLValueString($_POST['dSaldoC'], "text"),
                       GetSQLValueString($id_cliente, "int"),
                       1);  
		mysql_select_db($database_gestionAdmin, $gestionAdmin);
		$Result1 = mysql_query($SQL, $gestionAdmin) or die(mysql_error());		
		//Guardo pago de Saldo de Cuotas
		 $insertSQL = sprintf("INSERT INTO pagossaldos (id_cliente, id_recibo, id_tipo, que, fecha, pago) VALUES (%s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString($id_recibo, "int"),
                       1,
                       GetSQLValueString($_POST['dSaldoC'], "text"),
                       GetSQLValueString($fecha, "date"),
					   GetSQLValueString($_POST['pSaldoC'], "date"));
	  mysql_select_db($database_gestionAdmin, $gestionAdmin);
	  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
	  //Guardo Detalle
	  $insertSQL = sprintf("INSERT INTO recibo_detalle (id_recibo, id_cliente, id_detalle, cantidad, precio, id_articulo, preciopag) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($id_recibo, "int"),
                       GetSQLValueString($id_cliente, "int"),
                       5,
                       1,
                       GetSQLValueString($_POST['vSaldoC'], "double"),
                       0,
					   GetSQLValueString($_POST['pSaldoC'], "double"));
	
	  mysql_select_db($database_gestionAdmin, $gestionAdmin);
	 $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
  }
  //Pago de Servicio;
  
  $periodo=explode("-",$_POST['periodo']);
  $periodo=$periodo[1]."-".$periodo[0]."-01";
  //echo $periodo;
  $insertSQL = sprintf("INSERT INTO pagoabono (id_cliente, id_recibo, periodo, pago) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString($id_recibo, "int"),
                       GetSQLValueString($periodo, "date"),
                       GetSQLValueString($_POST['imporPagado'], "double"));
  //echo $insertSQL;
  mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
  //INSERTO SERVICIO
   $insertSQL = sprintf("INSERT INTO recibo_detalle (id_recibo, id_cliente, id_detalle, cantidad, precio, id_articulo, preciopag) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($id_recibo, "int"),
                       GetSQLValueString($id_cliente, "int"),
                       0,
                       1,
					   GetSQLValueString($_POST['pservicio'], "double"),
                       GetSQLValueString($_POST['id_articulo'], "int"),
					   GetSQLValueString($_POST['imporPagado'], "double"));

  mysql_select_db($database_gestionAdmin, $gestionAdmin);
 // echo $insertSQL."P";
  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
  //Verifico Saldo para el proximo periodo
   if($_POST['pservicio']>$_POST['imporPagado']){
  		$meses=array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");
		$saldo=$_POST['pservicio']-$_POST['imporPagado'];
		$fecha_db=explode("-",$periodo);
		$fecha_cambiada = mktime(0,0,0,$fecha_db[1],$fecha_db[2],$fecha_db[0]);
		$que=$meses[date("n",$fecha_cambiada)-1]." / ".date("Y",$fecha_cambiada);
		if($_POST['vSaldo']==-1){
	        $SQL = sprintf("INSERT INTO saldo (id_cliente, id_tipo, saldo, que) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($id_cliente, "int"),
                       0,
                       GetSQLValueString($saldo, "double"),
                       GetSQLValueString($que, "text"));	
		}else{
		    $SQL = sprintf("UPDATE saldo SET saldo=(saldo+%s), que=%s WHERE id_cliente=%s AND id_tipo=%s",

                       GetSQLValueString($saldo, "double"),
                       GetSQLValueString($que, "text"),
                       GetSQLValueString($id_cliente, "int"),
                       0);
		}			   
		 mysql_select_db($database_gestionAdmin, $gestionAdmin);
		 $Result1 = mysql_query($SQL, $gestionAdmin) or die(mysql_error());
  }
  //VERIFICO PAGO DE SALDO PARA EL REMITO ACTUAL
  if($_POST['vSaldo']>0){
    $saldo=$_POST['vSaldo']-$_POST['pSaldo']; 
    $SQL = sprintf("UPDATE saldo SET saldo=%s, que=%s WHERE id_cliente=%s AND id_tipo=%s",
                       GetSQLValueString($saldo, "double"),
                       GetSQLValueString($_POST['dSaldo'], "text"),
                       GetSQLValueString($id_cliente, "int"),
                       0);  
	mysql_select_db($database_gestionAdmin, $gestionAdmin);
    $Result1 = mysql_query($SQL, $gestionAdmin) or die(mysql_error());				   
	//INSERTO DETALLE DEL RECIBO
	$insertSQL = sprintf("INSERT INTO recibo_detalle (id_recibo, id_cliente, id_detalle, cantidad, precio, id_articulo, preciopag) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($id_recibo, "int"),
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString("4", "int"),
                       GetSQLValueString("1", "int"),
                       GetSQLValueString($_POST['vSaldo'], "double"),
                       0,
					   GetSQLValueString($_POST['pSaldo'], "double"));
	
	  mysql_select_db($database_gestionAdmin, $gestionAdmin);
	 $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
	 //Guardo Pago de Saldo
	  $insertSQL = sprintf("INSERT INTO pagossaldos (id_cliente, id_recibo, id_tipo, que, fecha, pago) VALUES (%s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString($id_recibo, "int"),
                       GetSQLValueString("0", "int"),
                       GetSQLValueString($_POST['dSaldo'], "text"),
                       GetSQLValueString($fecha, "date"),
					   GetSQLValueString($_POST['pSaldo'], "date"));
	  mysql_select_db($database_gestionAdmin, $gestionAdmin);
	  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
  }
  //Verifico si hay descuento
  if($_POST['descuento']<0)
  {
     $insertSQL = sprintf("INSERT INTO recibo_detalle (id_recibo, id_cliente, id_detalle, cantidad, precio, id_articulo, preciopag) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($id_recibo, "int"),
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString("1", "int"),
                       GetSQLValueString("1", "int"),
                       GetSQLValueString($_POST['descuento'], "double"),
                       0,
					   GetSQLValueString($_POST['descuento'], "double"));
	
	  mysql_select_db($database_gestionAdmin, $gestionAdmin);
	 $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
	//  echo $insertSQL;
  }elseif(strcmp($_POST['descuento'],"NO")!=0){
     $insertSQL = sprintf("INSERT INTO recibo_detalle (id_recibo, id_cliente, id_detalle, cantidad, precio, id_articulo, preciopag) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($id_recibo, "int"),
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString("2", "int"),
                       GetSQLValueString("1", "int"),
                       GetSQLValueString($_POST['descuento'], "double"),
                       0,
					   GetSQLValueString($_POST['descuento'], "double"));
	
	  mysql_select_db($database_gestionAdmin, $gestionAdmin);
	  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
	  //echo $insertSQL;
  }
  //Verifico Soporte 
 if(isset($_POST['precSoportePag']))
 {
   //Agrego detalle de Soporte
   $insertSQL = sprintf("INSERT INTO recibo_detalle (id_recibo, id_cliente, id_detalle, cantidad, precio, id_articulo, preciopag) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($id_recibo, "int"),
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString("6", "int"),
                       GetSQLValueString($_POST['cantSoporte'], "int"),
                       GetSQLValueString($_POST['precioSoporT'], "double"),
                       0,
					   GetSQLValueString($_POST['precSoportePag'], "double"));
	
	  mysql_select_db($database_gestionAdmin, $gestionAdmin);
	  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
   ///
 } 
//Verifico pago de Cable
if(isset($_POST['bcable']) AND $_POST['bcable']==1){

	   $insertSQL = sprintf("INSERT INTO recibo_detalle (id_recibo, id_cliente, id_detalle, cantidad, precio, id_articulo, preciopag) VALUES (%s, %s, %s, %s, %s, %s, %s)",
						   GetSQLValueString($id_recibo, "int"),
						   GetSQLValueString($id_cliente, "int"),
						   GetSQLValueString("7", "int"),
						   GetSQLValueString($_POST['CCable'], "int"),
						   GetSQLValueString($_POST['totalC'], "double"),
						   0,
						   GetSQLValueString($_POST['totalC'], "double"));
		
		  mysql_select_db($database_gestionAdmin, $gestionAdmin);
		 $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
}
//Verifico servico
if(isset($_POST['vser']) AND $_POST['vser']>0 )
{
    $id_reg_servicio = "0";
	if (isset($id_cliente)) {
	  $id_reg_servicio = (get_magic_quotes_gpc()) ? $id_cliente : addslashes($id_cliente);
	}
	mysql_select_db($database_gestionAdmin, $gestionAdmin);
	$query_reg_servicio = sprintf("SELECT servicio.id_servicio, servicio.id_cliente FROM servicio WHERE servicio.saldo>0 AND servicio.id_cliente=%s", $id_reg_servicio);
	$reg_servicio = mysql_query($query_reg_servicio, $gestionAdmin) or die(mysql_error());
	$row_reg_servicio = mysql_fetch_assoc($reg_servicio);
	$totalRows_reg_servicio = mysql_num_rows($reg_servicio);  
	do{
	    
	    $total=$_POST["c".$row_reg_servicio['id_servicio']];
		$cuota=$_POST["cu".$row_reg_servicio['id_servicio']]; 
	    $insertSQL = sprintf("INSERT INTO recibo_detalle (id_recibo, id_cliente, id_detalle, cantidad, precio, id_articulo, preciopag) VALUES (%s, %s, %s, %s, %s, %s, %s)",
						   GetSQLValueString($id_recibo, "int"),
						   GetSQLValueString($id_cliente, "int"),
						   GetSQLValueString($row_reg_servicio['id_servicio'] , "int"),
						   GetSQLValueString(1, "int"),
						   GetSQLValueString($total, "double"),
						   0,
						   GetSQLValueString($total, "double"));
		
		  mysql_select_db($database_gestionAdmin, $gestionAdmin);
		 $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
	    //Pago de Servicio:
		 $insertSQL = sprintf("INSERT INTO serviciospagos (id_cliente, id_servicio, pago, cuota, fecha, id_recibo) VALUES (%s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($id_cliente, "int"),
                       GetSQLValueString($row_reg_servicio['id_servicio'], "int"),
                       GetSQLValueString($total, "double"),
                       GetSQLValueString($cuota, "int"),
                       GetSQLValueString($fecha, "date"),
                       GetSQLValueString($id_recibo, "int"));		
		  mysql_select_db($database_gestionAdmin, $gestionAdmin);
		  $Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());
		  //Actualizo Saldo
			$updateSQL = sprintf("UPDATE servicio SET saldo=saldo-%s WHERE id_servicio=%s AND id_cliente=%s",
								   GetSQLValueString($total, "double"),
								   GetSQLValueString($row_reg_servicio['id_servicio'], "int"),
								   GetSQLValueString($id_cliente, "int"));
			
			  mysql_select_db($database_gestionAdmin, $gestionAdmin);
			  $Result1 = mysql_query($updateSQL, $gestionAdmin) or die(mysql_error());
	}while($row_reg_servicio = mysql_fetch_assoc($reg_servicio));
	 

}

//codigo andres

$msg_log="Cobro de cuota";

if(isset($_POST['Apellido'])and isset($_POST['nombre'])and isset($_POST['id_usuario'])){
$updateSQL=sprintf("INSERT INTO log(id_usuario,id_recibo,descripcion) VALUES(%s,%s,%s)", GetSQLValueString($_POST['id_usuario'], "int"), GetSQLValueString($id_recibo, "int"), GetSQLValueString($msg_log.' del cliente: '.$_POST['Apellido'].' '.$_POST['nombre'], "text"));
mysql_select_db($database_gestionAdmin, $gestionAdmin);
  $Result1 = mysql_query($updateSQL, $gestionAdmin) or die(mysql_error());
}
// fin codigo andres

header(sprintf("location:imprimirrecibo.php?id_cliente=%s&id_recibo=%s",$id_cliente,$id_recibo))
?>
