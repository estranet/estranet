<?php
function compara_fechas($fecha1,$fecha2)
{
    if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha1))
        list($dia1,$mes1,$ano1)=split("/",$fecha1);
    if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha1))
        list($dia1,$mes1,$ano1)=split("-",$fecha1);
    if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha2))
        list($dia2,$mes2,$ano2)=split("/",$fecha2);
    if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha2))
        list($dia2,$mes2,$ano2)=split("-",$fecha2);
    $dif = mktime(0,0,0,$mes1,$dia1,$ano1) - mktime(0,0,0, $mes2,$dia2,$ano2);
    return ($dif);                         
}
//Restar fechas
function restaFechas($dFecIni, $dFecFin)
{
$dFecIni = str_replace("-","",$dFecIni);
$dFecIni = str_replace("/","",$dFecIni);
$dFecFin = str_replace("-","",$dFecFin);
$dFecFin = str_replace("/","",$dFecFin);

ereg( "([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecIni, $aFecIni);
ereg( "([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecFin, $aFecFin);

$date1 = mktime(0,0,0,$aFecIni[2], $aFecIni[1], $aFecIni[3]);
$date2 = mktime(0,0,0,$aFecFin[2], $aFecFin[1], $aFecFin[3]);

return round(($date2 - $date1) / (60 * 60 * 24));
}
# PARAMETROS: 
# $fecha_nacimiento - Fecha de nacimiento de una persona. 
# 
# $fecha_control - Fecha actual o fecha a consultar. 
# 
# 
# EJEMPLO: 
# tiempo_transcurrido('22/06/1977', '04/05/2009'); 
# 
function tiempo_transcurrido($fecha_nacimiento, $fecha_control) 
{ 
   $fecha_actual = $fecha_control; 
    
   if(!strlen($fecha_actual)) 
   { 
      $fecha_actual = date('d-m-Y'); 
   } 
 
   // separamos en partes las fechas  
   $array_nacimiento = explode ( "-", $fecha_nacimiento );  
   $array_actual = explode ( "-", $fecha_actual );  
 
   $anos =  $array_actual[2] - $array_nacimiento[2]; // calculamos a�os  
   $meses = $array_actual[1] - $array_nacimiento[1]; // calculamos meses  
   $dias =  $array_actual[0] - $array_nacimiento[0]; // calculamos d�as  
 
   //ajuste de posible negativo en $d�as  
   if ($dias < 0)  
   {  
      --$meses;  
 
      //ahora hay que sumar a $dias los dias que tiene el mes anterior de la fecha actual  
      switch ($array_actual[1]) {  
         case 1:  
            $dias_mes_anterior=31; 
            break;  
         case 2:      
            $dias_mes_anterior=31; 
            break;  
         case 3:   
            if (bisiesto($array_actual[2]))  
            {  
               $dias_mes_anterior=29; 
               break;  
            }  
            else  
            {  
               $dias_mes_anterior=28; 
               break;  
            }  
         case 4: 
            $dias_mes_anterior=31; 
            break;  
         case 5: 
            $dias_mes_anterior=30; 
            break;  
         case 6: 
            $dias_mes_anterior=31; 
            break;  
         case 7: 
            $dias_mes_anterior=30; 
            break;  
         case 8: 
            $dias_mes_anterior=31; 
            break;  
         case 9: 
            $dias_mes_anterior=31; 
            break;  
         case 10: 
            $dias_mes_anterior=30; 
            break;  
         case 11: 
            $dias_mes_anterior=31; 
            break;  
         case 12: 
            $dias_mes_anterior=30; 
            break;  
      }  
 
      $dias=$dias + $dias_mes_anterior; 
 
      if ($dias < 0) 
      { 
         --$meses; 
         if($dias == -1) 
         { 
            $dias = 30; 
         } 
         if($dias == -2) 
         { 
            $dias = 29; 
         } 
      } 
   } 
 
   //ajuste de posible negativo en $meses  
   if ($meses < 0)  
   {  
      --$anos;  
      $meses=$meses + 12;  
   } 
 
   $tiempo[0] = $anos; 
   $tiempo[1] = $meses; 
   $tiempo[2] = $dias; 
   $tiempo=$tiempo[0]*12+$tiempo[1]*1;
   return $tiempo; 
} 
 
function bisiesto($anio_actual){  
   $bisiesto=false;  
   //probamos si el mes de febrero del a�o actual tiene 29 d�as  
     if (checkdate(2,29,$anio_actual))  
     {  
      $bisiesto=true;  
   }  
   return $bisiesto;  
} 
 
?>

