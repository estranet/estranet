<?php 
function number_pad($number,$n,$caracter) {
   return str_pad((int) $number,$n,$caracter,STR_PAD_LEFT);
}
$fecha=date("d/m/Y H:i:s");
$fecha1=date('Y-m-d H:i:s');
?>
<?php require_once('../Connections/gestionAdmin.php'); ?>
<?php
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_numeracion = "SELECT MAX(recibo.nrorecibo) AS nro FROM recibo";
$reg_numeracion = mysql_query($query_reg_numeracion, $gestionAdmin) or die(mysql_error());
$row_reg_numeracion = mysql_fetch_assoc($reg_numeracion);
$totalRows_reg_numeracion = mysql_num_rows($reg_numeracion);

$id_reg_datosCliente = "1";
if (isset($_GET['id_cliente'])) {
  $id_reg_datosCliente = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_datosCliente = sprintf("SELECT cliente.nombre, cliente.apellido, cliente.razonsocial, cliente.domicilio, cliente.ciudad, cliente.telf FROM cliente WHERE cliente.id_cliente=%s", $id_reg_datosCliente);
$reg_datosCliente = mysql_query($query_reg_datosCliente, $gestionAdmin) or die(mysql_error());
$row_reg_datosCliente = mysql_fetch_assoc($reg_datosCliente);
$totalRows_reg_datosCliente = mysql_num_rows($reg_datosCliente);
?>
<?php
require_once('../xajax/xajax.inc.php'); //incluimos la librelia xajax
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }

  return $theValue;
}
function eliminarFila($id_campo, $cant_campos,$resto){
	$respuesta = new xajaxResponse();
	$respuesta->addAssign("txtTotal" ,"value", $resto);
	$respuesta->addAssign("divTotal","innerHTML", $resto);   
	$respuesta->addRemove("rowDetalle_$id_campo"); //borro el detalle que indica el parametro id_campo
	-- $cant_campos; //Resto uno al numero de campos y si es cero borro todo

	if($cant_campos == 0){
		$respuesta->addRemove("rowDetalle_0");
		$respuesta->addAssign("num_campos", "value", "0"); //dejo en cero la cantidad de campos para seguir agregando si asi lo desea el usuario
		$respuesta->addAssign("cant_campos", "value", "0");
	}
    $respuesta->addAssign("cant_campos", "value", $cant_campos);    
	return $respuesta;
}

function cancelar(){  //elimina todo el contenido de la tabla y vuelve a cero los contadores
    
    $respuesta = new xajaxResponse();

    $respuesta->addRemove("tbDetalle"); //vuelve a crear la tabla vacia
    $respuesta->addCreate("tblDetalle", "tbody", "tbDetalle");
    $respuesta->addAssign("num_campos", "value", "0");
	$respuesta->addAssign("cant_campos", "value", "0");
	$respuesta->addAssign("txtTotal", "value", "0");
	$respuesta->addAssign("divTotal", "innerHTML", "0");
    return $respuesta;
}

function agregarFila($formulario){
    $respuesta = new xajaxResponse();    
	extract($formulario);	
	$id_campos = $cant_campos = $num_campos+1;
	$total=$txtTotal+($txtCantidad*$txtPrecio);
    $str_html_td1 = $txtCantidad . '<input type="hidden" id="hdnCantidad_' . $id_campos . '" name="hdnCantidad_' . $id_campos . '" value="' . $txtCantidad . '"/>' ;
    $str_html_td2 = "$txtArticulo" . '<input type="hidden" id="hdnArticulo_' . $id_campos . '" name="hdnArticulo_' . $id_campos . '" value="' . $txtArticulo . '"/>' ;
    $str_html_td3 = "$txtPrecio" . '<input type="hidden" id="hdnPrecio_' . $id_campos . '" name="hdnPrecio_' . $id_campos . '" value="' . $txtPrecio . '"/>' ;
    $str_html_td4 = '<img src="ajax/images/delete.png" width="16" height="16" alt="Eliminar" onclick="if(confirm(\'Realmente desea eliminar este detalle?\')){actualizo('. $id_campos .',proyecto.cant_campos.value,'.($txtCantidad*$txtPrecio).');}"/>';
    $str_html_td4 .= '<input type="hidden" id="hdnIdCampos_'. $id_campos .'" name="hdnIdCampos[]" value="'. $id_campos .'" />';
    if($num_campos == 0){ // creamos un encabezado de lo contrario solo agragamos la fila
		$respuesta->addCreate("tbDetalle", "tr", "rowDetalle_0");
        $respuesta->addCreate("rowDetalle_0", "th", "tdDetalle_01");    //creamos los campos
        $respuesta->addCreate("rowDetalle_0", "th", "tdDetalle_02");
        $respuesta->addCreate("rowDetalle_0", "th", "tdDetalle_03");
        $respuesta->addCreate("rowDetalle_0", "th", "tdDetalle_04");
       

        $respuesta->addAssign("tdDetalle_01", "innerHTML", "Cantidad");   //asignamos el contenido
        $respuesta->addAssign("tdDetalle_02", "innerHTML", "Descripcion");
        $respuesta->addAssign("tdDetalle_03", "innerHTML", "Precio");
        $respuesta->addAssign("tdDetalle_04", "innerHTML", "Eliminar");
	}
    $idRow = "rowDetalle_$id_campos";
    $idTd = "tdDetalle_$id_campos";
	$respuesta->addCreate("tbDetalle", "tr", $idRow);
    $respuesta->addCreate($idRow, "td", $idTd."1");     //creamos los campos
    $respuesta->addCreate($idRow, "td", $idTd."2");
    $respuesta->addCreate($idRow, "td", $idTd."3");
    $respuesta->addCreate($idRow, "td", $idTd."4");
   
/*
 *     Esta parte podria estar dentro de algun ciclo iterativo  */
    
    $respuesta->addAssign($idTd."1", "innerHTML", $str_html_td1);   //asignamos el contenido
    $respuesta->addAssign($idTd."2", "innerHTML", $str_html_td2);
    $respuesta->addAssign($idTd."3", "innerHTML", $str_html_td3);
    $respuesta->addAssign($idTd."4", "innerHTML", $str_html_td4);

/*  aumentamos el contador de campos  */
	//vuelvo a cero los campos
   	$respuesta->addAssign("txtCantidad","value", "");
  	$respuesta->addAssign("txtArticulo","value", "");
   	$respuesta->addAssign("txtPrecio","value", ""); 
	$respuesta->addAssign("num_campos","value", $id_campos);
	$respuesta->addAssign("cant_campos" ,"value", $id_campos);  
	$respuesta->addAssign("txtTotal" ,"value", $total);  
	$respuesta->addAssign("divTotal","innerHTML", "$ ". number_format($total,2));  
	return $respuesta;
}

function guardar($formulario){
    $flag = 0;
    extract($formulario);
    $respuesta = new xajaxResponse();
	$cantidad=count($hdnIdCampos);
	if($formulario['cant_campos']>0){
		include('../Connections/gestionAdmin.php');
		$insertSQL = sprintf("INSERT INTO recibo (nrorecibo, fecha, id_cliente, observacion, fpago, q) VALUES (%s, %s, %s, %s, %s, %s)",
						   GetSQLValueString($formulario['nrorecibo'], "int"),
						   GetSQLValueString($formulario['fecha'], "date"),
						   GetSQLValueString($formulario['id_cliente'] , "int"),
						   GetSQLValueString($formulario['observ'] , "text"),
						   GetSQLValueString($formulario['fpago'], "int"),
						   GetSQLValueString("1", "int"));
		mysql_select_db($database_gestionAdmin, $gestionAdmin);
		$Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());		
		$id_recibo=mysql_insert_id($gestionAdmin);			
		//$respuesta->addAlert($insertSQL);
		foreach($hdnIdCampos as $id){      // As� recorro cada campo en cada linea
			//	Guardo la consulta en una cadena
			 $insertSQL = sprintf("INSERT INTO recibodr (id_recibo, id_cliente, id_detalle, cantidad, precio, preciopag) VALUES (%s, %s, %s, %s, %s, %s)",
						   GetSQLValueString($id_recibo, "int"),
						   GetSQLValueString($formulario['id_cliente'], "int"),
						   GetSQLValueString($formulario['hdnArticulo_'.$id], "text"),
						   GetSQLValueString($formulario['hdnCantidad_'.$id], "int"),
						   GetSQLValueString($formulario['hdnPrecio_'.$id], "double"),
						   GetSQLValueString(($formulario['hdnPrecio_'.$id]*$formulario['hdnCantidad_'.$id]), "double"));
			//$respuesta->addAlert($insertSQL);
			mysql_select_db($database_gestionAdmin, $gestionAdmin);
			$Result1 = mysql_query($insertSQL, $gestionAdmin) or die(mysql_error());		
	 }
	 }else
	    {
		  $flag=1;  
		}
    if($flag == 0){
//		$conn->EjecutarSQL("COMMIT TRANSACTION A1");
		$MSG = "Datos guardados con exito";
		$respuesta->addAlert($MSG);
	  	$respuesta->addAssign("id_recibo","value", $id_recibo);
		$respuesta->addScriptCall('ir');
		//header("location:imprimir.php");
	}else{
	    $MSG="Debe Ingresar como Minimo un Detalle";    
    	$respuesta->addAlert($MSG);
	}
    return $respuesta;

}



$xajax=new xajax();         // Crea un nuevo objeto xajax
$xajax->setCharEncoding("iso-8859-1"); // le indica la codificaci�n que debe utilizar
$xajax->decodeUTF8InputOn();            // decodifica los caracteres extra�os
$xajax->registerFunction("agregarFila"); //Registramos la funci�n para indicar que se utilizar� con xajax.
$xajax->registerFunction("cancelar");
$xajax->registerFunction("eliminarFila");
$xajax->registerFunction("guardar");
$xajax->processRequests();
?>
<?php
//echo $descuento;
  if($totalRows_reg_numeracion==0)
     $numeracion=1;
  else
     $numeracion=$row_reg_numeracion['nro']+1;
	 
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<?php $xajax->printJavascript("../xajax"); //imprime el codigo javascript necesario para que funcione todo. ?>
<script language="javascript" type="text/javascript">
function actualizo(id_campo,cantidad,resto)
{
  var total=document.proyecto.txtTotal.value;
  resto=total-resto;
  xajax_eliminarFila(id_campo,cantidad,resto)
}
function ir()
{
  var id_recibo=document.getElementById("id_recibo").value;
  var id_cliente=document.getElementById("id_cliente").value;
  location.replace('imprimirrecibo.php?id_cliente='+id_cliente+'&id_recibo='+id_recibo);
  //alert(id_recibo);
}
</script>
<script language="javascript" type="text/javascript" src="../clientes/masks.js" ></script>
<script language="javascript" type="text/javascript">
  function init(){
	document.proyecto.reset();
	oNumberMask = new Mask("#.00", "number");
	oMask  = new Mask("###", "number");
	oNumberMask.attach(document.proyecto.txtPrecio);
    oMask.attach(document.proyecto.txtCantidad)
	}

</script>
<link href="../style.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {	color: #3664FF;
	font-weight: bold;
}
.x {	font-size: 36px;
	font-weight: bold;
	border: 2px solid #000000;
	color: #3664FF;
}
-->
</style>
<link href="../css/recibo.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="init()">
<form action="" method="post" name="proyecto" id="proyecto">
  <table width="607" height="913" border="0" align="center" cellpadding="0" cellspacing="0" class="borde" >
    <!--DWLayoutTable-->
    <tr>
      <td colspan="2" rowspan="3" align="center" valign="middle"><img src="../imagenes/logorecibo.jpg" width="208" height="59"></td>
      <td width="21" height="22">&nbsp;</td>
      <td width="42">&nbsp;</td>
      <td width="26">&nbsp;</td>
      <td colspan="2" rowspan="3" valign="top"><div align="center" class="Estilo1">RECIBO DE PAGO </div>
          <br>
&nbsp;&nbsp;&nbsp; Recibo Nro: <span class="TituloGrande">01 - <?php echo number_pad($numeracion,5,"0") ?></span><br>
&nbsp;&nbsp;&nbsp; Fecha:<span class="TituloGrande">&nbsp; <?php echo $fecha ?></span></td>
    </tr>
    <tr>
      <td height="45">&nbsp;</td>
      <td align="center" valign="middle" class="x">x</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="26">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="21">&nbsp;</td>
      <td width="239">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="260"></td>
      <td width="9"></td>
    </tr>
    <tr>
      <td height="164" colspan="7" valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="0">
          <!--DWLayoutTable-->
          <tr>
            <td width="6" height="19"></td>
            <td colspan="2" align="left" valign="middle" class="fondo">Cliente</td>
            <td width="7">&nbsp;</td>
          </tr>
          <tr>
            <td height="2"></td>
            <td width="158"></td>
            <td width="355"></td>
            <td></td>
          </tr>
          <tr>
            <td height="19"></td>
            <td colspan="2" rowspan="4" valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="0" class="bordepe">
                <!--DWLayoutTable-->
                <tr>
                  <td width="27%" valign="top" class="bordefino">Apellido y Nombre:</td>
                  <td width="73%" valign="top" class="cliente"> &nbsp;<?php echo $row_reg_datosCliente['apellido']; ?>&nbsp;&nbsp; <?php echo $row_reg_datosCliente['nombre']; ?></td>
                </tr>
                <tr>
                  <td valign="top" class="bordefino">Razon Social</td>
                  <td valign="top" class="cliente"> &nbsp;<?php echo $row_reg_datosCliente['razonsocial']; ?></td>
                </tr>
                <tr>
                  <td valign="top" class="bordefino">Domicilio:</td>
                  <td valign="top" class="cliente"> &nbsp;<?php echo $row_reg_datosCliente['domicilio']; ?></td>
                </tr>
                <tr>
                  <td valign="top" class="bordefino">Ciudad: </td>
                  <td valign="top" class="cliente"> &nbsp;<?php echo $row_reg_datosCliente['ciudad']; ?></td>
                </tr>
                <tr>
                  <td valign="top" class="bordefino">Telefono:</td>
                  <td valign="top" class="cliente"> &nbsp;<?php echo $row_reg_datosCliente['telf']; ?></td>
                </tr>
            </table></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td height="19"></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td height="19"></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td height="19"></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td height="8"></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td height="184" colspan="7" valign="top"><table width="45%" align="center" class="borde" >
          <tr>
            <td colspan="2" align="center" class="titulo">Agregar Detalle</td>
          </tr>
          <tr>
            <td width="28%">Cantidad</td>
            <td width="72%"><input name="txtCantidad" type="text" id="txtCantidad">
                <input type="hidden" id="num_campos" name="num_campos" value="0" />
                <input type="hidden" id="cant_campos" name="cant_campos" value="0" /></td>
          </tr>
          <tr>
            <td align="left" valign="top">Articulo</td>
            <td><input name="txtArticulo" type="text" id="txtArticulo" size="100"></td>
          </tr>
          <tr>
            <td align="left" valign="top">Precio</td>
            <td><input name="txtPrecio" type="text" id="txtPrecio"></td>
          </tr>
          <tr align="center" valign="middle">
            <td colspan="2"><input name="Submit" type="button" onClick="xajax_agregarFila(xajax.getFormValues('proyecto'))" value="Agregar Detalle">
            &nbsp; <input name="Submit2" type="button" onClick="xajax_cancelar()" value="Eliminar Detalles"></td>
          </tr>
        </table>
          <br>
          <table width="97%" align="center" class="borde" >
            <tr>
              <td colspan="2"><fieldset class="fieldset">
                <legend class="legend"> Detalle del Recibo </legend>
                <div class="clear"></div>
                <div id="form3" class="form-horiz">
                  <table width="100%" id="tblDetalle" class="listado">
                    <tbody id="tbDetalle">
                    </tbody>
                  </table>
                </div>
              </fieldset></td>
            </tr>
            <tr>
              <td width="25%" height="25">Total del Recibo es:
                  <input name="txtTotal" type="hidden" id="txtTotal" value="0">
              </td>
              <td width="75%"><div id="divTotal"></div></td>
            </tr>
          </table>
          <br></td>
    </tr>
    <tr>
      <td height="77">&nbsp;</td>
      <td colspan="5" valign="top" class="fondo">Forma de Pagos <br>
          <br>
          <input name="fpago" type="radio" value="0" checked>
      Efectivo<br>
      <br>
      <input name="fpago" type="radio" value="1">
      Tarjeta de Credito<br>
      <br>
      <input name="fpago" type="radio" value="2">
      Cheque</td>
      <td>&nbsp;</td>
    </tr>
    <tr height="4">
      <td height="4" ></td>
      <td colspan="5" valign="top"><img src="../imagenes/espacio.gif" width="1" height="4"></td>
      <td></td>
    </tr>
    <tr height="4">
      <td height="218" ></td>
      <td colspan="5" valign="top" class="fondo"><p>Observaciones: </p>
          <div align="center"><br>
              <textarea name="observ" cols="68" rows="8" id="textarea"></textarea>
              <br>
              <br>
        </div></td>
      <td></td>
    </tr>
    <tr height="4">
      <td height="115" ></td>
      <td colspan="5" align="center" valign="middle"><input name="btonGuardar" type="button" id="btonGuardar2" onClick="xajax_guardar(xajax.getFormValues('proyecto'))" value="Guardar e Imprimir">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input name="btonCancelar" type="button" id="btonCancelar" onClick="history.back()" value="Cancelar">
      <input name="id_cliente" type="hidden" id="id_cliente" value="<?php echo $_GET['id_cliente']; ?>">
      <input name="fecha" type="hidden" id="fecha" value="<?php echo $fecha1?>">
      <input name="nrorecibo" type="hidden" id="nrorecibo" value="<?php echo $numeracion?>">
      <input name="id_recibo" type="hidden" id="id_recibo">
</td>
      <td></td>
    </tr>
  </table>
</form>
</body>
</html>
<?php
mysql_free_result($reg_numeracion);

mysql_free_result($reg_datosCliente);
?>
