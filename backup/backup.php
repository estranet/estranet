<?php
$db = 'itidi';
$dir = '../bd/';
$nombre_backup = date("d-m-Y-H-i") . "-" . $db . ".zip";
// CONEXION A LA DB
include("../../Connections/itidi.php");
// RECUPERO LAS TABLAS
$tablas = mysql_list_tables($db);
if (!$tablas) {
    echo "Error en la base de datos: no se pueden listar las tablas \n";
    echo 'MySQL Error: ' . mysql_error();
    exit;
}
 
$lineas="";
// RECORRO TODAS LAS TABLAS
while ($tabla = mysql_fetch_row($tablas)) {
 
    // RECUPERO LA INFORMACION DE CREACION DE LA TABLA
    $creacion = mysql_fetch_array(mysql_query( "SHOW CREATE TABLE $tabla[0]" ));
    $lineas.= "-- Informacion de creacion de la tabla $tabla[0]\n\n";
    $lineas.= $creacion['Create Table'].";\n\n";
     // VUELCO LOS REGISTROS DE LA TABLA
    $lineas.= "-- Volcado de registros en la tabla $tabla[0]\n\n";
	//Limito la carga  en 1100  registros;
	$t=1100;
	//Calculo Cantida de Paginas
	$canRegistros=mysql_num_rows(mysql_query("SELECT * FROM $tabla[0]"));
	$cantPaginas=ceil($canRegistros/$t);
	$pagina=0;
	while($pagina<$cantPaginas)
	{
	  // VUELCO LOS REGISTROS DE LA TABLA
	    $inicio=$pagina*$t;
	    $registros = mysql_query( "SELECT * FROM $tabla[0] LIMIT $inicio,$t");	// RECUPERO LOS NOMBRES DE LOS CAMPOS
		$columnas_txt = "";
		$columnas = mysql_query( "SHOW COLUMNS FROM $tabla[0]" );
		$cantidad_columnas = mysql_num_rows($columnas);
		if (mysql_num_rows($columnas) > 0) {
			while ($columna = mysql_fetch_assoc($columnas)) {
				$columnas_txt .= $columna['Field'] . ", ";
			}
		}
		$columnas = substr($columnas_txt, 0, -2);
		$lineas.=  "INSERT INTO $tabla[0] ($columnas) VALUES\n" ;
		$registros_txt = "";
		while ($registro = mysql_fetch_array($registros)) {
			$i = 0;
			$registro_txt = "";
			while ($i < $cantidad_columnas) {
			
				
				$registro_txt .= "'".mysql_escape_string($registro[$i])."', ";
				$i++;
			}
			$registros_txt .= "(".substr($registro_txt, 0, -2)."),\n";
		}
		$lineas.= substr($registros_txt, 0, -2).";\n\n\n";
	  $pagina++;
	}

}
  if(file_exists($db.".sql"))
  		unlink($db.".sql");
  include_once("../../pclzip-2-8/pclzip.lib.php");
  $control = fopen($db.".sql","w+");  
  if($control == false){  
  	 die("No se ha podido crear el archivo.");  
  }
  fwrite($control, $lineas);
  
?>