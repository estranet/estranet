
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Restaurar Bases de Datos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/style.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript">
function eliminar(archivo)
{
  if(confirm("Esta Seguro que desea Eliminar el archivo: "+archivo))
      location.replace("eliminar.php?ar="+archivo);
}
</script>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="700" align="center" class="bordetable" >
  <tr>
    <td colspan="6" class="tituloBus">Restaurar Bases de datos</td>
  </tr>
   <tr align="center" class="linea2">
     <td colspan="6"><a href="index.php">Volver</a></td>
   </tr>
   <tr align="center" valign="middle" class="linea2">
               <td width="45%">Nombre del Archivo </td>
               <td width="17%">Fecha de Creacion </td>
               <td width="14%">Tama&ntilde;o del Archivo </td>
               <td width="9%">Restuarar BD </td>
               <td width="7%">Copiar Archivo </td>
               <td width="8%">Eliminar </td>
<?php   
   include("leer.php");
   include("byte_converter.class.php");
   $cant=count($entradas);
   if($cant>0){
   foreach ($entradas as $archivo => $timestamp) { ?>
  </tr>
			<tr class="tabla2">
				   <td width="45%" align="left"><?php echo $archivo?></td>
				   <td width="17%" align="left"> <?php echo date ( "d/m/Y H:i:s",$timestamp );  ?></td>
			       <td width="14%" align="right">
				   <?php 
					try{
					$byte = new byte_converter;
					$total = $byte->convert(filesize($dir."/".$archivo),"b","mb");
					//echo $total;
					}catch(Exception $e) {echo $e;} 
					echo number_format($total,2). "MB";
					?>
				   </td>
		          <td width="9%" align="center"><a href="puentedump.php?q=2&archivo=<?php echo $archivo ?>"><img src="../imagenes/b_browse.png" width="16" height="16" border="0"></a></td>
				  <td width="7%" align="center"><a href="bd/<?php echo $archivo ?>"><img src="../imagenes/b_props.png" width="16" height="16" border="0"></a></td>
				  <td width="8%" align="center"><a href="javascript:eliminar('<?php echo $archivo ?>');"><img src="../imagenes/b_drop.png" width="16" height="16" border="0"></a></td>
	  </tr>
    <?php }
	}else{?>
	  <tr class="tabla2">
               <td colspan="6" align="center" class="nohay">No hay archivos para restaurar </td>
     </tr>
  <?php }?>
	  <tr class="tabla2">
	    <td colspan="6" align="center">
		<form action="cargararchivo.php?q=5" method="post" enctype="multipart/form-data" name="bd" id="bd">
	      <table width="100%" class="tabla3" >
            <tr>
              <td height="40">Restaurar Base de Datos con otro Archivo externo solo archivos comprimidos zip:<br>                
              <input name="archivo" type="file" id="archivo" accept="application/x-zip-compressed">
              <input type="submit" name="Submit" value="Restaurar"></td>
            </tr>
          </table>
        </form></td>
  </tr> 
</table>
</body>
</html>
