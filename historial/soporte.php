<?php require_once('../Connections/gestionAdmin.php'); ?>
<?php
function number_pad($number,$n,$caracter) {
   return str_pad((int) $number,$n,$caracter,STR_PAD_LEFT);
}

$d_reg_cliente = "0";
if (isset($_GET['id_cliente'])) {
  $d_reg_cliente = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_cliente = sprintf("SELECT cliente.id_cliente, cliente.nombre, cliente.apellido, cliente.razonsocial, cliente.domicilio, cliente.barrio, cliente.fechaAlta AS fecha, cliente.cuit, cliente.telf, cuenta.abonomensual, articulos.articulo FROM cliente, cuenta, articulos WHERE cuenta.id_cliente=cliente.id_cliente AND articulos.id_articulo=cuenta.id_servicio AND cliente.id_cliente=%s", $d_reg_cliente,$d_reg_cliente);
$reg_cliente = mysql_query($query_reg_cliente, $gestionAdmin) or die(mysql_error());
$row_reg_cliente = mysql_fetch_assoc($reg_cliente);
$totalRows_reg_cliente = mysql_num_rows($reg_cliente);

$d_regFichaTecnica = "0";
if (isset($_GET['id_cliente'])) {
  $d_regFichaTecnica = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_regFichaTecnica = sprintf("SELECT * FROM fichatecnica WHERE fichatecnica.id_cliente=%s", $d_regFichaTecnica);
$regFichaTecnica = mysql_query($query_regFichaTecnica, $gestionAdmin) or die(mysql_error());
$row_regFichaTecnica = mysql_fetch_assoc($regFichaTecnica);
$totalRows_regFichaTecnica = mysql_num_rows($regFichaTecnica);

$d_reg_compobante = "0";
if (isset($_GET['id_cliente'])) {
  $d_reg_compobante = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_compobante = sprintf("SELECT DATE_FORMAT(soporte.fecha,'%%d/%%m/%%Y') AS fecha, soporte.id_cliente, soporte.cargo, soporte.id_soporte, soporte.observaciones FROM soporte WHERE soporte.id_cliente=%s ORDER BY soporte.fecha, soporte.id_soporte", $d_reg_compobante,$d_reg_compobante);
$reg_compobante = mysql_query($query_reg_compobante, $gestionAdmin) or die(mysql_error());
$row_reg_compobante = mysql_fetch_assoc($reg_compobante);
$totalRows_reg_compobante = mysql_num_rows($reg_compobante);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Historial de Cliente</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/recibo.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo5 {font-size: 10px}
.Estilo6 {color: #828B93; font: Tahoma;}
.Estilo2 {	font-size: 14px;
	font-weight: bold;
}
-->
</style>
<link href="../css/inphecthyuz.css" rel="stylesheet" type="text/css">
<link href="../style.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="../js/funcioones.js"></script>
</head>

<body>
<table width="1000" border="0" align="center" class="borde">
  <tr>
    <td colspan="2" align="center" valign="middle" class="fondo">Historial del Cliente </td>
  </tr>
  <tr>
    <td width="477" height="229" align="center" valign="top"><table width="90%"  border="0">
      <tr>
        <td align="center" class="fondo">Datos del Cliente</td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Cliente: <?php echo $row_reg_cliente['apellido']; ?> <?php echo $row_reg_cliente['nombre']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Razon Social: <?php echo $row_reg_cliente['razonsocial']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Domicilio: <?php echo $row_reg_cliente['domicilio']; ?>&nbsp; <?php echo $row_reg_cliente['barrio']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Fecha de Alta:&nbsp; <?php echo $row_reg_cliente['fecha']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">C.U.I.T.: <?php echo $row_reg_cliente['cuit']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Tel.: <?php echo $row_reg_cliente['telf']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Velociodad: &nbsp;<?php echo $row_reg_cliente['articulo']; ?> - <?php echo $row_reg_cliente['abonomensual']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">E-mail:</td>
      </tr>
    </table></td>
    <td width="509" align="center" valign="top"><table width="90%"  border="0">
      <tr>
        <td align="center" class="fondo">Datos del Cliente</td>
      </tr>
      <tr>
        <td align="left"><table width="100%" class="borde" >
          <tr class="cliente">
            <td width="39%" align="left">IP CLIENTE </td>
            <td width="61%" class="top11"><?php echo $row_regFichaTecnica['ipcliente']; ?></td>
          </tr>
          <tr class="cliente">
            <td align="left">NODO</td>
            <td class="top11"><?php echo $row_regFichaTecnica['nodo']; ?></td>
          </tr>
          <tr class="cliente">
            <td align="left">IP ANTENA </td>
            <td class="top11"><?php echo $row_regFichaTecnica['ipantena']; ?></td>
          </tr>
          <tr class="cliente">
            <td align="left">ESID</td>
            <td class="top11"><?php echo $row_regFichaTecnica['esid']; ?></td>
          </tr>
          <tr class="cliente">
            <td height="14" align="left">                  <label class="Estilo6"></label>                  <label>Mac AP:</label>                        </td>
            <td class="top11"><?php echo $row_regFichaTecnica['macap']; ?></td>
          </tr>
          <tr class="cliente">
            <td height="14" align="left">MAC TARJETA RED </td>
            <td class="top11"><?php echo $row_regFichaTecnica['mactarjetared']; ?></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>

<br>
<table width="1000"  border="0" align="center">
  <tr>
    <td align="center"><input name="Submit" type="button" onClick="location.replace('../clientes/buscarcliente.php')" value="Volver">
      &nbsp;&nbsp;&nbsp;&nbsp; <input type="button" name="Submit2" value="Imprimir Historial del Cliente"></td>
  </tr>
</table>
<br>
<table width="1000"  border="1" align="center" cellpadding="0" cellspacing="0" class="borde">
  <tr align="center" class="fondo">
    <td height="25" colspan="5">Detalle del Cliente</td>
  </tr>
  <tr align="center" valign="middle" class="thprimirlinea">
    <td width="140" height="28">Fecha</td>
    <td width="168">Nro de Soporte </td>
    <td width="58">Cargo</td>
    <td width="51">Imprimir</td>
    <td width="569">Observaciones</td>
  </tr>
  <?php $cargo=array("Empresa","Cliente")?>
  <?php if ($totalRows_reg_compobante > 0) { // Show if recordset not empty ?>
  <?php do { ?>
  <?php 
    $id_recibo=$row_reg_compobante['id_recibo'];
	
?>
  <tr id="linea" <?php echo $c ?>  onMouseOut="MouseOut(this)" onMouseOver="MouseOver(this)" class="fila_MouseOut">
  <?php $c++?>
    <td><?php echo $row_reg_compobante['fecha']; ?></td>
    <td><?php echo number_pad($row_reg_compobante['id_soporte'],5,"0") ?></td>
      <td align="center"><?php echo $cargo[$row_reg_compobante['cargo']] ?></td>
      <td align="center" valign="middle"><a href="../soporte/imprimirrecibo.php?id_cliente=<?php echo $_GET['id_cliente']; ?>&id_soporte=<?php echo $row_reg_compobante['id_soporte']; ?>&v=1"><img src="../imagenes/b_print.png" width="16" height="16" border="0"></a>
	  </td>
      <td align="left"> &nbsp;<?php echo $row_reg_compobante['observaciones']; ?>		</td>
  </tr>
  <?php } while ($row_reg_compobante = mysql_fetch_assoc($reg_compobante)); ?>
  <?php } // Show if recordset not empty ?>
  <?php if ($totalRows_reg_compobante == 0) { // Show if recordset empty ?>
  <tr align="center" valign="middle">
    <td colspan="5" class="nohay"><p>No hay recibos para el cliente</p></td>
  </tr>
  <?php } // Show if recordset empty ?>
</table>
<br>
<table width="1000"  border="0" align="center">
  <tr>
    <td align="center"><input name="Submit3" type="button" onClick="location.replace('../clientes/buscarcliente.php')" value="Volver">
&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="button" name="Submit22" value="Imprimir Historial del Cliente"></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($reg_cliente);

mysql_free_result($regFichaTecnica);

mysql_free_result($reg_compobante);

?>
