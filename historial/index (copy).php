<?php require_once('../Connections/gestionAdmin.php');
session_name('valido');
session_start();
$permisos=array(1,2);
include('../login/obliga.php');
 ?>
<?php
function number_pad($number,$n,$caracter) {
   return str_pad((int) $number,$n,$caracter,STR_PAD_LEFT);
}

$d_reg_cliente = "0";
if (isset($_GET['id_cliente'])) {
  $d_reg_cliente = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_cliente = sprintf("SELECT cliente.id_cliente, cliente.nombre, cliente.apellido, cliente.razonsocial, cliente.domicilio, cliente.barrio, cliente.fechaAlta AS fecha, cliente.cuit, cliente.telf, cuenta.abonomensual, articulos.articulo,cliente.mail FROM cliente, cuenta, articulos WHERE cuenta.id_cliente=cliente.id_cliente AND articulos.id_articulo=cuenta.id_servicio AND cliente.id_cliente=%s", $d_reg_cliente,$d_reg_cliente);
$reg_cliente = mysql_query($query_reg_cliente, $gestionAdmin) or die(mysql_error());
$row_reg_cliente = mysql_fetch_assoc($reg_cliente);
$totalRows_reg_cliente = mysql_num_rows($reg_cliente);

$d_regFichaTecnica = "0";
if (isset($_GET['id_cliente'])) {
  $d_regFichaTecnica = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_regFichaTecnica = sprintf("SELECT * FROM fichatecnica WHERE fichatecnica.id_cliente=%s", $d_regFichaTecnica);
$regFichaTecnica = mysql_query($query_regFichaTecnica, $gestionAdmin) or die(mysql_error());
$row_regFichaTecnica = mysql_fetch_assoc($regFichaTecnica);
$totalRows_regFichaTecnica = mysql_num_rows($regFichaTecnica);

$d_reg_compobante = "0";
if (isset($_GET['id_cliente'])) {
  $d_reg_compobante = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_compobante = sprintf("SELECT recibo.nrorecibo, DATE_FORMAT(recibo.fecha,'%%d/%%m/%%Y') AS fecha, recibo.id_recibo, recibo.q FROM recibo WHERE recibo.id_cliente=%s ORDER BY recibo.fecha, recibo.nrorecibo", $d_reg_compobante,$d_reg_compobante);
$reg_compobante = mysql_query($query_reg_compobante, $gestionAdmin) or die(mysql_error());
$row_reg_compobante = mysql_fetch_assoc($reg_compobante);
$totalRows_reg_compobante = mysql_num_rows($reg_compobante);

//codigo andres
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_orden = sprintf("SELECT id_orden FROM `ordeninstalacion` WHERE id_cliente=%s", $d_reg_cliente);
$reg_orden = mysql_query($query_reg_orden, $gestionAdmin) or die(mysql_error());
$row_reg_orden= mysql_fetch_assoc($reg_orden);
$totalRows_reg_orden = mysql_num_rows($reg_orden);

if($totalRows_reg_orden>0){
$id_ordeninstalacion=$row_reg_orden['id_orden'];
}
//fin codigo andres
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Historial de Cliente</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/recibo.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo5 {font-size: 10px}
.Estilo6 {color: #828B93; font: Tahoma;}
.Estilo2 {	font-size: 14px;
	font-weight: bold;
}
-->
</style>
<link href="../css/inphecthyuz.css" rel="stylesheet" type="text/css">
<link href="../style.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="../js/funcioones.js"></script>
</head>

<body>
<table width="1000" border="0" align="center" class="borde">
  <tr>
    <td colspan="2" align="center" valign="middle" class="fondo">Historial del Cliente </td>
  </tr>
  <tr>
    <td width="477" height="229" align="center" valign="top"><table width="90%"  border="0">
      <tr>
        <td align="center" class="fondo">Datos del Cliente</td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Cliente: <?php echo $row_reg_cliente['apellido']; ?> <?php echo $row_reg_cliente['nombre']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Razon Social: <?php echo $row_reg_cliente['razonsocial']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Domicilio: <?php echo $row_reg_cliente['domicilio']; ?>&nbsp; <?php echo $row_reg_cliente['barrio']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Fecha de Alta:&nbsp; <?php echo $row_reg_cliente['fecha']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">C.U.I.T.: <?php echo $row_reg_cliente['cuit']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Tel.: <?php echo $row_reg_cliente['telf']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">Velociodad: &nbsp;<?php echo $row_reg_cliente['articulo']; ?> - <?php echo $row_reg_cliente['abonomensual']; ?></td>
      </tr>
      <tr>
        <td align="left" bgcolor="#CCEDFF" class="cliente">E-mail: &nbsp;<?php echo $row_reg_cliente['mail']; ?></td>
      </tr>
    </table></td>
    <td width="509" align="center" valign="top"><table width="90%"  border="0">
      <tr>
        <td align="center" class="fondo">Datos del Cliente</td>
      </tr>
      <tr>
        <td align="left"><table width="100%" class="borde" >
          <tr class="cliente">
            <td width="39%" align="left">IP CLIENTE </td>
            <td width="61%" class="top11"><?php echo $row_regFichaTecnica['ipcliente']; ?></td>
          </tr>
          <tr class="cliente">
            <td align="left">NODO</td>
            <td class="top11"><?php echo $row_regFichaTecnica['nodo']; ?></td>
          </tr>
          <tr class="cliente">
            <td align="left">IP ANTENA </td>
            <td class="top11"><?php echo $row_regFichaTecnica['ipantena']; ?></td>
          </tr>
          <tr class="cliente">
            <td align="left">ESID</td>
            <td class="top11"><?php echo $row_regFichaTecnica['esid']; ?></td>
          </tr>
          <tr class="cliente">
            <td height="14" align="left">                  <label class="Estilo6"></label>                  <label>Mac AP:</label>                        </td>
            <td class="top11"><?php echo $row_regFichaTecnica['macap']; ?></td>
          </tr>
          <tr class="cliente">
            <td height="14" align="left">MAC TARJETA RED </td>
            <td class="top11"><?php echo $row_regFichaTecnica['mactarjetared']; ?></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>

<br>
<table width="1000"  border="0" align="center">
  <tr>
    <td align="center"><input name="Submit" type="button" onClick="location.replace('../clientes/buscarcliente.php')" value="Volver">
      &nbsp;&nbsp;&nbsp;&nbsp; <input type="button" name="Submit2" value="Imprimir Historial del Cliente">
 <!--codigo html andres -->
<input name="orden" type="button" onClick="location.replace('../clientes/imprimiroi.php?id_cliente='+<?php echo $_GET['id_cliente'];?>+'&id_orden='+ <?php echo $id_ordeninstalacion;?>)" value="Ver orden de instalacion">
 <!--fin codigo html andres -->

</td>
  </tr>
</table>
<br>
<table width="1000"  border="1" align="center" cellpadding="0" cellspacing="0" class="borde">
  <tr align="center" class="fondo">
    <td height="25" colspan="11">Detalle del Cliente</td>
  </tr>
  <tr align="center" valign="middle" class="thprimirlinea">
    <td width="100" height="28">Fecha</td>
    <td width="190">Nro de Recibo </td>
    <td width="50">Cobrador</td>
    <td width="71">Imprimir</td>
    <td width="164">Periodo</td>
    <td width="74">Abono</td>
    <td width="55">Bonif </td>
    <td width="52">Recar</td>
    <td width="75">C. Inst. </td>
    <td width="20">Soporte</td>
    <td width="57">Total</td>
  </tr>
  <?php if ($totalRows_reg_compobante > 0) { // Show if recordset not empty ?>
  <?php do { ?>
  <?php 
    $id_recibo=$row_reg_compobante['id_recibo'];
if($row_reg_compobante['q']==0){
	$id_re_reg_periodo = "0";
	if (isset($id_recibo)) {
	  $id_re_reg_periodo = (get_magic_quotes_gpc()) ? $id_recibo : addslashes($id_recibo);
	}
	mysql_select_db($database_gestionAdmin, $gestionAdmin);
	$query_reg_periodo = sprintf("SELECT * FROM pagoabono WHERE pagoabono.id_recibo=%s", $id_re_reg_periodo);
	$reg_periodo = mysql_query($query_reg_periodo, $gestionAdmin) or die(mysql_error());
	$row_reg_periodo = mysql_fetch_assoc($reg_periodo);
	$totalRows_reg_periodo = mysql_num_rows($reg_periodo);

	$d_reg_boni = "0";
if (isset($id_recibo)) {
  $d_reg_boni = (get_magic_quotes_gpc()) ? $id_recibo : addslashes($id_recibo);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_boni = sprintf("SELECT recibo_detalle.preciopag AS precio FROM recibo_detalle WHERE recibo_detalle.id_detalle=1 AND recibo_detalle.id_recibo=%s", $d_reg_boni);
$reg_boni = mysql_query($query_reg_boni, $gestionAdmin) or die(mysql_error());
$row_reg_boni = mysql_fetch_assoc($reg_boni);
$totalRows_reg_boni = mysql_num_rows($reg_boni);

	$d_reg_rec = "0";
	if (isset($id_recibo)) {
	  $d_reg_rec = (get_magic_quotes_gpc()) ? $id_recibo : addslashes($id_recibo);
	}
	mysql_select_db($database_gestionAdmin, $gestionAdmin);
	$query_reg_rec = sprintf("SELECT recibo_detalle.precio FROM recibo_detalle WHERE recibo_detalle.id_detalle=2 AND recibo_detalle.id_recibo=%s", $d_reg_rec);
	$reg_rec = mysql_query($query_reg_rec, $gestionAdmin) or die(mysql_error());
	$row_reg_rec = mysql_fetch_assoc($reg_rec);
	$totalRows_reg_rec = mysql_num_rows($reg_rec);

	$d_rec_cuota = "0";
if (isset($id_recibo)) {
  $d_rec_cuota = (get_magic_quotes_gpc()) ? $id_recibo : addslashes($id_recibo);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_rec_cuota = sprintf("SELECT recibo_detalle.preciopag AS precio FROM recibo_detalle WHERE recibo_detalle.id_detalle=3 AND recibo_detalle.id_recibo=%s", $d_rec_cuota);
$rec_cuota = mysql_query($query_rec_cuota, $gestionAdmin) or die(mysql_error());
$row_rec_cuota = mysql_fetch_assoc($rec_cuota);
$totalRows_rec_cuota = mysql_num_rows($rec_cuota);
	$idre_reg_total = "0";
if (isset($id_recibo)) {
  $idre_reg_total = (get_magic_quotes_gpc()) ? $id_recibo : addslashes($id_recibo);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_total = sprintf("SELECT SUM(recibo_detalle.preciopag) AS total FROM recibo_detalle WHERE recibo_detalle.id_recibo=%s", $idre_reg_total);
$reg_total = mysql_query($query_reg_total, $gestionAdmin) or die(mysql_error());
$row_reg_total = mysql_fetch_assoc($reg_total);
$totalRows_reg_total = mysql_num_rows($reg_total);

	$d_reg_soporte = "0";
if (isset($id_recibo)) {
  $d_reg_soporte = (get_magic_quotes_gpc()) ? $id_recibo : addslashes($id_recibo);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_soporte = sprintf("SELECT recibo_detalle.preciopag AS precio FROM recibo_detalle WHERE recibo_detalle.id_detalle=6 AND recibo_detalle.id_recibo=%s", $d_reg_soporte);
$reg_soporte = mysql_query($query_reg_soporte, $gestionAdmin) or die(mysql_error());
$row_reg_soporte = mysql_fetch_assoc($reg_soporte);
$totalRows_reg_soporte = mysql_num_rows($reg_soporte);
//codigo andres
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_log = sprintf("SELECT usuario.usuario FROM `log` join usuario on usuario.id_usuario=log.id_usuario WHERE id_recibo=%s", $id_recibo);
$reg_log = mysql_query($query_reg_log, $gestionAdmin) or die(mysql_error());
$row_reg_log= mysql_fetch_assoc($reg_log);
$totalRows_reg_log = mysql_num_rows($reg_log);

//fin codigo andres

  ?>
  <tr id="linea" <?php echo $c ?>  onMouseOut="MouseOut(this)" onMouseOver="MouseOver(this)" class="fila_MouseOut">
  <?php $c++?>
    <td><?php echo $row_reg_compobante['fecha']; ?></td>
    <td><span class="Estilo2">01 - <?php echo number_pad($row_reg_compobante['nrorecibo'],5,"0") ?></span></td>
<td><?php echo $row_reg_log['usuario']; ?></td>
      <td align="center" valign="middle"><a href="../recibos/imprimirrecibo.php?id_cliente=<?php echo $_GET['id_cliente']; ?>&id_recibo=<?php echo $row_reg_compobante['id_recibo']; ?>&v=1"><img src="../imagenes/b_print.png" width="16" height="16" border="0"></a></td>
      <td align="left" valign="middle">Abono&nbsp; mes de
        <?php 
	        $dfecha=explode("-",$row_reg_periodo['periodo']);
			$meses=array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nom","Dic");
			$periodo=$meses[$dfecha[1]-1]." - ".$dfecha[0];
  	        echo $periodo ?>
	  </td>
      <td align="right"><?php echo "$ ".number_format($row_reg_periodo['pago'],2,'.',','); ?></td>
      <td align="right"><?php if($totalRows_reg_boni>0) echo "$ ".number_format($row_reg_boni['precio'],2,'.',','); else echo "&nbsp;" ?></td>
      <td align="right"><?php if($totalRows_reg_rec>0) echo "$ ".number_format($row_reg_rec['precio'],2,'.',','); else echo "&nbsp;" ?></td>
      <td align="right"><?php if($totalRows_rec_cuota>0){								
								echo "$ ".number_format($row_rec_cuota['precio'],2,'.',','); 
							 }else
							  echo "&nbsp;"?>	</td>
      <td align="right"><?php if($totalRows_reg_soporte>0){								
								echo "$ ".number_format($row_reg_soporte['precio'],2,'.',','); 
							 }else
							  echo "&nbsp;"?>	</td>
      <td align="right"><?php echo "$ ".number_format($row_reg_total['total'],2,'.',','); ?></td>
  </tr>
<?php   mysql_free_result($reg_periodo);

mysql_free_result($reg_boni);

mysql_free_result($reg_rec);

mysql_free_result($rec_cuota);

mysql_free_result($reg_total);

mysql_free_result($reg_soporte);
?>
<?php }else{?>
<?php
	$r_reg_detalleC = "0";
	if (isset($id_recibo)) {
	  $r_reg_detalleC = (get_magic_quotes_gpc()) ? $id_recibo : addslashes($id_recibo);
	}
	$c_reg_detalleC = "0";
	if (isset($_GET['id_cliente'])) {
	  $c_reg_detalleC = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
	}
	mysql_select_db($database_gestionAdmin, $gestionAdmin);
	$query_reg_detalleC = sprintf("SELECT recibodr.cantidad, recibodr.precio, recibodr.preciopag,recibodr.id_detalle FROM recibodr WHERE recibodr.id_recibo=%s AND recibodr.id_cliente=%s", $r_reg_detalleC,$c_reg_detalleC);
	$reg_detalleC = mysql_query($query_reg_detalleC, $gestionAdmin) or die(mysql_error());
	$row_reg_detalleC = mysql_fetch_assoc($reg_detalleC);
	$totalRows_reg_detalleC = mysql_num_rows($reg_detalleC);

$r_reg_totalRC = "0";
if (isset($id_recibo)) {
  $r_reg_totalRC = (get_magic_quotes_gpc()) ? $id_recibo : addslashes($id_recibo);
}
$c_reg_totalRC = "0";
if (isset($_GET['id_cliente'])) {
  $c_reg_totalRC = (get_magic_quotes_gpc()) ? $_GET['id_cliente'] : addslashes($_GET['id_cliente']);
}
mysql_select_db($database_gestionAdmin, $gestionAdmin);
$query_reg_totalRC = sprintf("SELECT SUM(recibodr.preciopag) AS total FROM recibodr WHERE recibodr.id_recibo=%s AND recibodr.id_cliente=%s", $r_reg_totalRC,$c_reg_totalRC);
$reg_totalRC = mysql_query($query_reg_totalRC, $gestionAdmin) or die(mysql_error());
$row_reg_totalRC = mysql_fetch_assoc($reg_totalRC);
$totalRows_reg_totalRC = mysql_num_rows($reg_totalRC);
?>
  <tr id="linea" <?php echo $c ?>  onMouseOut="MouseOut(this)" onMouseOver="MouseOver(this)" class="fila_MouseOut">
    <td valign="top"><?php echo $row_reg_compobante['fecha']; ?></td>
    <td valign="top"><span class="Estilo2">01 - <?php echo number_pad($row_reg_compobante['nrorecibo'],5,"0") ?></span></td>
   <td><?php echo $row_reg_log['usuario']; ?></td>
 <td align="center" valign="top"><a href="../recibos/imprimirrecibo.php?id_cliente=<?php echo $_GET['id_cliente']; ?>&id_recibo=<?php echo $row_reg_compobante['id_recibo']; ?>&v=1"><img src="../imagenes/b_print.png" width="16" height="16" border="0"></a></td>
    <td colspan="7" align="center" valign="middle"><table width="100%" class="bordefino" >
      <tr align="center" valign="middle" class="thprimirlinea">
        <td width="15%">Cantidad</td>
        <td width="55%">Descripcion</td>
        <td width="15%">Precio </td>
        <td width="15%">Total </td>
        </tr>
	<?php do{?>
      <tr class="fila_MouseOver">
        <td align="left"><?php echo $row_reg_detalleC['cantidad']; ?></td>
        <td align="left"><?php echo $row_reg_detalleC['id_detalle']; ?></td>
        <td align="right">$ <?php echo number_format($row_reg_detalleC['precio'],2,'.',','); ?></td>
        <td align="right">$ <?php echo number_format($row_reg_detalleC['preciopag'],2,'.',','); ?></td>
        </tr>
		<?php }while($row_reg_detalleC = mysql_fetch_assoc($reg_detalleC));?>
      <tr>
        <td colspan="3" align="right" class="Estilo2">Total de Recibo:</td>
        <td align="right" class="Estilo2">$ <?php echo number_format($row_reg_totalRC['total'],2,'.',','); ?></td>
      </tr>
    </table></td>
  </tr>
<?php mysql_free_result($reg_detalleC);

mysql_free_result($reg_totalRC);?>

<?php }?>

  <?php } while ($row_reg_compobante = mysql_fetch_assoc($reg_compobante)); ?>
  <?php } // Show if recordset not empty ?>
  <?php if ($totalRows_reg_compobante == 0) { // Show if recordset empty ?>
  <tr align="center" valign="middle">
    <td colspan="10" class="nohay"><p>No hay recibos para el cliente</p></td>
  </tr>
  <?php } // Show if recordset empty ?>
</table>
<br>
<table width="1000"  border="0" align="center">
  <tr>
    <td align="center"><input name="Submit3" type="button" onClick="location.replace('../clientes/buscarcliente.php')" value="Volver">

&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="button" name="Submit22" value="Imprimir Historial del Cliente"></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($reg_cliente);

mysql_free_result($regFichaTecnica);

mysql_free_result($reg_compobante);

?>
