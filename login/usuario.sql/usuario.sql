-- phpMyAdmin SQL Dump
-- version 2.11.0
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-02-2008 a las 16:31:43
-- Versión del servidor: 5.0.41
-- Versión de PHP: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de datos: `osde`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL auto_increment,
  `usuario` varchar(160) NOT NULL,
  `clave` varchar(150) NOT NULL,
  `persona` varchar(160) NULL,
  `permiso` int(2) NOT NULL,
  `bloqueo` int(2) NOT NULL,
  `fechabloqueo` date NOT NULL,
  `ultimoacceso` date NOT NULL,
  PRIMARY KEY  (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `usuario`, `clave`, `permiso`, `bloqueo`, `fechabloqueo`, `ultimoacceso`,`persona`)
 VALUES
(1, 'admin', 'admin',1, 0, '0000-00-00', '0000-00-00','Administrador'),
(2, 'yohana', 'yohana',1, 0, '0000-00-00', '0000-00-00','Yohana Barahoma'),
(3, 'oscar', 'oscar',1, 0, '0000-00-00', '0000-00-00','Oscar Oviedo'),
(4, 'ariel', 'ariel',1, 0, '0000-00-00', '0000-00-00','Ariel Nicolas');
